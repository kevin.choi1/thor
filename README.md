## THOR Weekly Forecast
 
| Detail Tag | Information |
|------------|-------------|
|Created By: | Kevin Choi |
|Support email: | kevin.choi@spreetail.com|
|Business or Technical purpose: |Automation of weekly THOR forecast for demand planning|
|Models: |<ul><li>Boosted Seasonal Linear Regression</li><li>Lazy Prophet</li><li>Facebook Prophet</li><li>Autosmoother</li><li>Exponential Weight Moving Average</li><ul>|
|External Models: |<ul><li>New Product Review</li><li>Vendor Category Seasonality</li><ul>|
|Input Data Source |<li>silver_thor.input_oossales</li>|
|External Model Data Sources |<ul><li>silver_thor.fcst_npr</li><li>silver_thor.fcst_vcs</li><ul>|
|Dependent Data Sources |<li>silver_thor.input_networknode</li>|
|Update Datasets |<ul><li>gold_thor.fcst_model_results</li><li>gold_thor.fcst_best_model_results</li><li>gold_thor.fcst_best_model_results_archive</li><ul>|
|Output Datasets |<li>gold_thor.dp_forecast_output</li>|

## Development Log

| Date | Developed By | Reason |
|:----:|--------------|--------|
|15th January 2022 | Kevin Choi |MVP completed|
|1st March 2022 | Kevin Choi |Weekly automation and metrics completed|

## Forecasting Data Pipeline

<img alt="alt_text" width="1000000px" src="assets/forecast_metrics_workflow.jpg" />

**Note:** Add any specific note that requires attention.
