# Databricks notebook source
import pandas as pd

# COMMAND ----------

# input parameters
parameters = {
              'forecast_type': {'univariate': True, 'multivariate': True},
              'features': ['Price', 'OperationCost'],
              'npr_forecast': 'npr_forecast',
              'time_grain': 'W',
              'level': 'sku',
              'models': ['ewm','lazyprophet','fbprophet', 'boostedslr', 'autosmoother'],
              'tuning': 'gridsearch',
              'input_type': 'univariate',
              'target': {'csoh': 'WeeklySalesOOS', 'soh': 'AnnualSalesOOSAdjusted'},
              'time_input': {'weekly': 'WeekStartDate'},
              'sku_id': {'PartNumber': 'PartNumber', 'DimProductID': 'DimProductID', 'ItemID': 'ItemID'},
              'holdout_length': {'weekly': 16},
              'forecast_horizon': {'weekly': [5, 9, 13, 52]},
              'forecast_output': True,
              'created_on': pd.to_datetime('today').date()
              }

# COMMAND ----------

tuning_parameters = {'tuning': True,
                     'hyper_opt': 
                             {
                                 'ewm': {'alpha': [.1, .3, .5, .7, .9]},
                                 'sarimax': {
                                  'p':     hp.randint('p', 4),
                                  'd':     hp.randint('d', 2),
                                  'q':     hp.randint('q', 4),
                                  'cp':   hp.randint('cp', 2),
                                  'cd':   hp.randint('cd', 2),
                                  'cq':   hp.randint('cq', 2),
                                  'trend': hp.choice('trend', ['n', 'c', 't', 'ct']),
                                  'transformation': hp.choice('transformation', [None]),
                                  'model_type': hp.choice('model_choice', ['univariate', 'multivariate'])}, 
                                 'lazyprophet': {'regularization': [1, 1.2, 1.4, 1.6, 1.8], 'trend_dampening': [0, .5, 1], 'transformation': [None]}}}
