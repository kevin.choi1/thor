# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ## THOR Weekly Forecast
# MAGIC  
# MAGIC | Detail Tag | Information |
# MAGIC |------------|-------------|
# MAGIC |Created By: | Kevin Choi |
# MAGIC |Support email: | kevin.choi@spreetail.com|
# MAGIC |Business or Technical purpose: |Automation of weekly THOR forecast for demand planning|
# MAGIC |Models: |<li>Boosted Seasonal Linear Regression</li><li>Lazy Prophet</li><li>Facebook Prophet</li><li>Autosmoother</li><li>Exponential Weight Moving Average</li>|
# MAGIC |External Models: |<li>New Product Review</li><li>Vendor Category Seasonality</li>|
# MAGIC |Input Data Source |<li>silver_thor.input_oossales</li>|
# MAGIC |External Model Data Sources |<li>silver_thor.fcst_npr</li><li>silver_thor.fcst_vcs</li>|
# MAGIC |Dependent Data Sources |<li>silver_thor.input_networknode</li>|
# MAGIC |Update Datasets |<li>gold_thor.fcst_model_results</li><li>gold_thor.fcst_best_model_results</li><li>gold_thor.fcst_best_model_results_archive</li>|
# MAGIC |Output Datasets |<li>gold_thor.dp_forecast_output</li>|
# MAGIC 
# MAGIC ## Development Log
# MAGIC 
# MAGIC | Date | Developed By | Reason |
# MAGIC |:----:|--------------|--------|
# MAGIC |15th January 2022 | Kevin Choi |MVP completed|
# MAGIC |1st March 2022 | Kevin Choi |Weekly automation and metrics completed|
# MAGIC 
# MAGIC **Note:** Add any specific note that requires attention.

# COMMAND ----------

# MAGIC %md ##### Controller: Executes Thor Forecasting Engine

# COMMAND ----------

# use for hyperopt tuning
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from pyspark import SparkContext, SparkConf

# COMMAND ----------

# MAGIC %md ##### Step 1: import modules

# COMMAND ----------

# MAGIC %run ./config/run_config

# COMMAND ----------

# MAGIC %run ./src/pre_process

# COMMAND ----------

# MAGIC %run ./src/post_process

# COMMAND ----------

# MAGIC %run ./src/spark_post_process

# COMMAND ----------

# MAGIC %run ./src/train_test

# COMMAND ----------

# MAGIC %run ./src/fitter

# COMMAND ----------

# MAGIC %run ./src/orchestrate

# COMMAND ----------

# MAGIC %run ./src/spark_external_forecast

# COMMAND ----------

# MAGIC %md ##### Step 2: input parameters

# COMMAND ----------

# input parameters as variables
time_grain = parameters['time_grain']
features = parameters['features']
npr_forecast = parameters['npr_forecast']
level = parameters['level']
models = parameters['models']
tuning = parameters['tuning']
input_type = parameters['input_type']
target = parameters['target']['csoh']
target_secondary = parameters['target']['soh']
time_input = parameters['time_input']['weekly']
sku_id = parameters['sku_id']['DimProductID']
holdout_length = parameters['holdout_length']['weekly']
forecast_horizon = parameters['forecast_horizon']['weekly']
forecast_output = parameters['forecast_output']
tuning = tuning_parameters['tuning']
params = tuning_parameters['hyper_opt']
forecast_type = parameters['forecast_type']
created_on = parameters['created_on']
replenished = True

# COMMAND ----------

# MAGIC %md ##### Step 3: data ingestion

# COMMAND ----------

# get min/max holdout dates in training data + date the training data was updated
holdout_dates = spark.sql("""select CAST((max(WeekStartDate) - 112) as string) as holdout_min_date, CAST(max(WeekStartDate) as string) as holdout_max_date, CAST(max(created_on) as string) as created_on from silver_thor.input_oossales""")

# holdout date variables
min_date = """'2018-01-01'"""
holdout_min_date = """'{}'""".format(holdout_dates.first()['holdout_min_date'])
holdout_max_date = """'{}'""".format(holdout_dates.first()['holdout_max_date'])
created_on = """'{}'""".format(holdout_dates.first()['created_on'])

print('Minimum date: ', min_date)
print('Holdout minimum date: ', holdout_min_date)
print('Holdout maximum date: ', holdout_max_date)
print('Snapshot or creation date: ', created_on)

# COMMAND ----------

# replenished and non-replenished data
if replenished is True:
    # replenished with NPR
    raw_data = spark.sql("""select
        thor.created_on,
        thor.DimProductID,
        thor.WeekStartDate,
        thor.WeekNumber,
        thor.FiscalYear,
        thor.WeeklySalesOOS,
        npr.Forecast as npr_forecast,
        coalesce(npr.npr_sku, 'not_npr') as npr_sku
        from
        (
        select * from  silver_thor.input_oossales where ItemID in (select distinct(ItemID) from silver_thor.itemid_procurementstatus_list where ProcurementStatus in ('Replenish', 'Replenish - Reset', 'Replenish - Amplify') and ItemID in (select distinct(ItemID) from bronze_spreedw.dim_product where ListingType == 'Individual')) and WeekStartdate <= {0} and WeekStartDate >= {1}
        ) thor
        left join
        (select *, 'npr_sku' as npr_sku from silver_thor.fcst_npr_20220206) npr
        on thor.DimProductID = npr.id and thor.WeekStartDate = npr.ts""".format(holdout_max_date, min_date))

    # create sql temp table
    raw_data.createOrReplaceTempView('sql_raw_data')
    
    # convert data to pandas
    raw_data = raw_data.toPandas()

elif replenished is False:
    # non-replenished with NPR
    raw_data = spark.sql("""select
        thor.created_on,
        thor.DimProductID,
        thor.WeekStartDate,
        thor.WeekNumber,
        thor.FiscalYear,
        thor.WeeklySalesOOS,
        npr.Forecast as npr_forecast,
        coalesce(npr.npr_sku, 'not_npr') as npr_sku
        from
        (
        select * from silver_thor.input_oossales where ItemID in (select distinct(ItemID) from silver_thor.itemid_procurementstatus_list where ProcurementStatus in ('Not Ordering - Other', 'Not Ordering - Cost Improvement', 'Not Ordering - Low Volume', 'Not Ordering - Other','Not Ordering - Discontinued') and ItemID in (select distinct(ItemID) from bronze_spreedw.dim_product where ListingType == 'Individual')) and WeekStartdate <= {0} and WeekStartDate >= {1}
        ) thor
        left join
        (select *, 'npr_sku' as npr_sku from silver_thor.fcst_npr) npr
        on thor.DimProductID = npr.id and thor.WeekStartDate = npr.ts""".format(holdout_min_date, min_date))

    # create sql temp table
    raw_data.createOrReplaceTempView('sql_raw_data')
    
    # convert data to pandas
    raw_data = raw_data.toPandas()

# COMMAND ----------

# replenish vs non-replenished column name
if replenished is True:
    model_dbx_table_name = 'gold_thor.fcst_model_results'
    best_model_dbx_table_name = 'gold_thor.fcst_best_model_results'
elif replenished is False:
    model_dbx_table_name = 'gold_thor.fcst_model_results_non_replenished'
    best_model_dbx_table_name = 'gold_thor.fst_best_model_results_non_replenished'

print('All model table name: ', model_dbx_table_name)
print('best model table name: ', best_model_dbx_table_name)

# COMMAND ----------

#applies to npr_forecast - fcst_npr does not include created_on
external_npr_forecast = spark.sql("""
select 
npr_2.*,
rd.WeeklySalesOOS
from
(
select 
npr.id,
npr.ts,
npr.forecast,
npr.model,
npr.created_on,
case
when npr.ts <= {0} then 'Train'
when npr.ts > {0} then 'Forecast'
end as label
from silver_thor.fcst_npr npr
inner join (select DimProductID from sql_raw_data group by DimProductID) as criteria
on npr.id = criteria.DimProductID
) npr_2
left join (select DimProductID, WeekStartDate, WeeklySalesOOS from sql_raw_data) rd
on npr_2.id = rd.DimProductID and npr_2.ts = rd.WeekStartDate
""".format(holdout_max_date))

print('NPR forecast loaded.')

# COMMAND ----------

#applies to npr_forecast - fcst_vcs includes created_on
external_vcs_forecast = spark.sql("""
select 
vcs_2.*,
rd.WeeklySalesOOS
from
(
select 
vcs.id,
vcs.ts,
vcs.forecast,
vcs.model,
vcs.created_on,
case
when vcs.ts <= {0} then 'Train'
when vcs.ts > {0} then 'Forecast'
end as label
from silver_thor.fcst_vcs vcs
inner join (select DimProductID from sql_raw_data group by DimProductID) as criteria
on vcs.id = criteria.DimProductID
) vcs_2
left join (select DimProductID, WeekStartDate, WeeklySalesOOS from sql_raw_data) rd
on vcs_2.id = rd.DimProductID and vcs_2.ts = rd.WeekStartDate
""".format(holdout_max_date))

print('VCS forecast loaded.')

# COMMAND ----------

# MAGIC %md ##### Step 4: data cleaning

# COMMAND ----------

# filter data for features, time_input, target
data = raw_data[['DimProductID','WeekStartDate','WeekNumber','FiscalYear','WeeklySalesOOS','npr_forecast', 'npr_sku', 'created_on']]

# COMMAND ----------

# clean NAN to 0 for target variable
data = Preprocess.clean_missing(data, target)

# COMMAND ----------

# MAGIC %md ##### Step 5: preprocess - setup model fitting for parallelization

# COMMAND ----------

# group by per sku, date, and targetk
inputs = Preprocess(time_grain, holdout_length, target, features, time_input).group_for_parallel(data=data, response=target, group=sku_id)

# COMMAND ----------

# MAGIC %md ##### Step 6: parallelize train test split

# COMMAND ----------

# DBTITLE 0,Parallelize Train Test Split
# split data into train and test based on holdout length
par_data = sc.parallelize(inputs,100).map(lambda input: Train_Test(time_grain, holdout_length).train_test_label(input, time_input)).collect()

# COMMAND ----------

# only include forecast and holdout data
def only_forecast(data):
    only_forecast_data = data[(data['label'] != 'Train')]
    return(only_forecast_data)

# COMMAND ----------

# MAGIC %md ##### Step 7: parallelized model fitting

# COMMAND ----------

# DBTITLE 0,Parallelized Model Fitting
# orchestrate function to run all forecasts in parallel
forecast_list = sc.parallelize(par_data, 100).map(lambda par: Orchestrate(forecast_horizon, holdout_length, sku_id, time_input, forecast_output, forecast_type, target, features, time_grain, models).compile_results(input=par)).map(lambda par: only_forecast(data=par)).collect()

# length of orignal forecast list (gives SKU count)
print('Number of SKUs: ', len(forecast_list))

# COMMAND ----------

# MAGIC %md ##### Step 8: post-process model cleaning

# COMMAND ----------

# DBTITLE 0,All Forecast Model
# flatten the list of dataframes into a single dataframe
forecast = pd.concat(forecast_list)

forecast_columns = ['created_on', 'id', 'model', 'label', 'ts_category', 'error', 'ts', 'data_length', 'training_length', 'holdout_length', 'y_train', 'fitted_values', 'holdout_forecast', 'five_forecast', 'nine_forecast', 'thirteen_forecast', 'fifty_two_forecast', 'holdout_mae', 'holdout_mse', 'holdout_mape', 'holdout_smape', 'holdout_wfa']

result_schema = StructType([
    StructField('created_on', DateType(), True),
    StructField('id', IntegerType(), True), 
    StructField('model', StringType(), True),
    StructField('label', StringType(), True),
    StructField('ts_category', StringType(), True),
    StructField('error', StringType(), True),
    StructField('ts', DateType(), True),
    StructField('data_length', IntegerType(), True),
    StructField('training_length', IntegerType(), True),
    StructField('holdout_length', IntegerType(), True),
    StructField('y_train', DoubleType(), True),
    StructField('fitted_values', DoubleType(), True),
    StructField('holdout_forecast', DoubleType(), True),
    StructField('five_forecast', DoubleType(), True),
    StructField('nine_forecast', DoubleType(), True),
    StructField('thirteen_forecast', DoubleType(), True),
    StructField('fifty_two_forecast', DoubleType(), True),
    StructField('holdout_mae', DoubleType(), True),
    StructField('holdout_mse', DoubleType(), True),
    StructField('holdout_mape', DoubleType(), True),
    StructField('holdout_smape', DoubleType(), True),
    StructField('holdout_wfa', DoubleType(), True)
    ])

# convert into spark dataframe + apply replace_infs
spark_forecast = spark.createDataFrame(forecast[forecast_columns], schema = result_schema).withColumn('holdout_mape', replace_infs(col('holdout_mape'), lit(0))).withColumn('holdout_forecast', when(col('holdout_forecast') < 0, 0).otherwise(col('holdout_forecast'))).withColumn('five_forecast', when(col('five_forecast') < 0, 0).otherwise(col('five_forecast'))).withColumn('nine_forecast', when(col('nine_forecast') < 0, 0).otherwise(col('nine_forecast'))).withColumn('thirteen_forecast', when(col('thirteen_forecast') < 0, 0).otherwise(col('thirteen_forecast')))

# spark sql dataframe
spark_forecast.createOrReplaceTempView('spark_forecast_temp')

# spark sql clip forecast values for negative to 0, fill na values
spark.sql("""
SELECT
created_on,
id,
model,
-- fill  na labels with 'Forecast'
COALESCE('Forecast', label) AS label,
ts_category,
error,
ts,
data_length,
training_length,
holdout_length,
y_train,
fitted_values,
-- clip negative values to 0
holdout_forecast,
five_forecast,
nine_forecast,
thirteen_forecast,
fifty_two_forecast,
-- fill na holdout_mape with 0
holdout_mae,
holdout_mse,
COALESCE(0, holdout_mape) AS holdout_mape,
holdout_smape,
holdout_wfa
FROM spark_forecast_temp
""").createOrReplaceTempView('spark_forecast')

# COMMAND ----------

# MAGIC %md ##### Step 9: merge external forecast

# COMMAND ----------

# NPR
spark_npr = external_npr_forecast.groupBy('id').apply(ExternalForecast.npr_spark_wrapper)

# spark sql dataframe
spark_npr.createOrReplaceTempView('spark_npr') 

# COMMAND ----------

# VCS
spark_vcs = external_vcs_forecast.groupBy('id').apply(ExternalForecast.vcs_spark_wrapper)

# spark sql dataframe
spark_vcs.createOrReplaceTempView('spark_vcs')

# COMMAND ----------

# join vcs, npr, thor
spark.sql("""
SELECT
*
FROM spark_forecast
UNION ALL
SELECT 
*
FROM spark_npr
UNION ALL
SELECT 
*
FROM spark_vcs""").createOrReplaceTempView('forecast_sql')

# COMMAND ----------

# MAGIC %md ##### Step 10: Store all model results 

# COMMAND ----------

# write to gold all forecast results

# spark query
query = 'CREATE OR REPLACE TABLE {} AS SELECT * FROM {}'.format(model_dbx_table_name, 'forecast_sql')

# spark sql
spark.sql(query)

print('Successfully stored {}'.format(model_dbx_table_name))

# COMMAND ----------

# MAGIC %md ##### Step 11: Archive the previous best model results

# COMMAND ----------

# NO ARCHIVING FOR FIRST RUN
# insert forecast into archive
#spark.sql("""
#CREATE OR REPLACE TABLE  gold_thor.fcst_best_model_results_archive 
#AS
#SELECT * FROM gold_thor.fcst_best_model_results
#UNION ALL 
#SELECT * FROM gold_thor.fcst_best_model_results_archive""")

# COMMAND ----------

# MAGIC %md ##### Step 12: Store and select best model results 

# COMMAND ----------

# Best Model Query - chosen by the SMAPE because MAPE can be zero or inf

# spark query
spark_query = """
SELECT
DATE(a.created_on), 
b.id, 
a.ts, 
a.model, 
a.label, 
a.ts_category, 
a.data_length, 
a.training_length, 
a.holdout_length, 
a.error, 
a.y_train,
a.five_forecast, 
a.nine_forecast, 
a.thirteen_forecast,
a.adj_thirteen_forecast,
a.fifty_two_forecast, 
a.adj_fifty_two_forecast,
a.positive_margin_sales_percent,
a.holdout_forecast, 
a.holdout_mae, 
a.holdout_mse, 
a.holdout_mape, 
b.holdout_smape, 
a.holdout_wfa
  FROM
  (
  SELECT 
  fcst.*, 
  margins.`PositiveMarginSales%` AS positive_margin_sales_percent, 
  (margins.`PositiveMarginSales%` * fcst.thirteen_forecast) AS adj_thirteen_forecast, 
  (margins.`PositiveMarginSales%` * fcst.fifty_two_forecast) AS adj_fifty_two_forecast 
    FROM {0} AS fcst 
    LEFT JOIN silver_thor.ref_positivemargin AS margins ON fcst.id = margins.DimProductID
  ) AS a
  INNER JOIN (SELECT 
              id, 
              min(holdout_smape) AS holdout_smape 
                FROM {0}
                GROUP BY id) AS b 
  ON a.id = b.id AND a.holdout_smape = b.holdout_smape 
  WHERE a.ts >= {1}
  ORDER BY b.id, ts ASC
""".format(model_dbx_table_name, created_on)

# spark sql
spark.sql(spark_query).createOrReplaceTempView('spark_best_model')

# create dbx table
spark_best_model_query = 'CREATE OR REPLACE TABLE {0} AS SELECT * FROM {1}'.format(best_model_dbx_table_name, 'spark_best_model')
spark.sql(spark_best_model_query)
    
print('Successfully stored {}'.format(best_model_dbx_table_name))

# COMMAND ----------

# MAGIC %md ##### Step 13: process best model table as demand planning ready + adjustments with network to node disaggregation

# COMMAND ----------

# DBTITLE 0,Post-Process: Disaggregate Network to Node Gold Table
# MAGIC %run ./post_process_demand_planning_output
