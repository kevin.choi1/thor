# Databricks notebook source
# MAGIC %run ./Transformations

# COMMAND ----------

# MAGIC %run ./ModelClass

# COMMAND ----------

import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator
#from Models.Transformations import Transformations
#from ModelClass import ModelClass

class Ewm(BaseEstimator):
    model = 'ewm'
    version = 0.1
    default_param_space = {'alpha': .1,
                           'transformation': None}
    
    def __init__(self,
                 params: dict = None,
                 transformations = None,
                 alpha = 0.1,
                 trend_cap=None):
        self.params = params
        self.transformations = transformations
        self.alpha = alpha
        self.trend_cap = trend_cap
        
        return
    
    def fit(self, y):
        #if self.params is None:
        #    self.params = Ewm.default_param_space
        self.transformer = Transformations()
        y = pd.Series(self.transformer.transform(y, transformation=self.transformations))
        self.model_obj = y.ewm(alpha = self.alpha)
        self.fitted_values = np.round(self.model_obj.mean())
        
        return self.transformer.inverse_transform(self.fitted_values)

    def predict(self, forecast_horizon):
        predictions = pd.Series(np.ones(forecast_horizon)) * self.fitted_values.iloc[-1]
        
        return self.transformer.inverse_transform(predictions)
