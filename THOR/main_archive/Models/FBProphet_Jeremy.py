# Databricks notebook source
import numpy as np
import pandas as pd

from fbprophet import Prophet
from fbprophet.plot import add_changepoints_to_plot

#from Transformations import Transformations
#from ModelClass import ModelClass

# file/standard with naming conventions?

# Parameters to optimize. Default values for these will probably overfit
# changepoint_prior_scale
# seasonality_prior_scale
# changepoint_range

class FbProphet(ModelClass):
    model = 'fbprophet'
    version = 0.1
    default_param_space = {
                            'growth': 'linear',
                            'seasonality_mode': 'multiplicative',
                            'yearly_seasonality': True,
                            'daily_seasonality': False,
                            'weekly_seasonality'=False,
                            'seasonality_prior_scale'=1,
                            'changepoint_prior_scale'=0.02,
                            'changepoint_range'=.98
                        }
    
    def __init__(self,
                 params: dict=None,
                 freq='W',
                 trend_cap=True):
        self.params = params
        self.freq = freq
        self.trend_cap = trend_cap
        
        return
    
    def fit(self, y):
        if self.params is None:
            self.params = FbProphet.default_param_space
        self.y = y
        self.y.index.freq = self.freq
        self.transformer = Transformations()
        self.y = self.transformer.transform(self.y, transformation=self.params['transformation'])

        growth = self.params['growth']
        seasonality_mode = self.params['seasonality_mode']
        yearly_seasonality = self.params['yearly_seasonality']
        weekly_seasonality = self.params['weekly_seasonality']
        seasonality_prior_scale = self.params['seasonality_prior_scale']
        changepoint_prior_scale = self.params['changepoint_prior_scale']
        changepoint_range = self.params['changepoint_range']
        m = Prophet( 
            growth=growth,
            seasonality_mode = seasonality_mode,
            yearly_seasonality=yearly_seasonality,
            weekly_seasonality=weekly_seasonality,
            seasonality_prior_scale=seasonality_prior_scale,   
            changepoint_prior_scale=changepoint_prior_scale,
            changepoint_range=changepoint_range
        )

        self.model_obj = self.model_obj.fit(y)
        self.fitted_values = self.model_obj.fittedvalues
        
        return self.transformer.inverse_transform(self.fitted_values)
    
    def predict(self, forecast_horizon):
        future_df = self.model_obj.make_future_dataframe(periods=forecast_horizon, freq=self.freq)
        forecast_df = self.model_obj.predict(future_df)
        self.forecast_df = forecast_df
        fdt = forecast_df.dropna()['ds'].min()
        ldt = forecast_df.dropna()['ds'].max()
        
        if self.trend_cap:
            self.break_trend(fdt, ldt)
        predictions = self.forecast_df['yhat']
        return self.transformer.inverse_transform(predictions)

    # yhat = (trend * (1 + multiplicative_terms) + additive_terms
    def re_calc_yhat(self, trend,mult,add):
        yhat = trend * (1 + mult) + add
        return yhat

    def break_trend(self, fdt, ldt):
        start_t_val = self.forecast_df[self.forecast_df['ds']==fdt]['trend'].values[0]
        start_tl_val = self.forecast_df[self.forecast_df['ds']==fdt]['trend_lower'].values[0]
        start_tu_val = self.forecast_df[self.forecast_df['ds']==fdt]['trend_upper'].values[0]
        
        tf = self.forecast_df['ds']<fdt
        self.forecast_df.loc[tf,'trend'] = start_t_val
        
        self.forecast_df.loc[tf,'yhat'] = re_calc_yhat(self.forecast_df[tf]['trend'],self.forecast_df[tf]['multiplicative_terms'],self.forecast_df[tf]['additive_terms'])
        self.forecast_df.loc[tf,'yhat_lower'] = self.forecast_df[tf]['yhat']
        self.forecast_df.loc[tf,'yhat_upper'] = self.forecast_df[tf]['yhat']
        
        end_t_val = self.forecast_df[self.forecast_df['ds']==ldt]['trend'].values[0]
        end_tl_val = self.forecast_df[self.forecast_df['ds']==ldt]['trend_lower'].values[0]
        end_tu_val = self.forecast_df[self.forecast_df['ds']==ldt]['trend_upper'].values[0]
        
        tf = self.forecast_df['ds']>ldt
        self.forecast_df.loc[tf,'trend'] = end_t_val
        
        self.forecast_df.loc[tf,'yhat'] = re_calc_yhat(self.forecast_df[tf]['trend'],self.forecast_df[tf]['multiplicative_terms'],self.forecast_df[tf]['additive_terms'])
        self.forecast_df.loc[tf,'yhat_lower'] = self.forecast_df[tf]['yhat']
        self.forecast_df.loc[tf,'yhat_upper'] = self.forecast_df[tf]['yhat']
