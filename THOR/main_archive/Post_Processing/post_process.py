# Databricks notebook source
# MAGIC %run /Shared/THOR_tuning_dev/Models/Model_Utils

# COMMAND ----------

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 09:48:49 2021

@author: kevin.choi
"""
## need to create a post process function that collects the various forecast with their appropriate dates
import pandas as pd

class Postprocess:
    
    def __init__(self, holdout_length, forecast_horizon):
        self.holdout_length = holdout_length
        # get the longest forecast horizon to account for time series length
        self.forecast_horizon = forecast_horizon[2]

    def compile_results(self, fit_results, train_length, test_length, columns):
        """
        :params fit results: dictionary of forecasted values
        :params columns: list of column names
        :return reindex_table: reindex table to include forecast results at the appropriate dates
        """
        # create pandas dataframe for training data (before holdout)
        train_table = pd.DataFrame(dict([(k,pd.Series(v)) for k,v in fit_results.items() if k not in columns]))
        # create pandas dataframe for holdout and forecast
        forecast_table = pd.DataFrame(dict([(k,pd.Series(v)) for k,v in fit_results.items() if k in columns]))
        # create index for holdout and forecast
        forecast_index = list(range(train_length, train_length + self.holdout_length + self.forecast_horizon))
        # set index
        forecast_table = forecast_table.set_index([pd.Index(forecast_index)])
        # merge both tables
        reindex_table = pd.merge(train_table, forecast_table, how='outer', left_index=True, right_index=True)
        
        return(reindex_table)
      
    def calculate_accuracy(self, model_results):
        holdout_forecast = model_results['holdout_forecast'].dropna()
        y_test = model_results['y_test'].dropna()
        # Validation
        model_results['holdout_mae'] = Model_Utils().compute_mae(holdout_forecast, y_test)
        model_results['holdout_mse'] = Model_Utils().compute_mse(holdout_forecast, y_test)
        model_results['holdout_mape'] = Model_Utils().compute_mape(holdout_forecast, y_test)
        model_results['holdout_smape'] = Model_Utils().compute_smape(holdout_forecast, y_test)
        model_results['holdout_wfa'] = Model_Utils().compute_wfa(holdout_forecast, y_test)
        
        return(model_results)
