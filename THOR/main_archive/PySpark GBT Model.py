# Databricks notebook source
# MAGIC %md
# MAGIC # Gradient Boosted Tree Model via PySpark

# COMMAND ----------

# MAGIC %run ./src/models/ThorAutoSmoother_pyspark

# COMMAND ----------

# MAGIC %r
# MAGIC 
# MAGIC .libPaths(c("/dbfs/r_libs", .libPaths()))
# MAGIC 
# MAGIC suppressPackageStartupMessages(library(tidymodels))
# MAGIC suppressPackageStartupMessages(library(modeltime))
# MAGIC suppressPackageStartupMessages(library(tidyverse))
# MAGIC suppressPackageStartupMessages(library(timetk))
# MAGIC suppressPackageStartupMessages(library(sparklyr))
# MAGIC suppressPackageStartupMessages(library(arrow))
# MAGIC suppressPackageStartupMessages(library(mlflow))
# MAGIC 
# MAGIC nms <- function(x) {
# MAGIC   sdf_schema(x) %>% 
# MAGIC   enframe() %>% 
# MAGIC   select(-name) %>% 
# MAGIC   unnest_wider(value) %>% 
# MAGIC   display()
# MAGIC }
# MAGIC 
# MAGIC hd <- function(x) {
# MAGIC   x %>% 
# MAGIC     head(1000) %>% 
# MAGIC     collect() %>% 
# MAGIC     display()
# MAGIC }
# MAGIC 
# MAGIC sc <- spark_connect(method = "databricks")
# MAGIC 
# MAGIC #registerDoSpark(sc)
# MAGIC 
# MAGIC train <- tbl(sc, "silver_thor.tree_model_inputs") %>% 
# MAGIC   filter(label == "train") %>% 
# MAGIC   select(-label) %>% 
# MAGIC   sdf_register("train") 
# MAGIC 
# MAGIC test  <- tbl(sc, "silver_thor.tree_model_inputs") %>% 
# MAGIC   filter(label == "test" | ts_category == "npr") %>% 
# MAGIC   select(-label) %>% 
# MAGIC   sdf_register("test")

# COMMAND ----------

from pyspark.ml import Pipeline
from pyspark.ml import PipelineModel
from pyspark.ml.regression import GBTRegressor
from pyspark.ml.feature import VectorAssembler, StandardScaler
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from mlflow.models.signature import infer_signature

import mlflow

train = spark.sql("""select * from train""")
test  = spark.sql("""select * from test""")

# COMMAND ----------

# writing the autosmoother function in pyspark
from pyspark.ml.util import DefaultParamsReadable, DefaultParamsWritable 
from pyspark.ml import Transformer
from pyspark import keyword_only 

class AutoSM(Estimator, DefaultParamsRedable, DefaultParamsWritable):

    @keyword_only
    def __init__(self,
                 series = None,
                 smoother = 'double',
                 seasonal_period = None,
                 transformation = None,
                 freq='W',
                 trend_cap=0.50):
        self.series = series
        self.smoother = smoother
        self.seasonal_period = seasonal_period
        self.transformation = transformation
        self.freq = freq
        self.trend_cap = trend_cap
        return
    

# COMMAND ----------

y_train = train['WeeklySalesOOS']

iCols = ["GoogleCategoryLevel1_te", "GoogleCategoryLevel2_te", "GoogleCategoryLevel3_te", "GoogleCategoryLevel4_te",
         "BuyBox", "Price", "Date_year", "Date_quarter", "Date_month", "Date_yday", "Date_week", "idx"]

fts = VectorAssembler(
    inputCols = iCols,
    outputCol = "features"
)

scaler = StandardScaler(
    inputCol      = "features", 
    outputCol     = "scaledFeatures",
    withStd       = True, 
    withMean      = False
)

gbt = GBTRegressor(
    featuresCol           = "scaledFeatures", 
    labelCol              = "WeeklySalesOOS",
    maxDepth              = 20,
    stepSize              = 0.05,
    featureSubsetStrategy = "auto",
    maxIter               = 20,
    subsamplingRate       = 1,
    maxBins               = 64,
    minInfoGain           = 0.01,
    cacheNodeIds          = True
)

autosm = ThorAutoSmoother(
    series = y_train, 
    smoother = 'double',
    seasonal_period = None,
    transformation = None,
    freq = 'W',
    trend_cap = 0.50)


mae_evaluator = RegressionEvaluator(
    labelCol      = "WeeklySalesOOS",
    predictionCol = "prediction",
    metricName    = "mae"
)

r2_evaluator = RegressionEvaluator(
    labelCol      = "WeeklySalesOOS",
    predictionCol = "prediction",
    metricName    = "r2"
)

rmse_evaluator = RegressionEvaluator(
    labelCol      = "WeeklySalesOOS",
    predictionCol = "prediction",
    metricName    = "rmse"
)

pipeline = Pipeline(stages=[fts, scaler, gbt, autosm])

# COMMAND ----------

autosm

# COMMAND ----------

trns = fts.transform(train)

scl  = scaler.fit(trns)

scl

# COMMAND ----------

trns = fts.transform(train)

scl  = scaler.fit(trns)

# displaying standardscalermodel doesn't work
display(scl)

# COMMAND ----------

with mlflow.start_run(run_name="feature_scaled_std"):
    store_train = train.cache()
    store_test  = test.cache()
    model = pipeline.fit(store_train) # training model
    predictions = model.transform(store_test) # testing model
    train_signature = store_train.select(iCols) # ignores all other features created on the pipeline
    prediction_signature = predictions.select('prediction') # ignores all other features created on the training pipeline 
    signature = infer_signature(train_signature, prediction_signature) # register model schema
    test_metric = mae_evaluator.evaluate(predictions)
    mlflow.log_metric('test_' + mae_evaluator.getMetricName(), test_metric) 
    test_metric = r2_evaluator.evaluate(predictions)
    mlflow.log_metric('test_' + r2_evaluator.getMetricName(), test_metric) 
    test_metric = rmse_evaluator.evaluate(predictions)
    mlflow.log_metric('test_' + rmse_evaluator.getMetricName(), test_metric) 
    mlflow.spark.log_model(model, 'tree-model', signature=signature) # logging model to mlflow

# COMMAND ----------

logged_model = 'dbfs:/databricks/mlflow-tracking/3004506821378591/17d0702a637943a1bc8492b4d4e02d4d/artifacts/tree-model'

model = mlflow.spark.load_model(logged_model)

test_out = model.transform(test)

# COMMAND ----------

test_out.createOrReplaceTempView('tree_model_output')

# COMMAND ----------

# MAGIC %r
# MAGIC 
# MAGIC tbl(sc, "tree_model_output") %>% hd()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Training / Cross Validation

# COMMAND ----------

fts = VectorAssembler(
    inputCols = ["GoogleCategoryLevel1_te", "GoogleCategoryLevel2_te", "GoogleCategoryLevel3_te", "GoogleCategoryLevel4_te",
                 "BuyBox", "Price", "Year", "Quarter", "Month", "Day", "Week", "idx"],
    outputCol = "features"
)

gbt = GBTRegressor(
    featuresCol = "features", 
    labelCol    = "WeeklySalesOOS"
)

evaluator = RegressionEvaluator(
    labelCol      = "WeeklySalesOOS",
    predictionCol = "prediction",
    metricName    = "mae"
)

grid = ParamGridBuilder() \
    .addGrid(gbt.maxDepth, [20]) \
    .addGrid(gbt.stepSize, [0.05],) \
    .addGrid(gbt.featureSubsetStrategy, ["auto"]) \
    .addGrid(gbt.maxIter, [25, 50]) \
    .addGrid(gbt.subsamplingRate, [.5, 1]) \
    .addGrid(gbt.maxBins, [64]) \
    .addGrid(gbt.minInfoGain, [0.01, 0.05]) \
    .build()

pipeline = Pipeline(stages=[fts, gbt])

df.cache()

def fit_model():

  with mlflow.start_run(nested = True) as run:

    cv = CrossValidator(
        estimator          = pipeline, 
        evaluator          = evaluator, 
        estimatorParamMaps = grid,
        parallelism        = 16,
        numFolds           = 3
    )
    
    cvModel = cv.fit(df)
    model   = cvModel.bestModel
    
    test_metric = evaluator.evaluate(cvModel.transform(test))
    mlflow.log_metric('test_' + evaluator.getMetricName(), test_metric) 
  
    mlflow.spark.log_model(spark_model = model, sample_input = df, artifact_path = "model")

# COMMAND ----------

fit_model()
