# Databricks notebook source
import numpy as np

class Train_Test:
    
    def __init__(self, time_grain, holdout_length, target, features):
        self.time_grain = time_grain
        self.holdout_length = holdout_length
        self.target = target
        self.features = features
        
    def train_test_split(self, data):
        """
        Cleaning function to fill missing NAN to 0
    
        Parameters
        ----------
        data: TYPE pandas dataframe
            input of time series data
          
        Returns
        -------
        pandas dataframe: TYPE
            cleaned pandas dataframe with train and test
        """
        
        # Create train/test data
        #train = data[:len(data)-self.test_length]
        #test = data[len(data)-self.test_length:]
        
        # Create train/test data
        train = data[data['Label'] == 'Train']
        test = data[data['Label'] == 'Test']

        return(train, test)
    
    def train_test_label(self, data, time_input):
        """
        :params data:
        :params time_input:
        :return data: train, test and train less than holdout length is all train  
        """
        data = data.sort_values(by=time_input)
        data.reset_index(drop=True, inplace=True)
        
        if data['data_length'].min() <= self.holdout_length:
            data.loc[0:len(data),'Label'] = 'Train'
            data['ts_category'] = 'npr'
           #data.loc[len(data)-self.holdout_length:len(data)-1,'Label'] = 'Test'
        elif((data['data_length'].min() > self.holdout_length) & ((data['data_length'].min() < 30))):
            data.loc[0:len(data)-self.holdout_length, 'Label'] = 'Train'
            data.loc[len(data)-self.holdout_length:len(data)-1,'Label'] = 'Test'
            data['ts_category'] = 'half_year'
        else:
            data.loc[0:len(data)-self.holdout_length, 'Label'] = 'Train'
            data.loc[len(data)-self.holdout_length:len(data)-1,'Label'] = 'Test'
            data['ts_category'] = 'half_year_plus'           
            
        return(data)
 
    def X_Y_split_1D(self, train, test, features):
        """
        Cleaning function to fill missing NAN to 0
    
        Parameters
        ----------
        data: TYPE pandas dataframe
            input of time series data
            
        time_grain : TYPE string
            string input either 'monthly', 'weekly', 'daily'

        test_length : TYPE INT
            will determine the length test
          
        Returns
        -------
        pandas dataframe: TYPE
            cleaned pandas dataframe with train and test
        """
            
        X_train, Y_train = np.array(train[self.features]), np.array(train[self.target])
        X_test, Y_test = np.array(test[self.features]), np.array(test[self.target])
        
        return(X_train, Y_train, X_test, Y_test)
    
    def X_Y_split_2D(self, train, test, features):
        """
        Cleaning function to fill missing NAN to 0
    
        Parameters
        ----------
        data: TYPE pandas dataframe
            input of time series data
            
        time_grain : TYPE string
            string input either 'monthly', 'weekly', 'daily'

        test_length : TYPE INT
            will determine the length test
          
        Returns
        -------
        pandas dataframe: TYPE
            cleaned pandas dataframe with train and test
        """
            
        X_train, Y_train = np.vstack(train[self.features]), np.vstack(train[self.target])
        X_test, Y_test = np.vstack(test[self.features]), np.vstack(test[self.target])
        
        return(X_train, Y_train, X_test, Y_test)     
