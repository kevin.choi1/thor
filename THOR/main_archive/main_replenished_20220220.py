# Databricks notebook source
# MAGIC %md ##### Controller: Executes Thor Forecasting Engine

# COMMAND ----------

# use for hyperopt tuning
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from pyspark import SparkContext, SparkConf

# COMMAND ----------

# MAGIC %md ##### Step 1: import modules

# COMMAND ----------

# MAGIC %run ./config/run_config

# COMMAND ----------

# MAGIC %run ./src/pre_process

# COMMAND ----------

# MAGIC %run ./src/post_process

# COMMAND ----------

# MAGIC %run ./src/spark_post_process

# COMMAND ----------

# MAGIC %run ./src/train_test

# COMMAND ----------

# MAGIC %run ./src/fitter

# COMMAND ----------

# MAGIC %run ./src/orchestrate

# COMMAND ----------

# MAGIC %run ./src/spark_external_forecast

# COMMAND ----------

# MAGIC %md ##### Step 2: input parameters

# COMMAND ----------

# input parameters as variables
time_grain = parameters['time_grain']
features = parameters['features']
npr_forecast = parameters['npr_forecast']
level = parameters['level']
models = parameters['models']
tuning = parameters['tuning']
input_type = parameters['input_type']
target = parameters['target']['csoh']
target_secondary = parameters['target']['soh']
time_input = parameters['time_input']['weekly']
sku_id = parameters['sku_id']['DimProductID']
holdout_length = parameters['holdout_length']['weekly']
forecast_horizon = parameters['forecast_horizon']['weekly']
forecast_output = parameters['forecast_output']
tuning = tuning_parameters['tuning']
params = tuning_parameters['hyper_opt']
forecast_type = parameters['forecast_type']
created_on = parameters['created_on']
replenished = True

# COMMAND ----------

# MAGIC %md ##### Step 3: data ingestion

# COMMAND ----------

fcst_top_ranking_seasonality_table = spark.sql('SELECT VendorCategory, CategoryID, Correlation, LPAD(WeekNumber, 2, "00") AS WeekNumber, Seasonality FROM silver_thor.fcst_top_vendor_category_correlation')
fcst_top_ranking_seasonality_table.createOrReplaceTempView('fcst_top_ranking_seasonality_table')

# COMMAND ----------

# back fill for 2022-02-16
input_oossales = spark.sql(
"""
WITH
DateTable AS (
SELECT DISTINCT
MIN(DimDateID) AS DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) as WeekStartDate
,CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")) AS FiscalYearWeek
,FiscalMonth
,CONCAT(FiscalYear, LPAD(FiscalMonth, 2, "00")) AS FiscalYearMonth
,FiscalYear
,LPAD(FiscalWeek, 2, "00") AS WeekNumber
FROM bronze_spreedw_20220220.dim_date
GROUP BY DATE((LEFT(FiscalWeekRange, 10))), CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")), LPAD(FiscalWeek, 2, "00"), FiscalMonth, FiscalYear
ORDER BY DimDateID
),

ThisWeek AS (
SELECT
'2022-02-20' AS TodaysDate
,DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) AS ThisWeek
FROM bronze_spreedw_20220220.dim_date
WHERE DATE(Date) = '2022-02-20'
),

QuantityOnHand AS (
SELECT
CAT.DimProductID
,CAT.ItemID
,CAT.PartNumber
,QOH.DimDateID
,CAL.WeekStartDate
,QOH.QuantityOnHandStartOfDayNetwork AS QuantityOnHandUnfilled
FROM bronze_spreedw_20220220.fact_productperformanceinventory AS QOH
LEFT JOIN bronze_spreedw_20220220.dim_product AS CAT ON QOH.ItemID = CAT.ItemID
LEFT JOIN DateTable AS CAL ON QOH.DimDateID = CAL.DimDateID
WHERE CAL.WeekStartDate IS NOT NULL --AND CAT.DimProductID IN (10319, 60743, 62850)
),

HistoricalSales AS (
SELECT
VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,DATE((LEFT(CAL.FiscalWeekRange, 10))) AS WeekStartDate
,SUM(VC.Quantity) AS ItemsShipped
FROM bronze_spreedw_20220220.fact_valuechain AS VC
LEFT JOIN bronze_spreedw_20220220.dim_date AS CAL ON CAL.DimDateID = VC.DimDateOfOrderID
LEFT JOIN bronze_spreedw_20220220.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
GROUP BY VC.DimShippedProductID, PROD.ItemID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
ORDER BY VC.DimShippedProductID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
),

DimShippedIDList AS (
SELECT DISTINCT
CONCAT(PROD.Supplier, "_", PROD.GoogleCategoryLevel1) AS VendorCategory
,PROD.Supplier
,VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,PROD.GoogleCategoryLevel1
FROM bronze_spreedw_20220220.fact_valuechain AS VC
LEFT JOIN bronze_spreedw_20220220.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
--WHERE VC.DimShippedProductID IN (10319, 60743, 62850)
),

KeyTable AS (
SELECT DISTINCT
DimShippedIDList.*
,WeekStartDate
FROM DimShippedIDList
CROSS JOIN DateTable
),

SalesQOHUnfilled AS (
SELECT
KeyTable.VendorCategory
,KeyTable.Supplier
,KeyTable.PartNumber
,KeyTable.ItemID
,KeyTable.DimShippedProductID
,KeyTable.GoogleCategoryLevel1
,SEAS.CategoryID
,SEAS.Correlation
,DateTable.DimDateID
,KeyTable.WeekStartDate
,DateTable.FiscalYearWeek
,DateTable.WeekNumber
,DateTable.FiscalMonth
,DateTable.FiscalYearMonth
,DateTable.FiscalYear
,COALESCE(HistoricalSales.ItemsShipped, 0) AS ItemsShipped
,QuantityOnHand.QuantityOnHandUnfilled
,COALESCE(SEAS.Seasonality, 0.015) AS Seasonality
FROM KeyTable
LEFT JOIN QuantityOnHand ON QuantityOnHand.DimProductID = KeyTable.DimShippedProductID AND QuantityOnHand.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN HistoricalSales ON HistoricalSales.DimShippedProductID = KeyTable.DimShippedProductID AND HistoricalSales.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN DateTable ON DateTable.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN fcst_top_ranking_seasonality_table AS SEAS ON SEAS.VendorCategory = KeyTable.VendorCategory AND SEAS.WeekNumber = DateTable.WeekNumber
WHERE KeyTable.WeekStartDate <= CURRENT_TIMESTAMP() AND KeyTable.WeekStartDate >= '2014-12-21' --AND KeyTable.DimShippedProductID IN (10319, 60743, 62850, 72790)
ORDER BY KeyTable.DimShippedProductID, KeyTable.WeekStartDate
),

CumulativeSum AS (
SELECT
SalesQOHUnfilled.*
,SUM(ItemsShipped) OVER (PARTITION BY DimShippedProductID ORDER BY WeekStartDate) AS CumulativeSum
,SUM(SalesQOHUnfilled.ItemsShipped) OVER (PARTITION BY SalesQOHUnfilled.DimShippedProductID ORDER BY SalesQOHUnfilled.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING) AS AnnualSales
FROM SalesQOHUnfilled
ORDER BY DimShippedProductID, WeekStartDate
),

ActiveStatus AS (
SELECT
CumulativeSum.*
,CASE WHEN CumulativeSum.CumulativeSum = 0 THEN 'Inactive' ELSE 'Active' END AS ActiveStatus
FROM CumulativeSum
),

InStockStatus AS (
SELECT
ActiveStatus.*
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 0 ELSE ActiveStatus.QuantityOnHandUnfilled END AS QuantityOnHand
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 'Inactive' WHEN ActiveStatus.QuantityOnHandUnfilled < 1 THEN 'OOS' ELSE 'InStock' END AS InStockStatus
FROM ActiveStatus
ORDER BY ActiveStatus.DimShippedProductID, ActiveStatus.WeekStartDate
),

QOHFilled AS (
SELECT
q.*
,QOHPartition
,FIRST_VALUE(q.QuantityOnHand) OVER (PARTITION BY QOHPartition ORDER BY q.DimShippedProductID, q.WeekStartDate) AS QuantityOnHandFilled
FROM (
SELECT
InStockStatus.*
,SUM(CASE WHEN QuantityOnHand IS NULL THEN 0 ELSE 1 END) OVER (ORDER BY DimShippedProductID, WeekStartDate) AS QOHPartition
FROM InStockStatus
ORDER BY DimShippedProductID
) AS q
),

RelevantVariables AS (
SELECT
QOHFilled.VendorCategory
,QOHFilled.Supplier
,QOHFilled.PartNumber
,QOHFilled.DimShippedProductID
,QOHFilled.ItemID
,QOHFilled.GoogleCategoryLevel1
,QOHFilled.CategoryID
,QOHFilled.Correlation
,QOHFilled.DimDateID
,QOHFilled.WeekStartDate
,QOHFilled.FiscalYearWeek
,QOHFilled.WeekNumber
,QOHFilled.FiscalYear
,QOHFilled.FiscalMonth
,QOHFilled.FiscalYearMonth
,QOHFilled.ItemsShipped
,QOHFilled.QuantityOnHandFilled AS QuantityOnHand
,QOHFilled.Seasonality
,QOHFilled.CumulativeSum
,QOHFilled.ActiveStatus
,QOHFilled.InStockStatus
,QOHFilled.AnnualSales
,CASE WHEN QOHFilled.InStockStatus = 'InStock' OR QOHFilled.InStockStatus = 'Inactive' THEN QOHFilled.Seasonality ELSE 0 END AS WeeklyInStockSeasonality
FROM QOHFilled
),

InStockSeasonality AS (
SELECT
RelevantVariables.*
,ROUND(SUM(RelevantVariables.WeeklyInStockSeasonality) OVER (PARTITION BY RelevantVariables.DimShippedProductID ORDER BY RelevantVariables.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING), 4) AS InStockSeasonality
FROM RelevantVariables
ORDER BY RelevantVariables.DimShippedProductID, RelevantVariables.WeekStartDate
),

AnnualSalesOOS AS (
SELECT
InStockSeasonality.*
,CASE
WHEN InStockSeasonality >= .9999 THEN AnnualSales
WHEN (InStockSeasonality < .1) THEN (AnnualSales / .1)
ELSE (AnnualSales / InStockSeasonality)
END AS AnnualSalesOOS
FROM InStockSeasonality
),

FinalQuery AS (
SELECT
AnnualSalesOOS.*
,CASE WHEN InStockStatus = 'OOS' THEN Seasonality * AnnualSalesOOS ELSE ItemsShipped END AS WeeklySalesOOS
,CURRENT_TIMESTAMP() AS TodaysDate
FROM AnnualSalesOOS
LEFT JOIN ThisWeek ON ThisWeek.TodaysDate = TodaysDate
WHERE DATE(AnnualSalesOOS.WeekStartDate) < DATE(ThisWeek.ThisWeek)
ORDER BY Supplier, DimShippedProductID, WeekStartDate
)

SELECT
VendorCategory
,Supplier
,PartNumber
,ItemID
,DimShippedProductID AS DimProductID
,GoogleCategoryLevel1
,CategoryID
,Correlation
,DimDateID
,FiscalYearWeek
,FiscalYear
,WeekNumber
,WeekStartDate
,FiscalYearMonth
,FiscalMonth
,ActiveStatus
,InStockStatus
,Seasonality
,COALESCE(InStockSeasonality, 0) AS InStockSeasonality
,QuantityOnHand
,COALESCE(ItemsShipped, 0) AS ItemsShipped
,COALESCE(WeeklySalesOOS, 0) AS WeeklySalesOOS
,COALESCE(AnnualSales, 0) AS AnnualSales
,COALESCE(AnnualSalesOOS, 0) AS AnnualSalesOOS
,DATE('2022-02-20') AS created_on
FROM FinalQuery
WHERE WeekStartDate >= '2015-12-27'
ORDER BY Supplier, DimProductID, WeekStartDate
"""
)
input_oossales.createOrReplaceTempView('input_oossales')

# COMMAND ----------

# get min/max holdout dates in training data + date the training data was updated
holdout_dates = spark.sql("""select CAST((max(WeekStartDate) - 112) as string) as holdout_min_date, CAST(max(WeekStartDate) as string) as holdout_max_date, CAST(max(created_on) as string) as created_on from input_oossales""")

# holdout date variables
min_date = """'2018-01-01'"""
holdout_min_date = """'{}'""".format(holdout_dates.first()['holdout_min_date'])
holdout_max_date = """'{}'""".format(holdout_dates.first()['holdout_max_date'])
created_on = """'{}'""".format(holdout_dates.first()['created_on'])

# COMMAND ----------

min_date, holdout_min_date, holdout_max_date, created_on

# COMMAND ----------

# replenished and non-replenished data
if replenished is True:
    # replenished with NPR
    raw_data = spark.sql("""select
        thor.created_on,
        thor.DimProductID,
        thor.WeekStartDate,
        thor.WeekNumber,
        thor.FiscalYear,
        thor.WeeklySalesOOS,
        npr.Forecast as npr_forecast,
        coalesce(npr.npr_sku, 'not_npr') as npr_sku
        from
        (
        select * from  input_oossales where ItemID in (select distinct(ItemID) from silver_thor.itemid_procurementstatus_list where ProcurementStatus in ('Replenish', 'Replenish - Reset', 'Replenish - Amplify') and ItemID in (select distinct(ItemID) from bronze_spreedw.dim_product where ListingType == 'Individual')) and WeekStartdate <= {0} and WeekStartDate >= {1}
        ) thor
        left join
        (select *, 'npr_sku' as npr_sku from silver_thor.fcst_npr_20220220) npr
        on thor.DimProductID = npr.id and thor.WeekStartDate = npr.ts""".format(holdout_max_date, min_date))

    # create sql temp table
    raw_data.createOrReplaceTempView('sql_raw_data')
    
    # convert data to pandas
    raw_data = raw_data.toPandas()

elif replenished is False:
    # non-replenished with NPR
    raw_data = spark.sql("""select
        thor.created_on,
        thor.DimProductID,
        thor.WeekStartDate,
        thor.WeekNumber,
        thor.FiscalYear,
        thor.WeeklySalesOOS,
        npr.Forecast as npr_forecast,
        coalesce(npr.npr_sku, 'not_npr') as npr_sku
        from
        (
        select * from input_oossales where ItemID in (select distinct(ItemID) from silver_thor.itemid_procurementstatus_list where ProcurementStatus in ('Not Ordering - Other', 'Not Ordering - Cost Improvement', 'Not Ordering - Low Volume', 'Not Ordering - Other','Not Ordering - Discontinued') and ItemID in (select distinct(ItemID) from bronze_spreedw.dim_product where ListingType == 'Individual')) and WeekStartdate <= {0} and WeekStartDate >= {1}
        ) thor
        left join
        (select *, 'npr_sku' as npr_sku from silver_thor.fcst_npr_20220220) npr
        on thor.DimProductID = npr.id and thor.WeekStartDate = npr.ts""".format(holdout_min_date, min_date))

    # create sql temp table
    raw_data.createOrReplaceTempView('sql_raw_data')
    
    # convert data to pandas
    raw_data = raw_data.toPandas()

# COMMAND ----------

# replenish vs non-replenished column name
if replenished is True:
    model_dbx_table_name = 'gold_thor.fcst_model_results'
    best_model_dbx_table_name = 'gold_thor.fcst_best_model_results'
elif replenished is False:
    model_dbx_table_name = 'gold_thor.fcst_model_results_non_replenished'
    best_model_dbx_table_name = 'gold_thor.fst_best_model_results_non_replenished'

# COMMAND ----------

#applies to npr_forecast - fcst_npr does not include created_on
external_npr_forecast = spark.sql("""
select 
npr_2.*,
rd.WeeklySalesOOS
from
(
select 
npr.id,
npr.ts,
npr.forecast,
npr.model,
npr.created_on,
case
when npr.ts <= {0} then 'Train'
when npr.ts > {0} then 'Forecast'
end as label
from silver_thor.fcst_npr_20220220 npr
inner join (select DimProductID from sql_raw_data group by DimProductID) as criteria
on npr.id = criteria.DimProductID
) npr_2
left join (select DimProductID, WeekStartDate, WeeklySalesOOS from sql_raw_data) rd
on npr_2.id = rd.DimProductID and npr_2.ts = rd.WeekStartDate
""".format(holdout_max_date))

#print('Unique DimProductID: ', external_npr_forecast['DimProductID'].nunique())

# COMMAND ----------

#applies to npr_forecast - fcst_vcs includes created_on
external_vcs_forecast = spark.sql("""
select 
vcs_2.*,
rd.WeeklySalesOOS
from
(
select 
vcs.id,
vcs.ts,
vcs.forecast,
vcs.model,
vcs.created_on,
case
when vcs.ts <= {0} then 'Train'
when vcs.ts > {0} then 'Forecast'
end as label
from silver_thor.fcst_vcs_20220220 vcs
inner join (select DimProductID from sql_raw_data group by DimProductID) as criteria
on vcs.id = criteria.DimProductID
) vcs_2
left join (select DimProductID, WeekStartDate, WeeklySalesOOS from sql_raw_data) rd
on vcs_2.id = rd.DimProductID and vcs_2.ts = rd.WeekStartDate
""".format(holdout_max_date))

#print('Unique DimProductID: ', external_vcs_forecast['DimProductID'].nunique())

# COMMAND ----------

# MAGIC %md ##### Step 4: data cleaning

# COMMAND ----------

# filter data for features, time_input, target
data = raw_data[['DimProductID','WeekStartDate','WeekNumber','FiscalYear','WeeklySalesOOS','npr_forecast', 'npr_sku', 'created_on']]

# COMMAND ----------

# clean NAN to 0 for target variable
data = Preprocess.clean_missing(data, target)

# COMMAND ----------

# MAGIC %md ##### Step 5: preprocess - setup model fitting for parallelization

# COMMAND ----------

# group by per sku, date, and targetk
inputs = Preprocess(time_grain, holdout_length, target, features, time_input).group_for_parallel(data=data, response=target, group=sku_id)

# COMMAND ----------

# MAGIC %md ##### Step 6: parallelize train test split

# COMMAND ----------

# DBTITLE 0,Parallelize Train Test Split
par_data = sc.parallelize(inputs,100).map(lambda input: Train_Test(time_grain, holdout_length).train_test_label(input, time_input)).collect()

# COMMAND ----------

# only include forecast and holdout data
def only_forecast(data):
    only_forecast_data = data[(data['label'] != 'Train')]
    return(only_forecast_data)

# COMMAND ----------

# MAGIC %md ##### Step 7: parallelized model fitting

# COMMAND ----------

# DBTITLE 0,Parallelized Model Fitting
forecast_list = sc.parallelize(par_data, 100).map(lambda par: Orchestrate(forecast_horizon, holdout_length, sku_id, time_input, forecast_output, forecast_type, target, features, time_grain, models).compile_results(input=par)).map(lambda par: only_forecast(data=par)).collect()

# COMMAND ----------

# length of orignal forecast list (gives SKU count)
len(forecast_list)

# COMMAND ----------

# MAGIC %md ##### Step 8: post-process model cleaning

# COMMAND ----------

# DBTITLE 0,All Forecast Model
# flatten the list of dataframes into a single dataframe
forecast = pd.concat(forecast_list)

forecast_columns = ['created_on', 'id', 'model', 'label', 'ts_category', 'error', 'ts', 'data_length', 'training_length', 'holdout_length', 'y_train', 'fitted_values', 'holdout_forecast', 'five_forecast', 'nine_forecast', 'thirteen_forecast', 'fifty_two_forecast', 'holdout_mae', 'holdout_mse', 'holdout_mape', 'holdout_smape', 'holdout_wfa']

result_schema = StructType([
    StructField('created_on', DateType(), True),
    StructField('id', IntegerType(), True), 
    StructField('model', StringType(), True),
    StructField('label', StringType(), True),
    StructField('ts_category', StringType(), True),
    StructField('error', StringType(), True),
    StructField('ts', DateType(), True),
    StructField('data_length', IntegerType(), True),
    StructField('training_length', IntegerType(), True),
    StructField('holdout_length', IntegerType(), True),
    StructField('y_train', DoubleType(), True),
    StructField('fitted_values', DoubleType(), True),
    StructField('holdout_forecast', DoubleType(), True),
    StructField('five_forecast', DoubleType(), True),
    StructField('nine_forecast', DoubleType(), True),
    StructField('thirteen_forecast', DoubleType(), True),
    StructField('fifty_two_forecast', DoubleType(), True),
    StructField('holdout_mae', DoubleType(), True),
    StructField('holdout_mse', DoubleType(), True),
    StructField('holdout_mape', DoubleType(), True),
    StructField('holdout_smape', DoubleType(), True),
    StructField('holdout_wfa', DoubleType(), True)
    ])

# convert into spark dataframe + apply replace_infs
spark_forecast = spark.createDataFrame(forecast[forecast_columns], schema = result_schema).withColumn('holdout_mape', replace_infs(col('holdout_mape'), lit(0))).withColumn('holdout_forecast', when(col('holdout_forecast') < 0, 0).otherwise(col('holdout_forecast'))).withColumn('five_forecast', when(col('five_forecast') < 0, 0).otherwise(col('five_forecast'))).withColumn('nine_forecast', when(col('nine_forecast') < 0, 0).otherwise(col('nine_forecast'))).withColumn('thirteen_forecast', when(col('thirteen_forecast') < 0, 0).otherwise(col('thirteen_forecast')))

# spark sql dataframe
spark_forecast.createOrReplaceTempView('spark_forecast_temp')

# spark sql clip forecast values for negative to 0, fill na values
spark.sql("""
SELECT
created_on,
id,
model,
-- fill  na labels with 'Forecast'
COALESCE('Forecast', label) AS label,
ts_category,
error,
ts,
data_length,
training_length,
holdout_length,
y_train,
fitted_values,
-- clip negative values to 0
holdout_forecast,
five_forecast,
nine_forecast,
thirteen_forecast,
fifty_two_forecast,
-- fill na holdout_mape with 0
holdout_mae,
holdout_mse,
COALESCE(0, holdout_mape) AS holdout_mape,
holdout_smape,
holdout_wfa
FROM spark_forecast_temp
""").createOrReplaceTempView('spark_forecast')

# COMMAND ----------

# MAGIC %md ##### Step 9: merge external forecast

# COMMAND ----------

# test spark version npr
spark_npr = external_npr_forecast.groupBy('id').apply(ExternalForecast.npr_spark_wrapper)

# spark sql dataframe
spark_npr.createOrReplaceTempView('spark_npr') 

# COMMAND ----------

# test spark version vcs - vcs table matches thor columns
spark_vcs = external_vcs_forecast.groupBy('id').apply(ExternalForecast.vcs_spark_wrapper)

# spark sql dataframe
spark_vcs.createOrReplaceTempView('spark_vcs')

# COMMAND ----------

# join vcs, npr, thor
spark.sql("""
SELECT
*
FROM spark_forecast
UNION ALL
SELECT 
*
FROM spark_npr
UNION ALL
SELECT 
*
FROM spark_vcs""").createOrReplaceTempView('forecast_sql')

# COMMAND ----------

#forecast = spark_forecast.union(spark_vcs).union(spark_npr)

# COMMAND ----------

# MAGIC %md ##### Step 10: spark database 

# COMMAND ----------

# write to gold all forecast results

# spark query
query = 'CREATE OR REPLACE TABLE {} AS SELECT * FROM {}'.format(model_dbx_table_name, 'forecast_sql')

# spark sql
spark.sql(query)

print('Successfully stored {}'.format(model_dbx_table_name))

# COMMAND ----------

# NO ARCHIVING FOR FIRST RUN
# insert forecast into archive
spark.sql("""
CREATE OR REPLACE TABLE  gold_thor.fcst_best_model_results_archive 
AS
SELECT * FROM gold_thor.fcst_best_model_results
UNION ALL 
SELECT * FROM gold_thor.fcst_best_model_results_archive""")

# COMMAND ----------

# Best Model Query - chosen by the SMAPE because MAPE can be zero or inf

# spark query
spark_query = """
SELECT
DATE(a.created_on), 
b.id, 
a.ts, 
a.model, 
a.label, 
a.ts_category, 
a.data_length, 
a.training_length, 
a.holdout_length, 
a.error, 
a.y_train,
a.five_forecast, 
a.nine_forecast, 
a.thirteen_forecast,
a.adj_thirteen_forecast,
a.fifty_two_forecast, 
a.adj_fifty_two_forecast,
a.positive_margin_sales_percent,
a.holdout_forecast, 
a.holdout_mae, 
a.holdout_mse, 
a.holdout_mape, 
b.holdout_smape, 
a.holdout_wfa
  FROM
  (
  SELECT 
  fcst.*, 
  margins.`PositiveMarginSales%` AS positive_margin_sales_percent, 
  (margins.`PositiveMarginSales%` * fcst.thirteen_forecast) AS adj_thirteen_forecast, 
  (margins.`PositiveMarginSales%` * fcst.fifty_two_forecast) AS adj_fifty_two_forecast 
    FROM {0} AS fcst 
    LEFT JOIN silver_thor.ref_positivemargin AS margins ON fcst.id = margins.DimProductID
  ) AS a
  INNER JOIN (SELECT 
              id, 
              min(holdout_smape) AS holdout_smape 
                FROM {0}
                GROUP BY id) AS b 
  ON a.id = b.id AND a.holdout_smape = b.holdout_smape 
  WHERE a.ts >= {1}
  ORDER BY b.id, ts ASC
""".format(model_dbx_table_name, created_on)

# spark sql
spark.sql(spark_query).createOrReplaceTempView('spark_best_model')

# create dbx table
spark_best_model_query = 'CREATE OR REPLACE TABLE {0} AS SELECT * FROM {1}'.format(best_model_dbx_table_name, 'spark_best_model')
spark.sql(spark_best_model_query)
    
print('Successfully stored {}'.format(best_model_dbx_table_name))

# COMMAND ----------

# MAGIC %md ##### Step 11: process best model table as demand planning ready + adjustments with network to node disaggregation

# COMMAND ----------

# DBTITLE 0,Post-Process: Disaggregate Network to Node Gold Table
# MAGIC %run ./post_process_demand_planning_output_test
