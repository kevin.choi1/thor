# Databricks notebook source
# MAGIC %md ##### Controller: Executes Thor Forecasting Engine

# COMMAND ----------

# use for hyperopt tuning
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from pyspark import SparkContext, SparkConf

# COMMAND ----------

# MAGIC %md ##### Step 1: import modules

# COMMAND ----------

# MAGIC %run ./config/run_config

# COMMAND ----------

# MAGIC %run ./src/pre_process

# COMMAND ----------

# MAGIC %run ./src/post_process

# COMMAND ----------

# MAGIC %run ./src/train_test

# COMMAND ----------

# MAGIC %run ./src/fitter

# COMMAND ----------

# MAGIC %run ./src/orchestrate

# COMMAND ----------

# MAGIC %run ./src/external_forecast

# COMMAND ----------

# MAGIC %md ##### Step 2: input parameters

# COMMAND ----------

# input parameters as variables
time_grain = parameters['time_grain']
features = parameters['features']
npr_forecast = parameters['npr_forecast']
level = parameters['level']
models = parameters['models']
tuning = parameters['tuning']
input_type = parameters['input_type']
target = parameters['target']['csoh']
target_secondary = parameters['target']['soh']
time_input = parameters['time_input']['weekly']
sku_id = parameters['sku_id']['DimProductID']
holdout_length = parameters['holdout_length']['weekly']
forecast_horizon = parameters['forecast_horizon']['weekly']
forecast_output = parameters['forecast_output']
tuning = tuning_parameters['tuning']
params = tuning_parameters['hyper_opt']
forecast_type = parameters['forecast_type']
created_on = parameters['created_on']
replenished = True

# COMMAND ----------

# MAGIC %md ##### Step 3: data ingestion

# COMMAND ----------

# query based on sku_list and replenish - ItemID count 16442 and only individual listing type
if replenished is False:
    raw_data = spark.sql("""select * from silver_thor.input_elements where ItemID in (select distinct(ItemID) from silver_thor.itemid_procurementstatus_list where ProcurementStatus in ('Replenish', 'Replenish - Reset', 'Replenish - Amplify') and ItemID in (select distinct(ItemID) from bronze_spreedw.dim_product where ListingType == 'Individual')) and WeekStartdate <= '2021-08-01' and WeekStartDate >= '2018-01-01'""").toPandas()

elif replenished is False:
    raw_data = spark.sql("""select * from silver_thor.input_elements where ItemID in (select distinct(ItemID) from bronze_spreedw.dim_product where ListingType == 'Individual') and WeekStartdate <= '2021-08-08' and WeekStartDate >= '2018-01-01'""").toPandas()

# COMMAND ----------

# replenished with NPR
raw_data = spark.sql("""select
thor.*,
npr.Forecast as npr_forecast,
coalesce(npr.npr_sku, 'not_npr') as npr_sku
from
(
select * from silver_thor.input_elements where ItemID in (select distinct(ItemID) from silver_thor.itemid_procurementstatus_list where ProcurementStatus in ('Replenish', 'Replenish - Reset', 'Replenish - Amplify') and ItemID in (select distinct(ItemID) from bronze_spreedw.dim_product where ListingType == 'Individual')) and WeekStartdate <= '2021-08-01' and WeekStartDate >= '2018-01-01'
) thor
left join
(select *, 'npr_sku' as npr_sku from silver_thor.fcst_npr) npr
on thor.DimProductID = npr.DimProductID and thor.WeekStartDate = npr.WeekStartDate""")

raw_data.createOrReplaceTempView('sql_raw_data')
raw_data = raw_data.toPandas()

# COMMAND ----------

# joins only ids that are in the raw_data query; includes both forecast and historical data
min_training_data = """'2021-01-01'"""
last_forecast_date = """'2021-08-01'"""

#applies to npr_forecast
external_forecast = spark.sql("""
select 
npr_2.*,
rd.WeeklySalesOOS
from
(
select 
npr.*,
case
when npr.WeekStartDate <= {0} then 'Train'
when npr.WeekStartDate > {0} then 'Forecast'
end as label
from silver_thor.fcst_npr npr
inner join (select DimProductID from sql_raw_data group by DimProductID) as criteria
on npr.DimProductID = criteria.DimProductID
) npr_2
left join (select DimProductID, WeekStartDate, WeeklySalesOOS from sql_raw_data) rd
on npr_2.DimProductID = rd.DimProductID and npr_2.WeekStartDate = rd.WeekStartDate
""".format(last_forecast_date)).toPandas()

print(external_forecast['DimProductID'].nunique())

# COMMAND ----------

len(raw_data)

# COMMAND ----------

# MAGIC %md ##### Step 4: data cleaning

# COMMAND ----------

# filter data for features, time_input, target
data = raw_data[['DimProductID', 'Price', 'OperationCost', 'WeekStartDate', 'BuyBoxPercentageAmazon', 'WeekNumber', 'FiscalYear', 'WeeklySalesOOS', 'npr_forecast', 'npr_sku']]

# COMMAND ----------

# clean NAN to 0 for target variable
data = Preprocess.clean_missing(data, target)

# COMMAND ----------

# MAGIC %md ##### Step 5: preprocess - setup model fitting for parallelization

# COMMAND ----------

# group by per sku, date, and targetk
inputs = Preprocess(time_grain, holdout_length, target, features, time_input).group_for_parallel(data=data, response=target, group=sku_id)

# COMMAND ----------

# MAGIC %md ##### Step 6: parallelize train test split

# COMMAND ----------

# DBTITLE 0,Parallelize Train Test Split
par_data = sc.parallelize(inputs,100).map(lambda input: Train_Test(time_grain, holdout_length).train_test_label(input, time_input)).collect()

# COMMAND ----------

# MAGIC %md ##### Step 7: parallelized model fitting

# COMMAND ----------

# DBTITLE 0,Parallelized Model Fitting
forecast_list = sc.parallelize(par_data, 100).map(lambda par: Orchestrate(forecast_horizon, holdout_length, sku_id, time_input, forecast_output, forecast_type, target, features, time_grain, models).compile_results(input=par)).collect()

# COMMAND ----------

# length of orignal forecast list (gives SKU count)
len(forecast_list)

# COMMAND ----------

# MAGIC %md ##### Step 8: post-process model cleaning

# COMMAND ----------

# DBTITLE 0,All Forecast Model
forecast = pd.concat(forecast_list)

# COMMAND ----------

# MAGIC %md ##### Step 9: merge external forecast

# COMMAND ----------

external_forecast_group = ExternalForecast(model = 'npr', ts_category = 'npr', id_name = sku_id, ts_name = time_input, forecast_horizon = forecast_horizon).general_group_for_parallel(data = external_forecast, group = sku_id)

# COMMAND ----------

external_forecast_flat = pd.concat(external_forecast_group)

# COMMAND ----------

external_forecast_final = external_forecast_flat[['label','id', 'ts', 'model', 'ts_category', 'error', 'data_length', 'training_length', 'holdout_length', 'fitted_values',	'y_train','five_forecast','nine_forecast','thirteen_forecast','fifty_two_forecast','holdout_mae','holdout_mse','holdout_mape','holdout_smape','holdout_wfa']]

# COMMAND ----------

forecast = pd.concat([forecast, external_forecast_final])

# COMMAND ----------

# floor and turn forecast values into INT
from numpy import inf

forecast_columns = ['fitted_values','holdout_forecast','five_forecast', 'nine_forecast', 'thirteen_forecast', 'fifty_two_forecast']

forecast[forecast_columns] = forecast[forecast_columns].clip(lower=0)

# All NaN values will be labeled foreast
forecast['label'].fillna('Forecast', inplace=True)
forecast['holdout_mape'].replace([np.inf, -np.inf], 0, inplace=True)
forecast['created_on'] = created_on

# COMMAND ----------

# MAGIC %md ##### Step 10: spark database 

# COMMAND ----------

# DBTITLE 1,Write to gold all forecast results
spark_forecast = spark.createDataFrame(forecast)
spark_forecast.write.mode("overwrite").saveAsTable('gold_thor.model_forecast_results')

df3.write.mode("overwrite").saveAsTable("example")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from gold_thor.model_forecast_results

# COMMAND ----------

# DBTITLE 1,Best Model Query - chosen by the SMAPE because MAPE can be zero or inf
spark_best_model = spark.sql("""
select b.id, a.created_on, a.ts, a.model, a.label, a.ts_category, a.data_length, a.training_length, a.holdout_length, a.fitted_values, a.y_train, a.error, a.five_forecast, a.nine_forecast, a.thirteen_forecast, a.fifty_two_forecast,
a.y_test, a.holdout_forecast, a.holdout_mae, a.holdout_mse, a.holdout_mape, b.holdout_smape, a.holdout_wfa from gold_thor.model_forecast_results a
inner join
(select id, min(holdout_smape) as holdout_smape from gold_thor.model_forecast_results group by id) b
on a.id = b.id
and a.holdout_smape = b.holdout_smape
order by b.id, ts asc
""")

spark_best_model.write.mode("overwrite").saveAsTable('gold_thor.best_model_forecast_results')

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from gold_thor.model_forecast_results
