# Databricks notebook source
# MAGIC %md ##### Controller: Executes Thor Forecasting Engine

# COMMAND ----------

# import Thor

# COMMAND ----------

# MAGIC %md ##### Step 1: Import Modules

# COMMAND ----------

# MAGIC %run ./Configurations/run_config

# COMMAND ----------

# MAGIC %run ./Pre_Processing/pre_process

# COMMAND ----------

# MAGIC %run ./Post_Processing/post_process

# COMMAND ----------

# MAGIC %run ./Train_Test_Split/train_test

# COMMAND ----------

# MAGIC %run ./Fitter

# COMMAND ----------

# MAGIC %md ##### Step 2: Input Parameters

# COMMAND ----------

# input parameters
parameters = {'time_grain': 'W',
              'level': 'sku',
              'models': ['ewm', 'sarimax', 'lazyprophet'],
              'tuning': 'gridsearch',
              'input_type': 'univariate',
              'target': {'csoh': 'WeeklySalesOOS', 'soh': 'AnnualSalesOOSAdjusted'},
              'features': {'time': 'new_date'},
              'time_input': {'weekly': 'WeekStartDate'},
              'sku_id': {'Part_Number': 'PartNumber'},
              'holdout_length': {'weekly': 52},
              'forecast_horizon': {'weekly': [4, 8, 12]},
              'forecast_output': True
              }

# COMMAND ----------

tuning_parameters = {'tuning': True,
                     'grid_search': 
                             {
                                 'ewm': {'alpha': [.1, .3, .5, .7, .9]},
                                 'sarimax': {'p': [0, 1, 2], 'd': [0, 1], 'q': [0, 1, 2], 'P': [0, 1, 2], 'D': [0, 1], 'Q': [0, 1, 2], 'trend': ['n','c','t','ct']},
                                 'lazyprophet': {'regularization ': [1, 1.2, 1.4, 1.6, 1.8], 'trend_dampening': [0, .5, 1]}}}

# COMMAND ----------

tuning_parameters['grid_search']['ewm']

# COMMAND ----------

# input parameters as variables
time_grain = parameters['time_grain']
level = parameters['level']
models = parameters['models']
tuning = parameters['tuning']
input_type = parameters['input_type']
target = parameters['target']['csoh']
target_secondary = parameters['target']['soh']
features = parameters['features']['time']
time_input = parameters['time_input']['weekly']
sku_id = parameters['sku_id']['Part_Number']
holdout_length = parameters['holdout_length']['weekly']
forecast_horizon = parameters['forecast_horizon']['weekly']
forecast_output = parameters['forecast_output']
tuning = paramters['tuning']

# COMMAND ----------

# MAGIC %md ##### Step 3: Data Ingestion

# COMMAND ----------

# import data
raw_data = spark.sql("select * from silver_thor.fcst_oos_sales_qoh where PartNumber in ('1310', '601-BOOST', '701-BOOST', '35160')").toPandas()

# COMMAND ----------

# label train/test
data = Train_Test(time_grain, holdout_length, target, features).train_test_label(raw_data)

# COMMAND ----------

# clean NAN to 0
data = Preprocess.clean_missing(data, target)

# COMMAND ----------

# group by per sku, date, and target
inputs = Preprocess.group_for_parallel(data=data, response=target, group=sku_id, time_input=features)

# COMMAND ----------

# DBTITLE 1,MAIN LOOP: LOOP THROUGH EACH MODEL PER SKU
# loop through skus
#warnings.filterwarnings("ignore")
import warnings
warnings.filterwarnings("ignore")

forecast_dictionary = dict()
forecast_dataframe = pd.DataFrame()
forecast_list = list()

for input in inputs:    
    # parameters for each input
    id = input[sku_id].min()
    start = input[time_input].min()
    
    # used to calculate the time series from start - end
    data_length = len(input)
    
    # Train test split (last 52 weeks is test)
    train_test_obj = Train_Test(time_grain, holdout_length, target, features)
    train_test_ready = train_test_obj.train_test_label(input)
    train, test = train_test_obj.train_test_split(train_test_ready)
    
    # Test set with target
    y_test = test[target]
    
    # Date column using the 90 day forecast
    time_series = Preprocess.start_end_date(data_length=data_length, start=start,forecast_horizon=forecast_horizon[2], date_type=time_grain)
    
    # Create model list to append to forecast_dictionary
    model_output_list = {}
    
    # length of test set used later to reindex
    train_length = len(train)
    test_length = len(test)
    
    # Iterate through the model list
    for model in models:
        # create model dictionary with model name, y, fitted values, forecast, and errors
        model_dictionary = Fitter(holdout_length = holdout_length, forecast_horizon=forecast_horizon, forecast_output=forecast_output).model_fit_fcst(train=train, target=target, model=model,time_input=time_input, features=features)
        
        # create column for y_test
        model_dictionary['y_test'] = np.array(y_test)
        # create weekly dates from start to end (end of forecast)
        model_dictionary['time_series'] = np.array(time_series)
        # Init post process   
        postprocess_obj = Postprocess(holdout_length=holdout_length,forecast_horizon=forecast_horizon)
        model_results = postprocess_obj.compile_results(fit_results=model_dictionary,train_length=train_length,test_length=test_length,columns=['date_length','thirty_forecast', 'sixty_forecast', 'ninety_forecast','y_test'])
        model_results['id'] = id
        model_results['model'] = model
        # appended dataframe model_results
        forecast_list.append(model_results)

# COMMAND ----------

# MAGIC %md ##### Step 4: Post-Processing

# COMMAND ----------

forecast = pd.concat(forecast_list)

# COMMAND ----------

# MAGIC %md ##### Step 5: EDA

# COMMAND ----------

forecast[(forecast['id'] == '601-BOOST') & (forecast['model'] == 'sarimax')].head(5)

# COMMAND ----------

lazyprophet = forecast[(forecast['id'] == '601-BOOST') & (forecast['model'] == 'lazyprophet')]
ewm = forecast[(forecast['id'] == '601-BOOST') & (forecast['model'] == 'ewm')]
sarimax = forecast[(forecast['id'] == '601-BOOST') & (forecast['model'] == 'sarimax')]

# COMMAND ----------

# ewm 601-BOOST
ewm_fitted_values = ewm['fitted_values']
ewm_thirty_forecast = ewm['thirty_forecast']
ewm_sixty_forecast = ewm['sixty_forecast']
ewm_ninety_forecast = ewm['ninety_forecast']
y_ewm_train = ewm['y_train']
y_ewm_test = ewm['y_test']

# COMMAND ----------

# sarimax 601-BOOST
sarimax_fitted_values = sarimax['fitted_values']
sarimax_thirty_forecast = sarimax['thirty_forecast']
sarimax_sixty_forecast = sarimax['sixty_forecast']
sarimax_ninety_forecast = sarimax['ninety_forecast']
y_sarimax_train = sarimax['y_train']
y_sarimax_test = sarimax['y_test']

# COMMAND ----------

# lazyprophet 601-BOOST
lp_fitted_values = lazyprophet['fitted_values']
lp_thirty_forecast = lazyprophet['thirty_forecast']
lp_sixty_forecast = lazyprophet['sixty_forecast']
lp_ninety_forecast = lazyprophet['ninety_forecast']
y_lp_train = lazyprophet['y_train']
y_lp_test = lazyprophet['y_test']

# COMMAND ----------

len(lp_fitted_values), len(lp_forecast), len(y_lp_train), len(y_lp_test)

# COMMAND ----------

# ewm 601-BOOST
plt.plot(np.append(y_ewm_train, y_ewm_test ), label='actuals', color='blue')
plt.plot(np.append(ewm_fitted_values, ewm_forecast), label='predicted', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# sarimax 601-BOOST
plt.plot(np.append(y_sarimax_train, y_sarimax_test ), label='actuals', color='blue')
plt.plot(np.append(sarimax_fitted_values, sarimax_forecast), label='predicted', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# lp 601-BOOST
# need to remove nan from forecasts plus y_lp_test
plt.plot(np.append(y_lp_train, y_lp_test ), label='actuals', color='blue')
plt.plot(np.append(lp_fitted_values, lp_thirty_forecast), label='30_predicted', color='orange')
plt.plot(np.append(lp_fitted_values, lp_sixty_forecast), label='60_predicted', color='green')
plt.plot(np.append(lp_fitted_values, lp_ninety_forecast), label='90_predicted', color='black')
plt.legend()
plt.show()
