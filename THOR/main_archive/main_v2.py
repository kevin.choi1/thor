# Databricks notebook source
# MAGIC %md ##### Controller: Executes Thor Forecasting Engine

# COMMAND ----------

# import Thor
#from sklearn.model_selection import GridSearchCV
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials

# COMMAND ----------

# MAGIC %md ##### Step 1: Import Modules

# COMMAND ----------

# MAGIC %run ./Configurations/run_config

# COMMAND ----------

# MAGIC %run ./Pre_Processing/pre_process

# COMMAND ----------

# MAGIC %run ./Post_Processing/post_process

# COMMAND ----------

# MAGIC %run ./Train_Test_Split/train_test

# COMMAND ----------

# MAGIC %run ./Models/Ewm

# COMMAND ----------

# MAGIC %run ./Models/Sarimax

# COMMAND ----------

# MAGIC %run ./Models/LazyProphet

# COMMAND ----------

# MAGIC %run ./Fitter

# COMMAND ----------

# MAGIC %md ##### Step 2: Input Parameters

# COMMAND ----------

# input parameters
parameters = {
              'forecast_type': {'univariate': True, 'multivariate': True},
              'features': ['Price', 'OperationCost'],
              'time_grain': 'W',
              'level': 'sku',
              'models': ['ewm', 'sarimax', 'lazyprophet'],
              'tuning': 'gridsearch',
              'input_type': 'univariate',
              'target': {'csoh': 'WeeklySalesOOS', 'soh': 'AnnualSalesOOSAdjusted'},
              'time_input': {'weekly': 'WeekStartDate'},
              'sku_id': {'Part_Number': 'PartNumber', 'DimProductID': 'DimProductID'},
              'holdout_length': {'weekly': 16},
              'forecast_horizon': {'weekly': [5, 9, 13]},
              'forecast_output': True
              }

# COMMAND ----------

tuning_parameters = {'tuning': True,
                     'hyper_opt': 
                             {
                                 'ewm': {'alpha': [.1, .3, .5, .7, .9]},
                                 'sarimax': {
                                  'p':     hp.randint('p', 4),
                                  'd':     hp.randint('d', 2),
                                  'q':     hp.randint('q', 4),
                                  'cp':   hp.randint('cp', 2),
                                  'cd':   hp.randint('cd', 2),
                                  'cq':   hp.randint('cq', 2),
                                  'trend': hp.choice('trend', ['n', 'c', 't', 'ct']),
                                  'transformation': hp.choice('transformation', [None]),
                                  'model_type': hp.choice('model_choice', ['univariate', 'multivariate'])}, 
                                 'lazyprophet': {'regularization': [1, 1.2, 1.4, 1.6, 1.8], 'trend_dampening': [0, .5, 1], 'transformation': [None]}}}

# COMMAND ----------

# input parameters as variables
time_grain = parameters['time_grain']
features = parameters['features']
level = parameters['level']
models = parameters['models']
tuning = parameters['tuning']
input_type = parameters['input_type']
target = parameters['target']['csoh']
target_secondary = parameters['target']['soh']
time_input = parameters['time_input']['weekly']
sku_id = parameters['sku_id']['DimProductID']
holdout_length = parameters['holdout_length']['weekly']
forecast_horizon = parameters['forecast_horizon']['weekly']
forecast_output = parameters['forecast_output']
tuning = tuning_parameters['tuning']
params = tuning_parameters['hyper_opt']
forecast_type = parameters['forecast_type']

# COMMAND ----------

# MAGIC %md ##### Step 3: Data Ingestion

# COMMAND ----------

# import data
if forecast_type['multivariate']:
    raw_data = spark.sql("select * from silver_thor.input_elements where GoogleCategoryLevel1 in ('Home & Garden', 'Health & Beauty') and WeekStartdate <= '2021-08-01' and WeekStartDate >= '2018-01-01'").toPandas()
else:
    raw_data = spark.sql("select * from silver_thor.fcst_oos_sales_qoh where PartNumber in ('1310', '601-BOOST', '701-BOOST', '35160') and WeekStartdate <= '2021-08-01' and WeekStartDate >= '2018-01-01' ").toPandas()

# COMMAND ----------

# filter data for features, time_input, target
data = raw_data[['DimProductID','Price', 'OperationCost', 'WeekStartDate', 'BuyBoxPercentageAmazon', 'WeekNumber', 'FiscalYear', 'WeeklySalesOOS']]

# COMMAND ----------

# clean NAN to 0
data = Preprocess.clean_missing(data, target)

# COMMAND ----------

# group by per sku, date, and target
inputs = Preprocess.group_for_parallel(data=data, response=target, group=sku_id, time_input=features)

# COMMAND ----------

# parameters for each input

def orchestrate(inputs, models, params):
    
    for input in inputs:   
        id = input[sku_id].min()
        start = input[time_input].min()

        # used to calculate the time series from start - end
        data_length = len(input)

        # pre-process
        model_dictionary = {}
        time_series = Preprocess.start_end_date(data_length=data_length, start=start,forecast_horizon=forecast_horizon[2], date_type=time_grain)
        week_number = Preprocess.start_end_week_number(time_series)
        year_number = Preprocess.start_end_year_number(time_series)
        model_dictionary['time_series'] = np.array(time_series)
        model_dictionary['week_number'] = np.array(week_number)
        model_dictionary['year_number'] = np.array(year_number)
        input['avg_30_day_price'] = Preprocess.rolling_thirty_average(input, variable='Price', time_variable=time_input)
        avg_30_day_price = [input['avg_30_day_price'].min()] * len(time_series)
        model_dictionary['avg_price'] = np.array(avg_30_day_price)
        model_dictionary['y_test'] = np.array(y_test)

        # Train test split (last 52 weeks is test)
        train_test_obj = Train_Test(time_grain, holdout_length, target, features)
        train_test_ready = train_test_obj.train_test_label(input)
        train, test = train_test_obj.train_test_split(train_test_ready)

        # Test set with target
        y_train = train[target]
        y_test = test[target]
        x_train = train[['Price']].astype(int).to_numpy()
        # Iterate through the model list
        for model in models:
            # create model dictionary with model name, y, fitted values, forecast, and errors
            model_dictionary = Fitter(holdout_length = holdout_length, forecast_horizon=forecast_horizon, forecast_output=forecast_output).model_fit_fcst(train=train, target=target, model=model,time_input=time_input, features=features, x_train=x_train)

            # create column for y_test
            #model_dictionary['y_test'] = np.array(y_test)
            # create weekly dates from start to end (end of forecast)
            #model_dictionary['time_series'] = np.array(time_series)
            # Init post process   
            postprocess_obj = Postprocess(holdout_length=holdout_length,forecast_horizon=forecast_horizon)
            model_results = postprocess_obj.compile_results(fit_results=model_dictionary,train_length=train_length,test_length=test_length,columns=['date_length','thirty_forecast', 'sixty_forecast', 'ninety_forecast','y_test'])
            model_results['id'] = id
            model_results['model'] = model
            # appended dataframe model_results
            forecast_list.append(model_results)
        
        return(forecast_list)

# COMMAND ----------

# need to check for short time series length

# COMMAND ----------

import warnings
warnings.filterwarnings("ignore")

### Main Program
forecast_list = []

for input in inputs:    
    #####  MAIN CODE
    id = input[sku_id].min()
    start = input[time_input].min()

    # used to calculate the time series from start - end
    data_length = len(input)
    
    if data_length < holdout_length:
        # 
        

     # pre-process
    model_dictionary = {}
    time_series = Preprocess.start_end_date(data_length=data_length, start=start,forecast_horizon=forecast_horizon[2], date_type=time_grain)
    week_number = Preprocess.start_end_week_number(time_series)
    year_number = Preprocess.start_end_year_number(time_series)
    model_dictionary['time_series'] = np.array(time_series)
    model_dictionary['week_number'] = np.array(week_number)
    model_dictionary['year_number'] = np.array(year_number)
    input['avg_30_day_price'] = Preprocess.rolling_thirty_average(input, variable='Price', time_variable=time_input)
    avg_30_day_price = [input['avg_30_day_price'].min()] * len(time_series)
    model_dictionary['avg_price'] = np.array(avg_30_day_price)

    # Train test split (last 52 weeks is test)
    train_test_obj = Train_Test(time_grain, holdout_length, target, features)
    train_test_ready = train_test_obj.train_test_label(input, time_input)
    train, test = train_test_obj.train_test_split(train_test_ready)

    # Test set with target
    
    y_train = train[target]
    y_train = pd.Series(np.array(train[target]), index=np.array(pd.to_datetime(train[time_input])))
    y_train = y_train.asfreq(time_grain)
    y_test = test[target]

    train_length = len(train)
    test_length = len(test)


    time_series = Preprocess.start_end_date(data_length=data_length, start=start,forecast_horizon=forecast_horizon[2], date_type=time_grain)
    week_number = Preprocess.start_end_week_number(time_series)
    year_number = Preprocess.start_end_year_number(time_series)
    model_dictionary['time_series'] = np.array(time_series)
    model_dictionary['week_number'] = np.array(week_number)
    model_dictionary['year_number'] = np.array(year_number)
    model_dictionary_feature = model_dictionary['avg_price'] = np.array(avg_30_day_price)

    if forecast_type['multivariate']:
        x_train = train['Price'].to_numpy().reshape(-1,1)
    else:
        x_train = None
    
    for model in models:
        # Fitter output as model dictionary
        model_dictionary = Fitter(train_length=train_length,holdout_length = holdout_length, forecast_horizon=forecast_horizon, forecast_output=forecast_output).model_fit_fcst(y_train=y_train, y_test=y_test,train=train,target=target, model=model,time_input=time_input, features=features,x_train=x_train, model_dictionary=model_dictionary_feature)
        # Post Process
        postprocess_obj = Postprocess(holdout_length=holdout_length,forecast_horizon=forecast_horizon)
        # Compile Results to fix data structure
        model_results = postprocess_obj.compile_results(fit_results=model_dictionary,train_length=train_length,test_length=test_length,columns=['date_length', 'holdout_forecast', 'thirty_forecast', 'sixty_forecast', 'ninety_forecast','y_test'])
        # Calculate Accuracy
        model_results = postprocess_obj.calculate_accuracy(model_results)
        model_results['id'] = id
        model_results['model'] = model

        # appended dataframe model_results
        forecast_list.append(model_results)

# COMMAND ----------

forecast = pd.concat(forecast_list)

# COMMAND ----------

forecast.head()

# COMMAND ----------

forecast.createDataFrame(forecast)
forecast.createOrReplaceTempView('forecast')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TABLE 
# MAGIC as
# MAGIC SELECT * from gold_thor.forecast

# COMMAND ----------

# multivariate
plt.plot(y_test, label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# price sarimax
plt.plot(model_dictionary['y_test'], label='actuals', color='blue')
plt.plot(model_dictionary['test'], label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# price prophet
plt.plot(model_dictionary['y_test'], label='actuals', color='blue')
plt.plot(model_dictionary['test'], label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# multivariate lazyprophet
plt.plot(np.array(y_test), label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# univariate lazyprophet
plt.plot(np.array(y_test), label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# price lazyprophet
plt.plot(np.array(y_test), label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# lazy prophet test

import quandl
import pandas as pd
import matplotlib.pyplot as plt

#Get bitcoin data
data = quandl.get("BITSTAMP/USD")
#let's get our X matrix with the new variables to use
X = data.drop('Low', axis = 1)
X = X.iloc[-730:,:]
y = data['Low']
y = y[-730:]

thor_model_obj = LazyProphet(freq = 365, 
                            estimator = 'linear', 
                            max_boosting_rounds = 200,
                            approximate_splits = True,
                            regularization = 1.2,
                             exogenous=X)
thor_fitted_values = thor_model_obj.fit(time_series=yn)
test_thor_forecast = thor_model_obj.extrapolate(holdout_length)
thor_model_obj.summary()
