# Databricks notebook source
# MAGIC %md ##### Controller: Executes Thor Forecasting Engine

# COMMAND ----------

# run at the item id level (this is what DP uses)

# COMMAND ----------

# import Thor
#from sklearn.model_selection import GridSearchCV
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials

# COMMAND ----------

# MAGIC %md ##### Step 1: Import Modules

# COMMAND ----------

# MAGIC %run ./Configurations/run_config

# COMMAND ----------

# MAGIC %run ./Pre_Processing/pre_process

# COMMAND ----------

# MAGIC %run ./Post_Processing/post_process

# COMMAND ----------

# MAGIC %run ./Train_Test_Split/train_test

# COMMAND ----------

# MAGIC %run ./Models/Ewm

# COMMAND ----------

# MAGIC %run ./Models/Sarimax

# COMMAND ----------

# MAGIC %run ./Models/LazyProphet

# COMMAND ----------

# MAGIC %run ./Fitter

# COMMAND ----------

# MAGIC %md ##### Step 2: Input Parameters

# COMMAND ----------

# input parameters
parameters = {
              'forecast_type': {'univariate': True, 'multivariate': True},
              'features': ['Price', 'OperationCost'],
              'time_grain': 'W',
              'level': 'sku',
              'models': ['ewm', 'lazyprophet'],
              'tuning': 'gridsearch',
              'input_type': 'univariate',
              'target': {'csoh': 'WeeklySalesOOS', 'soh': 'AnnualSalesOOSAdjusted'},
              'time_input': {'weekly': 'WeekStartDate'},
              'sku_id': {'Part_Number': 'PartNumber', 'DimProductID': 'DimProductID'},
              'holdout_length': {'weekly': 16},
              'forecast_horizon': {'weekly': [5, 9, 13]},
              'forecast_output': True
              }

# COMMAND ----------

tuning_parameters = {'tuning': True,
                     'hyper_opt': 
                             {
                                 'ewm': {'alpha': [.1, .3, .5, .7, .9]},
                                 'sarimax': {
                                  'p':     hp.randint('p', 4),
                                  'd':     hp.randint('d', 2),
                                  'q':     hp.randint('q', 4),
                                  'cp':   hp.randint('cp', 2),
                                  'cd':   hp.randint('cd', 2),
                                  'cq':   hp.randint('cq', 2),
                                  'trend': hp.choice('trend', ['n', 'c', 't', 'ct']),
                                  'transformation': hp.choice('transformation', [None]),
                                  'model_type': hp.choice('model_choice', ['univariate', 'multivariate'])}, 
                                 'lazyprophet': {'regularization': [1, 1.2, 1.4, 1.6, 1.8], 'trend_dampening': [0, .5, 1], 'transformation': [None]}}}

# COMMAND ----------

# input parameters as variables
time_grain = parameters['time_grain']
features = parameters['features']
level = parameters['level']
models = parameters['models']
tuning = parameters['tuning']
input_type = parameters['input_type']
target = parameters['target']['csoh']
target_secondary = parameters['target']['soh']
time_input = parameters['time_input']['weekly']
sku_id = parameters['sku_id']['DimProductID']
holdout_length = parameters['holdout_length']['weekly']
forecast_horizon = parameters['forecast_horizon']['weekly']
forecast_output = parameters['forecast_output']
tuning = tuning_parameters['tuning']
params = tuning_parameters['hyper_opt']
forecast_type = parameters['forecast_type']

# COMMAND ----------

# MAGIC %md ##### Step 3: Data Ingestion

# COMMAND ----------

# import data
if forecast_type['multivariate']:
    raw_data = spark.sql("""select * from silver_thor.input_elements where DimProductID in 
    (SELECT DISTINCT(DimProductID)
FROM silver_thor.sku_desc
WHERE Qty_2021 > 1.0 AND Tot_Vol_2021 > 1.0)
and WeekStartdate <= '2021-08-08' and WeekStartDate >= '2018-01-01'""").toPandas()
else:
    raw_data = spark.sql("""select * from silver_thor.fcst_oos_sales_qoh where PartNumber in ('1310', '601-BOOST', '701-BOOST', '35160') and WeekStartdate <= '2021-08-08' and WeekStartDate >= '2018-01-01'""").toPandas()

# COMMAND ----------

# filter data for features, time_input, target
data = raw_data[['DimProductID','Price', 'OperationCost', 'WeekStartDate', 'BuyBoxPercentageAmazon', 'WeekNumber', 'FiscalYear', 'WeeklySalesOOS']]

# COMMAND ----------

# clean NAN to 0
data = Preprocess.clean_missing(data, target)

# COMMAND ----------

# group by per sku, date, and target
inputs = Preprocess(time_grain, holdout_length, target, features, time_input).group_for_parallel(data=data, response=target, group=sku_id)

# COMMAND ----------

from pyspark import SparkContext, SparkConf
par_data = sc.parallelize(inputs,100).map(lambda input: Train_Test(time_grain, holdout_length, target, features).train_test_label(input, time_input)).collect()

# COMMAND ----------

# Find a way to do this before the main loop
model_dictionary = {}
time_series = Preprocess.start_end_date(data_length=data_length, start=start,forecast_horizon=forecast_horizon[2], date_type=time_grain)
week_number = Preprocess.start_end_week_number(time_series)
year_number = Preprocess.start_end_year_number(time_series)
model_dictionary['time_series'] = np.array(time_series)
model_dictionary['week_number'] = np.array(week_number)
model_dictionary['year_number'] = np.array(year_number)
input['avg_30_day_price'] = Preprocess.rolling_thirty_average(input, variable='Price', time_variable=time_input)
avg_30_day_price = [input['avg_30_day_price'].min()] * len(time_series)
model_dictionary['avg_price'] = np.array(avg_30_day_price)

# COMMAND ----------

# I think train test split is creating some pockets of time series NaN. for ID 9 we have 
# input length as 132 but the full time series is 145

# COMMAND ----------

# need to check for short time series length
# 1: if ts is less than holdout just assign ewm on all data 16 weeks which is also NP (120 days)
# 2: 16 - 30 change to EWM or heuristic model (half_year)
# 3: Everything else it's fine to have a 16 week holdout

# COMMAND ----------

# label train and test before the fit loop

# COMMAND ----------

par_data = par_data[0:20]

# COMMAND ----------

import warnings
warnings.filterwarnings("ignore")

### Main Program
forecast_list = []

for input in par_data:
    #####  MAIN CODE
    id = input[sku_id].min()
    print(id)
    start = input[time_input].min()

    # used to calculate the time series from start - end
    #data_length = len(input)
    data_length = input['data_length'].min()
    
     # pre-process
    model_dictionary = {}
    time_series = Preprocess.start_end_date(data_length=data_length, start=start,forecast_horizon=forecast_horizon[2], date_type=time_grain)
    model_dictionary['time_series'] = np.array(time_series)


    # Train test split (last 52 weeks is test)
    train_test_obj = Train_Test(time_grain, holdout_length, target, features)
    train, test = train_test_obj.train_test_split(input)

    # Test set with target
    
    y_train = train[target]
    y_train = pd.Series(np.array(train[target]), index=np.array(pd.to_datetime(train[time_input])))
    y_train = y_train.asfreq(time_grain)
    y_test = test[target]

    train_length = len(train)
    test_length = len(test)

    time_series = Preprocess.start_end_date(data_length=data_length, start=start,forecast_horizon=forecast_horizon[2], date_type=time_grain)
    model_dictionary['time_series'] = np.array(time_series)

    if forecast_type['multivariate']:
        x_train = train['Price'].to_numpy().reshape(-1,1)
    else:
        x_train = None
    
    if input['ts_category'].min() == 'npr':
        print('npr')
        model_dictionary = Fitter(train_length=train_length,holdout_length = holdout_length, forecast_horizon=forecast_horizon, forecast_output=forecast_output).model_fit_fcst(y_train=y_train, y_test=y_test,train=train,target=target, model='ewm',time_input=time_input, features=features,x_train=x_train, model_dictionary=None)
        
        # Calculate Accuracy
        model_results = postprocess_obj.calculate_accuracy(model_results)
        model_results['id'] = id
        model_results['model'] = model

        # appended dataframe model_results
        forecast_list.append(model_results)
        
    else:
        
        # ts category is more than 16 weeks
        
        for model in models:
            # Fitter output as model dictionary
            model_dictionary = Fitter(train_length=train_length,holdout_length = holdout_length, forecast_horizon=forecast_horizon, forecast_output=forecast_output).model_fit_fcst(y_train=y_train, y_test=y_test,train=train,target=target, model=model,time_input=time_input, features=features,x_train=x_train, model_dictionary=None)
            # Post Process
            postprocess_obj = Postprocess(holdout_length=holdout_length,forecast_horizon=forecast_horizon)
            # Compile Results to fix data structure
            model_results = postprocess_obj.compile_results(fit_results=model_dictionary,train_length=train_length,test_length=test_length,columns=['date_length', 'holdout_forecast', 'thirty_forecast', 'sixty_forecast', 'ninety_forecast','y_test'])
            # Calculate Accuracy
            model_results = postprocess_obj.calculate_accuracy(model_results)
            model_results['id'] = id
            model_results['model'] = model
            model_results['ts'] = time_series
            model_results['data_length'] = data_length
            model_results['training_length'] = train_length
            model_results['holdout_length'] = holdout_length

            # appended dataframe model_results
            forecast_list.append(model_results)
        

# COMMAND ----------

# DBTITLE 1,All Forecast Model
forecast = pd.concat(forecast_list)

# COMMAND ----------

spark_forecast = spark.createDataFrame(forecast)
spark_forecast.createOrReplaceTempView('forecast')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE or replace TABLE gold_thor.model_forecast_results
# MAGIC as
# MAGIC SELECT * from forecast

# COMMAND ----------

# DBTITLE 1,Best Model Query
spark_best_model = spark.sql("""
select b.id, a.ts, a.model, a.data_length, a.training_length, a.holdout_length, a.fitted_values, a.y_train, a.error, a.thirty_forecast, a.sixty_forecast, a.ninety_forecast,
a.y_test, a.holdout_forecast, a.holdout_mae, a.holdout_mse, a.holdout_mape, a.holdout_smape, b.holdout_wfa from gold_thor.model_forecast_results a
inner join
(select id, MAX(holdout_wfa) as holdout_wfa from gold_thor.model_forecast_results group by id) b
on a.id = b.id
and a.holdout_wfa = b.holdout_wfa
order by b.id
""")

spark_best_model.createOrReplaceTempView('best_model_forecast')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE table gold_thor.best_model_forecast_results
# MAGIC as SELECT * FROM best_model_forecast

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from gold_thor.best_model_forecast_results

# COMMAND ----------

# multivariate
plt.plot(y_test, label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# price sarimax
plt.plot(model_dictionary['y_test'], label='actuals', color='blue')
plt.plot(model_dictionary['test'], label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# price prophet
plt.plot(model_dictionary['y_test'], label='actuals', color='blue')
plt.plot(model_dictionary['test'], label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# multivariate lazyprophet
plt.plot(np.array(y_test), label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# univariate lazyprophet
plt.plot(np.array(y_test), label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# price lazyprophet
plt.plot(np.array(y_test), label='actuals', color='blue')
plt.plot(test_thor_forecast, label='predictions', color='orange')
plt.legend()
plt.show()

# COMMAND ----------

# lazy prophet test

import quandl
import pandas as pd
import matplotlib.pyplot as plt

#Get bitcoin data
data = quandl.get("BITSTAMP/USD")
#let's get our X matrix with the new variables to use
X = data.drop('Low', axis = 1)
X = X.iloc[-730:,:]
y = data['Low']
y = y[-730:]

thor_model_obj = LazyProphet(freq = 365, 
                            estimator = 'linear', 
                            max_boosting_rounds = 200,
                            approximate_splits = True,
                            regularization = 1.2,
                             exogenous=X)
thor_fitted_values = thor_model_obj.fit(time_series=yn)
test_thor_forecast = thor_model_obj.extrapolate(holdout_length)
thor_model_obj.summary()
