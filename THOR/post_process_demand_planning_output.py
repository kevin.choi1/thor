# Databricks notebook source
# There are many null holdout  mapes, and we are removing them

# COMMAND ----------

forecast_output = 'gold_thor.dp_forecast_output'

# COMMAND ----------

# network_node script still uses bronze_spreedw table not 2022022013

# COMMAND ----------

# MAGIC %run ./src/network_node

# COMMAND ----------

# only individuals and replenished + adjustment to fifty_two and thirteen forecast
spark.sql("""select node.WarehouseState, id as DimProductID, dp.ItemID, thor.ts as WeekStartDate, thor.created_on as ForecastCreatedOn, thor.adj_fifty_two_forecast*node.WarehouseDist as Forecast, thor.holdout_mape as HoldoutMape, thor.model as Model 
from 
(select fcst.created_on, fcst.label, fcst.holdout_mape, fcst.model,fcst.id, fcst.ts, margins.`PositiveMarginSales%`, (margins.`PositiveMarginSales%` * fcst.thirteen_forecast) as adj_thirteen_forecast, (margins.`PositiveMarginSales%` * fcst.fifty_two_forecast) as adj_fifty_two_forecast from gold_thor.fcst_best_model_results as fcst left join silver_thor.ref_positivemargin as margins on fcst.id =margins.DimProductID) as thor 
left join bronze_spreedw.dim_product as dp on thor.id = dp.DimProductID 
left join silver_thor.input_networknode as node on thor.id = node.DimProductID 
where thor.label = 'Forecast' and thor.holdout_mape is not null order by thor.id, thor.ts asc""").createOrReplaceTempView('dp_forecast_output')

# COMMAND ----------

# NO ARCHIVING FOR FIRST RUN
# insert forecast into archive
#spark.sql("""INSERT INTO gold_thor.dp_forecast_output_archive select * from gold_thor.dp_forecast_output""")

# COMMAND ----------

spark.sql("""CREATE OR REPLACE TABLE {} AS SELECT * FROM dp_forecast_output""".format(forecast_output))

# COMMAND ----------

print("network to node table disaggregated. ready to give to demand planning as : ", forecast_output)
