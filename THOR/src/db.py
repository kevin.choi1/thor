# Databricks notebook source
# DBTITLE 1,Move current tables into archive
results = spark.sql('select * from gold_thor.model_forecast_results')
results.createOrReplaceTempView('results')
best_results = spark.sql('select * from gold_thor.best_model_forecast_results')
best_results.createOrReplaceTempView('best_results')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TABLE gold_thor.model_forecast_results_archive
# MAGIC as
# MAGIC SELECT * from results

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TABLE gold_thor.best_model_forecast_results_archive
# MAGIC as
# MAGIC SELECT * from best_results
