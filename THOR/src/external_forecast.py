# Databricks notebook source
# MAGIC %run ./post_process

# COMMAND ----------

class ExternalForecast:
    def __init__(self, model, ts_category, id_name, ts_name, forecast_horizon):
        self.model = model
        self.ts_category = ts_category
        self.id = id_name
        self.ts = ts_name
        self.forecast_horizon = forecast_horizon
        return
    
    def general_group_for_parallel(self, data, group):
        """
        :param data: pandas dataframe
        :param response: dependent variable string
        :param group: group by statement
        :param time_input: time_series based column
        :return: list of group by dataframes
        """
        groups = list()
        
        for k, v in data.groupby(group):
            v = v.sort_values(by=self.ts)
            v['id'] = v[self.id]
            v['ts'] = pd.to_datetime(v[self.ts])
            v['model'] = self.model
            v['ts_category'] = self.ts_category
            v['error'] = np.nan
            v['data_length'] = len(v)
            v['training_length'] = len(v[v['label'] == 'Train'])
            v['holdout_length'] = len(v[v['label'] == 'Train'])
            v['fitted_values'] = np.where(v['label'] == 'Train', v['Forecast'], np.nan)
            v['y_train'] = v['WeeklySalesOOS']
            v['five_forecast'] = v[v['label'] == 'Forecast']['Forecast'][0:5]
            v['nine_forecast'] = v[v['label'] == 'Forecast']['Forecast'][0:9]
            v['thirteen_forecast'] = v[v['label'] == 'Forecast']['Forecast'][0:13]
            v['fifty_two_forecast'] = np.where(v['label'] == 'Forecast', v['Forecast'], np.nan)
            
            # calculate error metrics in holdout period
            v = Postprocess(holdout_length = v['training_length'].min(), forecast_horizon = self.forecast_horizon).calculate_accuracy(model_results = v, holdout_forecast_name = 'fitted_values', y_test_name = 'y_train')
    
            groups.append(v)

        return(groups) 
