# Databricks notebook source
# Databricks notebook source
import logging
logger = spark._jvm.org.apache.log4j
logging.getLogger("py4j").setLevel(logging.ERROR)

# COMMAND ----------

from statsmodels.tsa.holtwinters import ExponentialSmoothing
from statsmodels.tsa.holtwinters import Holt
import statsmodels.api
#from fbprophet import Prophet

# COMMAND ----------

# MAGIC %run ./models/Ewm

# COMMAND ----------

# MAGIC %run ./models/Sarimax

# COMMAND ----------

# MAGIC %run ./models/LazyProphet

# COMMAND ----------

# MAGIC %run ./models/FbProphet

# COMMAND ----------

# MAGIC %run ./models/ThorBoostedSLR

# COMMAND ----------

# MAGIC %run ./models/ThorAutoSmoother

# COMMAND ----------

# needs better trace back with self.y giving AssertionError
# maybe we shouldn't have this stuff in the init
class Fitter:
    
    def __init__(self, train_length, test_length, holdout_length, forecast_horizon, forecast_output):
        self.train_length = train_length
        self.test_length = test_length
        self.holdout_length = holdout_length
        self.forecast_horizon = forecast_horizon
        self.forecast_output = forecast_output
       #self.error_logger = error_logger
        
    def model_fit_fcst(self, y_train, y_test, train, target, model, time_input, ts_category, features, x_train=None, params: dict=None, model_dictionary: dict=None):
        
        ####################### EWM #######################
        if model == 'ewm':
            try:
                print('ewm')
                thor_model_obj = Ewm()
                thor_fitted_values = thor_model_obj.fit(y_train)

                if ts_category == 'npr':
                    
                    # holdout for npr is the in sample MAPE
                    holdout_thor_forecast = thor_fitted_values
                    five_thor_forecast = thor_model_obj.predict(self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(self.forecast_horizon[3])
                    err = None    
                    
                elif ts_category == 'less_half_year' or ts_category == 'half_year_plus':
                    
                    holdout_thor_forecast = thor_model_obj.predict(len(y_test))
                    five_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[3])
                    err = None
                    
                else:
                    
                    holdout_thor_forecast = thor_model_obj.predict(len(y_test))
                    five_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(len(y_test) + self.forecast_horizon[3])
                    err = None
                    
            except Exception as e:
                
                model = None
                #thor_fitted_values = None
                holdout_thor_forecast = None
                five_thor_forecast = None
                nine_thor_forecast = None
                thirteen_thor_forecast = None
                fifty_two_thor_forecast = None
                thor_forecast = None
                err = str(e)
                print(e)
                
        ####################### Sarimax #######################         
        elif model == 'sarimax':
            try:
                
                thor_model_obj = Sarimax()
                
                if x_train is not None:
                    
                    holdout_exog_forecast = thor_model_obj.multivariate_forecast(model_dictionary=model_dictionary, holdout_length=self.holdout_length, train_length=self.train_length, forecast_horizon=0, columns = ['avg_price'])
                    five_exog_forecast = thor_model_obj.multivariate_forecast(model_dictionary=model_dictionary, holdout_length=self.holdout_length, train_length=self.train_length, forecast_horizon=self.forecast_horizon[0], columns = ['avg_price'])
                    nine_exog_forecast = thor_model_obj.multivariate_forecast(model_dictionary=model_dictionary, holdout_length=self.holdout_length, train_length=self.train_length, forecast_horizon=self.forecast_horizon[1], columns = ['avg_price'])
                    thirteen_exog_forecast = thor_model_obj.multivariate_forecast(model_dictionary=model_dictionary, holdout_length=self.holdout_length, train_length=self.train_length, forecast_horizon=self.forecast_horizon[2], columns = ['avg_price'])
                    fifty_two_exog_forecast = thor_model_obj.multivariate_forecast(model_dictionary=model_dictionary, holdout_length=self.holdout_length, train_length=self.train_length, forecast_horizon=self.forecast_horizon[3], columns = ['avg_price'])
                    
                else:
                    
                    holdout_exog_forecast = None
                    five_exog_forecast = None
                    nine_exog_forecast = None
                    thirteen_exog_forecast = None
                    fifty_two_exog_forecast = None
                    err = None
                    
                #thor_fitted_values = thor_model_obj.fit(y=y_train,x=x_train)

                # 30, 60, 90 forecast
                if self.forecast_output == True:
                    
                    holdout_thor_forecast = thor_model_obj.predict(self.holdout_length, exog=holdout_exog_forecast)
                    five_thor_forecast = thor_model_obj.predict(self.holdout_length + self.forecast_horizon[0], exog=five_exog_forecast)
                    nine_thor_forecast = thor_model_obj.predict(self.holdout_length + self.forecast_horizon[1], exog=nine_exog_forecast)
                    thirteen_exog_forecast = thirteen_exog_forecast.copy()
                    thirteen_thor_forecast = thor_model_obj.predict(self.holdout_length + self.forecast_horizon[2], exog=thirteen_exog_forecast)
                    fifty_two_exog_forecast = fifty_two_exog_forecast.copy()
                    fifty_two_thor_forecast = thor_model_obj.predict(self.holdout_length + self.forecast_horizon[3], exog=fifty_two_exog_forecast)
                    
                else:
                    print('No forecast horizon output specified.')
                
            except Exception as e:
                
                model = None
                thor_fitted_values = None
                holdout_thor_forecast = None
                five_thor_forecast = None
                nine_thor_forecast = None
                thirteen_thor_forecast = None
                fifty_two_thor_forecast = None
                thor_forecast = None
                err = str(e)
                print(e)
                
        ####################### LazyProphet #######################       
        elif model == 'lazyprophet':
            try:
                
                print('lp')
                thor_model_obj = LazyProphet(freq = 52, 
                                               estimator = 'linear',
                                               approximate_splits = True,
                                               regularization = 1.5,
                                               global_cost = 'maicc',
                                               split_cost = 'mse',
                                               seasonal_regularization = 'auto',
                                               trend_dampening = 2.0,
                                               max_boosting_rounds = 50,
                                               exogenous = x_train
                                                    )
                #thor_fitted_values = thor_model_obj.fit(y_train)['yhat']
                
                # 30, 60, 90 forecast
                if ts_category == 'less_half_year' or ts_category == 'half_year_plus':

                    holdout_thor_forecast = thor_model_obj.extrapolate(len(y_test))
                    five_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[3])
                    err = None
                    
                elif ts_category == 'year_plus':
                    
                    holdout_thor_forecast = thor_model_obj.extrapolate(len(y_test))
                    five_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.extrapolate(len(y_test) + self.forecast_horizon[3])
                    err = None
                
            except Exception as e:
                
                model = None
                #thor_fitted_values = None
                holdout_thor_forecast = None
                five_thor_forecast = None
                nine_thor_forecast = None
                thirteen_thor_forecast = None
                fifty_two_thor_forecast = None
                thor_forecast = None
                err = str(e)
                print(e)

        ####################### Prophet #######################
        elif model == 'fbprophet':
            try:
                
                print('fb')
                thor_model_obj = FbProphet()
                thor_fitted_values = thor_model_obj.fit(y_train)
                
                # 30, 60, 90 forecast
                if ts_category == 'half_year_plus':
                    
                    holdout_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test))
                    five_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test) + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test) + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test) + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test) + self.forecast_horizon[3])
                    err = None
                    
                elif ts_category == 'year_plus':
                    
                    holdout_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test))
                    five_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test)+ self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test) + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test) + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(forecast_horizon = len(y_test) + self.forecast_horizon[3])
                    err = None
                    
            except Exception as e:
                
                model = None
                #thor_fitted_values = None
                holdout_thor_forecast = None
                five_thor_forecast = None
                nine_thor_forecast = None
                thirteen_thor_forecast = None
                fifty_two_thor_forecast = None
                thor_forecast = None
                err = str(e)
                print(e)
                
        ####################### Boosted SLR #######################
        elif model == 'boostedslr':
            try: 
                
                print('bslr')
                thor_model_obj = ThorBoostedSLR(seasonal_period = 12,
                                                smooth_factor = 1,
                                                model_type = 'mult',
                                                boost = True,
                                                boost_iter = 10,
                                                learning_rate = 0.5,
                                                transformation = None,
                                                freq='W',
                                                trend_cap=0.50)
                #thor_fitted_values = thor_model_obj.fit(y_train)
    
                # 30, 60, 90 forecast
                if ts_category == 'less_half_year' or ts_category == 'half_year_plus':

                    holdout_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length)
                    five_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[3])
                    err = None
                    
                elif ts_category == 'year_plus':
                
                    holdout_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length)
                    five_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[3])
                    err = None

            except Exception as e:

                    model = None
                    thor_fitted_values = None
                    holdout_thor_forecast = None
                    five_thor_forecast = None
                    nine_thor_forecast = None
                    thirteen_thor_forecast = None
                    fifty_two_thor_forecast = None
                    thor_forecast = None
                    err = str(e)
                    print(e)

        ####################### AutoSmoother #######################
        elif model == 'autosmoother':
            try: 
                
                print('autosmoother')
                thor_model_obj = ThorAutoSmoother(series = y_train,
                                                  smoother = 'double',
                                                  seasonal_period = None,
                                                  transformation = None,
                                                  freq='W',
                                                  trend_cap=0.50)
                thor_fitted_values = thor_model_obj.fit(y_train)
    
                # 30, 60, 90 forecast
                if ts_category == 'half_year_plus':

                    holdout_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length)
                    five_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[3])
                    err = None
                    
                elif ts_category == 'year_plus':
                
                    holdout_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length)
                    five_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[0])
                    nine_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[1])
                    thirteen_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[2])
                    fifty_two_thor_forecast = thor_model_obj.predict(forecast_horizon = self.test_length + self.forecast_horizon[3])
                    err = None

            except Exception as e:

                    model = None
                    #thor_fitted_values = None
                    holdout_thor_forecast = None
                    five_thor_forecast = None
                    nine_thor_forecast = None
                    thirteen_thor_forecast = None
                    fifty_two_thor_forecast = None
                    thor_forecast = None
                    err = str(e)
                    print(e)

        else:

                    model = None
                    #thor_fitted_values = None
                    holdout_thor_forecast = None
                    five_thor_forecast = None
                    nine_thor_forecast = None
                    thirteen_thor_forecast = None
                    fifty_two_thor_forecast = None
                    thor_forecast = None
                    err = 'No model fit.'
                
        
        #return {'fitted_values': np.array(thor_fitted_values), 'five_forecast': np.array(five_thor_forecast), 'nine_forecast': np.array(nine_thor_forecast), 'thirteen_forecast': np.array(thirteen_thor_forecast), 'fifty_two_forecast': np.array(fifty_two_thor_forecast),'y_train': np.array(y_train), 'y_test': np.array(y_test), 'holdout_forecast': np.array(holdout_thor_forecast), 'error': err}
        return {'five_forecast': np.array(five_thor_forecast), 'nine_forecast': np.array(nine_thor_forecast), 'thirteen_forecast': np.array(thirteen_thor_forecast), 'fifty_two_forecast': np.array(fifty_two_thor_forecast),'y_test': np.array(y_test), 'holdout_forecast': np.array(holdout_thor_forecast), 'error': err}
    
