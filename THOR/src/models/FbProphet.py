# Databricks notebook source
# MAGIC %run ./Transformations

# COMMAND ----------

# MAGIC %run ./ModelClass

# COMMAND ----------

import numpy as np
import pandas as pd
from fbprophet import Prophet

class FbProphet(ModelClass):
    model = 'fbprophet'
    version = 0.2
    default_param_space = {'growth': 'linear',
                            'seasonality_mode': 'multiplicative',
                            'yearly_seasonality': True,
                            'daily_seasonality': False,
                            'weekly_seasonality': False,
                            'seasonality_prior_scale': 1,
                            'changepoint_prior_scale': 0.02,
                            'changepoint_range': .98,
                            'transformation': None}
    
    def __init__(self, 
                 growth='linear',
                 seasonality_mode='multiplicative',
                 yearly_seasonality=True,
                 daily_seasonality=False,
                 weekly_seasonality=False,
                 seasonality_prior_scale=1,
                 changepoint_prior_scale=0.02,
                 changepoint_range=0.98,
                 transformation=None,
                 freq='W',
                 trend_cap=0.50):
        #self.params = params
        self.growth=growth
        self.seasonality_mode=seasonality_mode
        self.yearly_seasonality=yearly_seasonality
        self.daily_seasonality=daily_seasonality
        self.weekly_seasonality=weekly_seasonality
        self.seasonality_prior_scale=seasonality_prior_scale
        self.changepoint_prior_scale=changepoint_prior_scale
        self.changepoint_range=changepoint_range
        self.transformation=transformation
        self.freq = freq
        self.trend_cap = trend_cap
        return
    
    @staticmethod
    def build_input(y):
        y = y.to_frame().reset_index()
        y.columns = ['ds', 'y']
        y['ds'] = y['ds'].dt.tz_localize(None)
        return y
    
    def fit(self, y):
        #if self.params is None:
        #    self.params = FbProphet.default_param_space
        self.transformer = Transformations()
        y = self.transformer.transform(y, transformation=self.transformation)
        y = FbProphet.build_input(y)
        #growth = self.params['growth']
        #seasonality_mode = self.params['seasonality_mode']
        #yearly_seasonality = self.params['yearly_seasonality']
        #weekly_seasonality = self.params['weekly_seasonality']
        #seasonality_prior_scale = self.params['seasonality_prior_scale']
        #changepoint_prior_scale = self.params['changepoint_prior_scale']
        #changepoint_range = self.params['changepoint_range']
        m = Prophet( 
            growth=self.growth,
            seasonality_mode = self.seasonality_mode,
            yearly_seasonality=self.yearly_seasonality,
            weekly_seasonality=self.weekly_seasonality,
            seasonality_prior_scale=self.seasonality_prior_scale,   
            changepoint_prior_scale=self.changepoint_prior_scale,
            changepoint_range=self.changepoint_range
        )
        self.model_obj = m.fit(y)
        prophet_output = self.model_obj.predict(y)
        fitted_values = prophet_output['yhat']
        return self.transformer.inverse_transform(fitted_values)
        
    def predict(self, forecast_horizon):
        future_df = self.model_obj.make_future_dataframe(periods=forecast_horizon,
                                                         freq=self.freq,
                                                         include_history=False) 
        prophet_predicted_output = self.model_obj.predict(future_df)
        predictions = prophet_predicted_output['yhat'].clip(lower=0) 
        self.predicted_trend = prophet_predicted_output['trend'].values
        if self.trend_cap is not None:
            predictions = self.trend_dampen(predictions, self.predicted_trend, self.trend_cap)
        return self.transformer.inverse_transform(predictions)
