# Databricks notebook source
# MAGIC %run ./Transformations

# COMMAND ----------

# MAGIC %run ./ModelClass

# COMMAND ----------

import numpy as np
import pandas as pd

class ThorFft:
    model = 'fft'
    version = 0.2
    default_param_space = {'n_harmonics': 5,
                           'transformation': None}
    
    def __init__(self, 
                 params: dict=None,
                 freq='W',
                 trend_cap=None):
        self.params = params
        self.freq = freq
        self.trend_cap = trend_cap
        
    def reconstruct_signal( self,
                            n_periods,
                            forecast_length,
                            fft_model,
                            ft_sample_frequencies,
                            fft_terms_for_reconstruction,
                            linear_trend
                          ):
        
        pi = np.pi
        t = np.arange(0, n_periods+forecast_length)
        restored_sig = np.zeros(t.size)
        for i in fft_terms_for_reconstruction:
            ampli = np.absolute(fft_model[i]) / n_periods   # amplitude
            phase = np.angle(fft_model[i],
                             deg=False)                       # phase in radians
            restored_sig += ampli * np.cos(2 * pi * ft_sample_frequencies[i] * t + phase)
        if forecast_length:
            self.predicted_trend = (linear_trend[0] * t)[-forecast_length:]
        return restored_sig + linear_trend[0] * t
        
    def fit(self, y):
        if self.params is None:
            self.params = ThorFft.default_param_space
        self.transformer = Transformations()
        y = self.transformer.transform(y, transformation=self.params['transformation'])
        self.training_length = len(y)
        t = np.arange(0, self.training_length)
        self.linear_trend = np.polyfit(t, y, 1)
        training_endog_detrend = y - self.linear_trend[0] * t
        self.model_obj = np.fft.fft(training_endog_detrend)
        indexes = list(range(self.training_length))                            
        # sort by amplitude
        indexes.sort(key=lambda i: np.absolute(self.model_obj[i]) / self.training_length,
                     reverse=True)
        self.fft_terms_for_reconstruction = indexes[:1 + self.params['n_harmonics'] * 2]
        self.ft_sample_frequencies = np.fft.fftfreq(n=self.training_length,
                                                    d=1)
        fft_fit_forecast = self.reconstruct_signal(
                                                 n_periods=self.training_length,
                                                 forecast_length=0,
                                                 fft_model=self.model_obj,
                                                 ft_sample_frequencies=self.ft_sample_frequencies,
                                                 fft_terms_for_reconstruction=self.fft_terms_for_reconstruction,
                                                 linear_trend=self.linear_trend
                                              )
        fitted_values = pd.Series(fft_fit_forecast).clip(lower=0)
        return self.transformer.inverse_transform(fitted_values)
    
    def predict(self, forecast_horizon):
        fft_fit_forecast = self.reconstruct_signal(n_periods=self.training_length,
                                                   forecast_length=forecast_horizon,
                                                   fft_model=self.model_obj,
                                                   ft_sample_frequencies=self.ft_sample_frequencies,
                                                   fft_terms_for_reconstruction=self.fft_terms_for_reconstruction,
                                                   linear_trend=self.linear_trend
                                                   )
        predictions = pd.Series(fft_fit_forecast[-forecast_horizon:]).clip(lower=0)
        predicted_trend = self.predicted_trend
        if self.trend_cap is not None:
            predictions = self.trend_dampen(predictions, predicted_trend, self.trend_cap)
        return self.transformer.inverse_transform(predictions)
