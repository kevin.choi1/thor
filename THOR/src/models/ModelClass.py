# Databricks notebook source
# MAGIC %run ./trend_dampen

# COMMAND ----------

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 14:12:04 2021

@author: kevin.choi
"""

import abc
import numpy as np
import pandas as pd

class ModelClass(abc.ABC):
    """The Model Class which is the base class for all model types. 
       Fit and predict methods are required for child classes."""
    
    #def __str__(self):
    #   return f'{self.model} with params: {self.params}'
    
    #def __repr__(self):
    #    return f'{type(self).__name__}({self.params, self.freq, self.trend_cap})'
        
    @abc.abstractmethod
    def fit(self, y: pd.Series) -> pd.Series:
        """Fit method which returns fitted values"""
        pass

    @abc.abstractmethod
    def predict(self, forecast_horizon: int) -> pd.Series: 
        """Predict method which returns predicted values"""
        pass

    def get_frequency(self, index):
        if len(index) > 2:
            freq = pd.infer_freq(index)
        elif len(index) == 2:
            time_delta = (index[-1] - index[0]).days
            if time_delta > 2 and time_delta < 20:
                freq = 'W'
            elif time_delta < 2:
                freq = 'D'
            elif time_delta > 20 and time_delta < 40:
                freq = 'M'
            elif time_delta > 300:
                freq = 'Y'
        else:
            #safe setting to month
            freq = 'M'
        return freq

    def trend_dampen(self, predictions, trend, trend_cap) -> np.array:
        """
        Applies exponential decay magic to trends that are above/below cap YoY

        Parameters
        ----------
        predictions : TYPE
            DESCRIPTION.
        trend : TYPE
            DESCRIPTION.
        trend_cap : TYPE
            DESCRIPTION.

        Returns
        -------
        dampened_predictions : TYPE
            DESCRIPTION.

        """
        if trend[0] == 0 and trend[-1] != 0:
            damp_factor = trend_cap
        elif trend[0] == 0 and trend[-1] == 0:
            dampened_trend = trend
            damp_factor = 0
        else:
            predicted_trend_perc = (trend[-1] - trend[0]) / (trend[0]) 
            trend_change = np.abs(trend_cap / predicted_trend_perc)
            damp_factor = max(0, trend_change)
        dampened_trend = TrendDampen(trend, damp_factor, damp_style='smooth').main()    
        dampened_predictions = predictions - trend + dampened_trend
        return dampened_predictions
        
