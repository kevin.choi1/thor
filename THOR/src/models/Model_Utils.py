# Databricks notebook source
import numpy as np
import warnings
from scipy.optimize import minimize
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.utils import check_array

class Model_Utils:
    def __init__(self, epsilon = 0.001):
        self.epsilon = epsilon

    def compute_abs_err(self, y, yhat):
        #TODO: add flags here to check for abs_err input values

        #check_array([y, yhat])

        return abs(y - yhat)


    def compute_mae(self, y, yhat):
        #TODO: check for null values in y or yhat. should warn or raise error?
        #TODO: check the len of y and yhat. fails if len(y) < > len(yhat)

        #check_array([y, yhat])

        return mean_absolute_error(y, yhat)


    def compute_mse(self, y, yhat):
        #TODO: check for null values in y or yhat. should warn or raise error?
        #TODO: check the len of y and yhat. fails if len(y) < > len(yhat)

        #check_array([y, yhat])

        return mean_squared_error(y, yhat)


    def compute_mape(self, y, yhat):

        #check_array([y, yhat])

        return np.mean(np.abs((y - yhat) / y))


    def compute_smape(self, y, yhat):

        #check_array([y, yhat])

        return (np.sum(2 * np.abs(y - yhat) / (np.abs(y) + np.abs(yhat) + self.epsilon))) / (len(y))


    def compute_wfa(self, y, yhat):

        #TODO: add flags here to check for wfa input values

        abs_err = Model_Utils().compute_abs_err(y, yhat)

        if sum(y) + sum(yhat) + sum(abs_err) == 0:
            wfa = 0
        elif min(yhat) < 0:
            wfa = 0
        else:
            wfa = max(0, 1 - 2*sum(abs_err)/(self.epsilon + sum(y) + sum(yhat)))  

        return wfa
      
    def rolling_compute_mape(self, y, yhat):
        
        y.expanding().mean()
        yhat.expanding().mean()
        
        return
