# Databricks notebook source
# MAGIC %run ./Transformations

# COMMAND ----------

# MAGIC %run ./ModelClass

# COMMAND ----------

import numpy as np
import pandas as pd
import statsmodels.api as sm

class Sarimax():
    model = 'sarimax'
    version = 0.1
    default_param_space = {
                            'p': 1,
                            'd': 0,
                            'q': 1,
                            'cp': 0,
                            'cd': 0,
                            'cq': 0,
                            's': 52,
                            'trend': 'n',
                            'transformation': None
                            }
    
    def __init__(self,
                 params: dict = None,
                 freq='W',
                 trend_cap=None):
        self.params = params
        self.freq = freq
        self.trend_cap = trend_cap
        return
    
    def fit(self, y, x=None):
        if self.params is None:
            self.params = Sarimax.default_param_space
            
        # Inputs
        self.y = y
        self.x = x
        
        # Transformations
        self.y.index.freq = self.freq
        self.transformer = Transformations()
        self.y = self.transformer.transform(self.y, transformation=self.params['transformation'])
        
        # Parameters
        self.order = self.params['p'], self.params['d'], self.params['q']
        self.seasonal_order = self.params['cp'], self.params['cd'], self.params['cq'], self.params['s']
        self.trend = self.params['trend']
        # Check for univariate or multivariate models

        self.model_obj = sm.tsa.statespace.SARIMAX(endog=self.y,
                                                   exog = self.x,
                                                   enforce_invertibility=True,
                                                   enforce_stationarity=True,
                                                   order=self.order,
                                                   seasonal_order=self.seasonal_order,
                                                   trend=self.trend)
        self.model_obj = self.model_obj.fit()
        self.fitted_values = self.model_obj.fittedvalues
        
        return self.transformer.inverse_transform(self.fitted_values)
    
    def predict(self, forecast_horizon, exog=None):
        predictions = self.model_obj.predict(start=len(self.y),
                                             end=len(self.y) + forecast_horizon - 1, exog=exog).clip(lower=0)
        t = np.arange(0, forecast_horizon)
        lr = np.polyfit(t, predictions, 1)
        predicted_trend =  lr[0]*t+lr[1]
        #if self.trend_cap is not None:
            #predictions = self.trend_dampen(predictions, predicted_trend, self.trend_cap)
        return self.transformer.inverse_transform(predictions)
    
    def multivariate_forecast(self, model_dictionary=None, holdout_length=None, train_length=None, forecast_horizon=None, columns=None):
        """
        :params holdout_length:
        :params train_length:
        :params forecast_horizon:
        """
        if (holdout_length and train_length and forecast_horizon) is not None: 
            holdout_length = holdout_length - 1
            train_length = train_length - 1
            #forecast_horizon = forecast_horizon - 1
        
        forecast_pd = pd.DataFrame(model_dictionary, columns = columns)
        exog_forecast = forecast_pd.loc[train_length:train_length+holdout_length+forecast_horizon, columns]
        
        return(exog_forecast)
