# Databricks notebook source
# MAGIC %run ./Transformations

# COMMAND ----------

# MAGIC %run ./model_src/AutoSmoother

# COMMAND ----------

# MAGIC %run ./ModelClass

# COMMAND ----------

import pandas as pd
import numpy as np


class ThorAutoSmoother(ModelClass):
    model = 'auto_smooth'
    version = 0.2
    default_param_space = {'smoother': 'simple',
                           'seasonal_period': 0,
                           'transformation': None}
    
    def __init__(self,
                 series = None,
                 smoother = 'double',
                 seasonal_period = None,
                 transformation = None,
                 freq='W',
                 trend_cap=0.50):
        self.series = series
        self.smoother = smoother
        self.seasonal_period = seasonal_period
        self.transformation = transformation
        self.freq = freq
        self.trend_cap = trend_cap
        return
    
    def fit(self, y):
        #if self.params is None:
        #   self.params = ThorAutoSmoother.default_param_space
        self.transformer = Transformations()
        y = self.transformer.transform(y, transformation=self.transformation)
        self.model_obj = AutoSmoother(series = self.series, smoother=self.smoother,
                                     seasonal_period=self.seasonal_period)
        self.fitted_values = self.model_obj.fit(y)
        return self.transformer.inverse_transform(self.fitted_values)

    
    def predict(self, forecast_horizon):
        predictions = self.model_obj.predict(forecast_horizon=forecast_horizon)
        t = np.arange(0, forecast_horizon)
        lr = np.polyfit(t, predictions, 1)
        predicted_trend =  lr[0]*t+lr[1]
        if self.trend_cap is not None:
            predictions = self.trend_dampen(predictions, predicted_trend, self.trend_cap)
        return self.transformer.inverse_transform(predictions)
    
