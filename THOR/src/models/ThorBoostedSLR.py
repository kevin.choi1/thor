# Databricks notebook source
# MAGIC %run ./Transformations

# COMMAND ----------

# MAGIC %run ./model_src/BoostedSLR

# COMMAND ----------

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 16:17:08 2021

@author: ER90614
"""

import pandas as pd
import numpy as np
    
class ThorBoostedSLR(ModelClass):
    model = 'boosted_slr'
    version = 0.2
    default_param_space = {'seasonal_period': 0,
                           'learning_rate': .2,
                           'model_type': 'mult',
                           'transformation': None
                           }

    def __init__(self, 
                 seasonal_period = 12,
                 smooth_factor = 1,
                 model_type = 'mult',
                 boost = True,
                 boost_iter = 10,
                 learning_rate = 0.5,
                 transformation = None,
                 freq='W',
                 trend_cap=None):
        self.seasonal_period = seasonal_period
        self.smooth_factor = smooth_factor
        self.model_type = model_type
        self.boost = boost
        self.boost_iter = boost_iter
        self.learning_rate = learning_rate
        self.transformation = transformation
        self.freq = freq
        self.trend_cap = trend_cap
        return
                
    def fit(self, y):
        #if self.params is None:
        #    self.params = ThorBoostedSLR.default_param_space
        self.transformer = Transformations()
        y = self.transformer.transform(y, transformation=self.transformation)
        seasonal_period = self.seasonal_period
        smooth_factor = self.smooth_factor
        model_type = self.model_type
        boost = self.boost
        boost_iter = self.boost_iter
        learning_rate = self.learning_rate
        self.model_obj = BoostedSLR(seasonal_period = seasonal_period,
                                    smooth_factor = smooth_factor,
                                    model_type = model_type,
                                    boost = boost,
                                    boost_iter = boost_iter,
                                    learning_rate = learning_rate)
        fitted = self.model_obj.fit(y)   
        return self.transformer.inverse_transform(pd.Series(fitted))
    
    def predict(self, forecast_horizon):
        predictions = self.model_obj.predict(forecast_horizon)
        t = np.arange(0, forecast_horizon)
        lr = np.polyfit(t, predictions, 1)
        predicted_trend =  lr[0]*t+lr[1]
        predicted_trend = self.model_obj.predicted_trend
        if self.trend_cap is not None:
            predictions = self.trend_dampen(predictions, predicted_trend, self.trend_cap)
        return self.transformer.inverse_transform(pd.Series(predictions))
