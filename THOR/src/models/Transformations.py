# Databricks notebook source
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 13:36:46 2021

@author: kevin.choi
"""

import statsmodels.api as sm
import pywt
import numpy as np
import pandas as pd
lowess = sm.nonparametric.lowess

class Transformations:
    """ Base transformations for converting input/outputs"""
    
    def __init__(self):
        pass
        
    def lowess_transform(self, endog):
        
        z = lowess(endog, endog.index, frac=.15, return_sorted=False)
        
        # floor set to 0
        z[z < 0] = 0
        
        # convert array into series
        endog = pd.Series(z, index=endog.index)

        return endog
      
    def log_transform(self, ts):
        
        log_tsv = np.log(np.where(ts < 1, 1, ts))
        log_ts = pd.Series(log_tsv, index=ts.index)
        
        return log_ts

    def inverse_log_transform(self, ts):
        return np.exp(ts) 
    
    def wavelet(self, endog, wavelet = 'db4', wavelet_level = 1):
      
        coeff = pywt.wavedec(endog, wavelet, mode="per")
        
        sigma = (1/0.6745) * self.maddest(coeff[-wavelet_level])

        uthresh = sigma * np.sqrt(2*np.log(len(endog)))
        
        coeff[1:] = (pywt.threshold(i, value=uthresh, mode='hard') for i in coeff[1:])
        
        z = pywt.waverec(coeff, wavelet, mode='per')
        
        z[z<0] = 0
        
        tr_endog = pd.Series(z, index=endog.index)

        return tr_endog
    
    def find_wavelet(self, endog):
        wavelet_name = []
        wavelet_l = []
        all_wavelets = []
        errors = []
        for i in range(6):
            for each in pywt.wavelist():
                try:
                    transformed_time_series = self.wavelet(endog, wavelet = each, wavelet_level = i)        
                    if self.rmse(transformed_time_series, endog) > 0:
                        wavelet_name.append(each)
                        wavelet_l.append(i)
                        all_wavelets.append(transformed_time_series)
                        errors.append(self.rmse(transformed_time_series, endog))
                except:
                    continue  
        opt = errors.index(min(errors))
        return wavelet_name[opt], wavelet_l[opt]
    
    def rmse(self, pred, target):
        rmse = np.linalg.norm(pred - target) / np.sqrt(len(target))
        return rmse 
    
    def IQR_transform(self, endog):
        Q1,Q3 = np.percentile(endog , [25,75])
        
        #med = np.median(endog)
        
        IQR = Q3 - Q1
        
        lower_range, upper_range = Q1 - (2.0 * IQR), Q3 + (2.0 * IQR)        
        
        #avg = np.mean(endog[(endog > lower_range) & (endog < upper_range)])
        
        endog[endog < lower_range] = lower_range
        
        endog[endog > upper_range] = upper_range
        
        tr_endog = pd.Series(endog, index = endog.index)
        
        return tr_endog
    
    def transform(self, time_series, transformation):
        self.transformation = transformation
        if self.transformation == 'iqr':
            transformed_time_series = self.IQR_transform(time_series)
        elif self.transformation == 'log':
            transformed_time_series = self.log_transform(time_series)
        elif self.transformation == 'lowess':
            transformed_time_series = self.lowess_transform(time_series)
        elif self.transformation == 'wavelet':
            wavelet, wavelet_lvl = self.find_wavelet(time_series)
            transformed_time_series = self.wavelet(time_series, 
                                                   wavelet=wavelet, 
                                                   wavelet_level=wavelet_lvl)    
        elif self.transformation is None:
            transformed_time_series = time_series
        return transformed_time_series
    
    def inverse_transform(self, transformed_time_series):
        if self.transformation == 'log':
            inverse_time_series = self.inverse_log_transform(transformed_time_series)
        else:
            inverse_time_series = transformed_time_series
        return inverse_time_series
