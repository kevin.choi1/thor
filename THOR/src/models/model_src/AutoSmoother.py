# Databricks notebook source
from statsmodels.tsa.holtwinters import ExponentialSmoothing, SimpleExpSmoothing, Holt
from statsmodels.tsa.seasonal import seasonal_decompose
import pandas as pd
import numpy as np

#old bad code, need refactoring
class AutoSmoother:
    def __init__(self, 
                 series = None,
                 smoother = 'double', 
                 clean_outliers = True, 
                 iqr_multiplier = 1.25,
                 smooth_history = False, 
                 damped = False,
                 alpha = 0.5,
                 beta = 0.5,
                 gamma = None,
                 optimize = True,
                 seasonal_period = None):
        
        self.series = series   
        self.smooth_history = smooth_history                   
        self.clean_outliers = clean_outliers
        self.seasonal_period = seasonal_period
        self.iqr_multiplier = iqr_multiplier
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.damped = damped    
        self.optimize = optimize
        self.smoother = smoother
        self.set_model(smoother) 
        self.remove_outliers()  
        self.seasonality = None
        return 

    def remove_outliers(self):
        if self.clean_outliers:
            q75, q25 = np.percentile(self.series, [75 ,25])
            iqr = q75 - q25
            lowerbound = q25 - self.iqr_multiplier * iqr
            upperbound = q75 + self.iqr_multiplier * iqr
            mean = np.mean(self.series[(self.series > lowerbound) & (self.series < upperbound)])
            self.series[self.series > upperbound] = mean
            self.series[self.series < lowerbound] = mean           
        return

    def set_model(self, smoother):
        if smoother == 'double':
            self.model = self.sm_holt_smoothing
        elif smoother =='simple':
            self.model = self.sm_simple_smoothing   
        elif smoother =='triple':
            self.model = self.sm_triple_smoothing   
        return
    
    def get_smoothed_history(self):
        self.series = self.simple_exponential_smoothing(series = self.series, alpha = .5)[0]
        return
    
    def deseasonalize(self):
        decomp = seasonal_decompose(self.series, extrapolate_trend=1)
        self.seasonality = decomp.seasonal
        self.series -= self.seasonality
        return

    def sm_holt_smoothing(self):
        model = Holt(self.series, damped = self.damped).fit(
                                                 smoothing_level = self.alpha,
                                                 smoothing_slope = self.beta,
                                                 optimized = self.optimize,
                                                 )      
        return model

    def sm_simple_smoothing(self):
        model = SimpleExpSmoothing(self.series).fit(
                     #smoothing_level = self.alpha,
                     optimized = self.optimize)
        return model

    def sm_triple_smoothing(self):
        model = ExponentialSmoothing(self.series,
                                     trend='add', seasonal='add',
                                     seasonal_periods = self.seasonal_period).fit(
                     smoothing_level = self.alpha,
                     smoothing_slope = self.beta,
                     smoothing_seasonal = self.gamma,
                     optimized = self.optimize,
                     use_boxcox = False,
                     )
        return model
    
    def fit(self, series):
        self.series = series.copy()
        if self.smooth_history:
            self.get_smoothed_history() 
        if self.seasonal_period:
            self.deseasonalize()
        if self.smoother == 'ensemble':
            self.simple_obj = self.sm_simple_smoothing()
            self.double_obj = self.sm_holt_smoothing()
            self.damped = True
            self.double_damped_obj = self.sm_holt_smoothing()
            simple = self.simple_obj.predict(0, len(self.series)-1)
            double = self.double_obj.predict(0, len(self.series)-1)
            double_damped = self.double_damped_obj.predict(0, len(self.series)-1)
            fitted = (simple + double + double_damped) / 3
        else:
            self.model_obj = self.model
            fitted = self.model_obj().predict(0, len(self.series)-1)
        if self.seasonality is not None:
            fitted += self.seasonality
        return fitted
    
    def predict(self, forecast_horizon):
        self.forecast_horizon = forecast_horizon
        init_point = len(self.series)
        final_point = len(self.series) + self.forecast_horizon
        if self.smoother == 'ensemble':
            simple = self.simple_obj.predict(init_point, final_point-1)
            double = self.double_obj.predict(init_point, final_point-1)
            double_damped = self.double_damped_obj.predict(init_point, final_point-1)
            predicted = (simple + double + double_damped) / 3
        else:
            predicted = self.model_obj().predict(init_point, final_point-1)
        if self.seasonality is not None:
            self.seasonality = np.resize(self.seasonality[:self.seasonal_period], (final_point,))
            #self.seasonality = np.resize(self.seasonality[:self.seasonal_period].values, (final_point,))
            predicted += self.seasonality[-self.forecast_horizon:]
        return predicted
