# Databricks notebook source
import statsmodels.api
import pandas
import numpy

class BoostedSLR:
    
    def __init__(self,
                 seasonal_period,
                 smooth_factor = 1,
                 model_type = 'mult',
                 boost = True,
                 boost_iter = 10,
                 learning_rate = .2):
                
                self.seasonal_period = seasonal_period
                self.smooth_factor = smooth_factor
                self.model_type = model_type
                self.boost = boost
                self.boost_iter = boost_iter
                self.learning_rate = learning_rate

                return

    def get_avg_seasonal_idx(
                            self,
                            ts_list,
                            nk,
                            smooth_factor):
        # List of average exog per season
        if self.model_type == 'mult':
            ak_list = [sum(ts_list[i])/len(ts_list[i]) for i in range(0, len(ts_list))]
    
            # List of starting seasonal index for each time period in a season
            s_list = [ts_list[i]/ak_list[i] for i in range(len(ak_list))]
        elif self.model_type == 'add':
            ak_list = [sum(ts_list[i])/len(ts_list[i]) for i in range(0, len(ts_list))]
    
            # List of starting seasonal index for each time period in a season
            s_list = [ts_list[i] - ak_list[i] for i in range(len(ak_list))]
            

        # logic for incomplete seasons
        if nk < 1.0:
            s_avg = numpy.mean(s_list)
        elif nk >= 1.0:
            s_avg = [numpy.mean([s[i] for s in s_list if len(s) > i]) for i in range(len(max(s_list,key=len)))]

        # Smooth factor applied to list of seasonal averages
        s_avg_s = numpy.array(s_avg) * self.smooth_factor

        return(s_avg_s)


    def fit(self, input_endog):
        self.input_endog = input_endog
        # Exog
        ts = self.input_endog.values
        if self.seasonal_period:
            # Number of available time periods
            n_total = len(self.input_endog)
            
            # Number of complete seasons
            nk = n_total/self.seasonal_period
    
            # List of exog per season
            ts_list = [ts[i:i + self.seasonal_period] for i in range(0, len(ts), self.seasonal_period)]
    
            # Calculate seasonal indices for time series
            self.s_avg_s = self.get_avg_seasonal_idx(
                                            ts_list = ts_list,
                                            nk = nk,
                                            smooth_factor = 1.0)
        else:
            if self.model_type == 'add':
                self.s_avg_s = 0
            else:
                self.s_avg_s = 1
            

        if self.model_type == 'add':

            # Regression model (add)
            X = range(0, len(ts))
            X = statsmodels.api.add_constant(X)
            # Changed to take out seasonality from time series before fitting regression
            y = ts - numpy.resize(self.s_avg_s, len(ts))

        if self.model_type == 'mult':
            
            # Regression model (mult)
            X = range(0, len(ts))
            X = statsmodels.api.add_constant(X)
            # Changed to take out seasonality from time series before fitting regression
            y = ts/numpy.resize(self.s_avg_s, len(ts))


        if self.boost:
            # Boosting
            boosted_data = y
            boosted_output = numpy.zeros(len(y))
            coefs = numpy.zeros(2)

            # Boosting Iterations
            for i in range(self.boost_iter):
                model = statsmodels.api.OLS(boosted_data, X)
                res = model.fit()
                fitted = model.predict(res.params, X)
                boosted_output =  boosted_output + self.learning_rate*fitted
                boosted_data = y - boosted_output
                coefs = coefs + self.learning_rate*res.params
        # Baseline seasonal linear regression
        else:
            model = statsmodels.api.OLS(y, X)
            res = model.fit()
            coefs = res.params
                
        self.model = model
        self.coefs = coefs
        # X values to include ts and forecast
        X = range(0, len(self.input_endog.values))

        # Time as the variable in regression
        X = statsmodels.api.add_constant(X)

        # Predicted Y
        yhat = self.model.predict(self.coefs, X)
        # Predict Y adjusted with seasonal index
        if self.model_type == 'mult':
            y_adj = yhat * numpy.resize(self.s_avg_s, len(yhat))
        elif self.model_type == 'add':
            y_adj = yhat + numpy.resize(self.s_avg_s, len(yhat))

        return y_adj

    def predict(self, forecast_horizon):
        X = range(0, len(self.input_endog.values)+forecast_horizon)

        # Time as the variable in regression
        X = statsmodels.api.add_constant(X)

        # Predicted Y
        yhat = self.model.predict(self.coefs, X)
        self.predicted_trend = yhat[-forecast_horizon:]
        # Predict Y adjusted with seasonal index
        if self.model_type == 'mult':
            y_adj = yhat * numpy.resize(self.s_avg_s, len(yhat))
        elif self.model_type == 'add':
            y_adj = yhat + numpy.resize(self.s_avg_s, len(yhat))

        # forecast in sample sample
        frc_out = y_adj[len(self.input_endog.values):]
        
        return frc_out
    
