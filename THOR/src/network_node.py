# Databricks notebook source
# MAGIC %md # Network to Node Disaggregation

# COMMAND ----------

input_networknode = spark.sql(
"""
WITH 

L365Sales AS (
  SELECT 
    VC.DimDateOfOrderID
    ,DATE(CAL.Date) AS Date
    ,DATE((LEFT(CAL.FiscalWeekRange, 10))) AS WeekStartDate
    ,VC.DimShippedProductID
    ,GEO.PostalCode
    ,VC.DimShippingGeographyID
    ,FC.DimWarehouseLocationID
    ,FC.WarehouseState
    ,COALESCE(OFC.OptimalFC, FC.WarehouseState) AS OptimalFC
    ,VC.Quantity
  -- bronze is updated each week with new data
  FROM bronze_spreedw.fact_valuechain VC
  LEFT JOIN bronze_spreedw.dim_date CAL ON CAL.DimDateID = VC.DimDateOfOrderID 
  LEFT JOIN bronze_spreedw.dim_product PROD ON PROD.DimProductID = VC.DimShippedProductID
  LEFT JOIN bronze_spreedw.dim_warehouselocation AS FC ON FC.DimWareHouseLocationID = VC.DimWarehouseLocationID
  LEFT JOIN bronze_spreedw.dim_geography AS GEO ON GEO.DimGeographyID = VC.DimShippingGeographyID
  -- silver table updated monhtly
  LEFT JOIN silver_thor.ref_optimalfc AS OFC ON OFC.DestZip = GEO.PostalCode
  WHERE DATE(CAL.Date) >= DATE(CURRENT_TIMESTAMP()) - 365 AND FC.WarehouseState <> 'Warehouse State Not Known'
),

L365Sales_DimProdID AS (
  SELECT 
    DimShippedProductID AS DimProductID
    ,SUM(Quantity) AS NetworkSales
  FROM L365Sales
  GROUP BY DimProductID
  ORDER BY DimProductID
),

L365Sales_DimProdIDFC AS (
  SELECT 
    DimShippedProductID AS DimProductID
    ,OptimalFC AS WarehouseState
    ,SUM(Quantity) AS NodeSales
  FROM L365Sales
  GROUP BY DimProductID, OptimalFC
  ORDER BY DimProductID, OptimalFC
),

NetworkDist AS (
  SELECT DISTINCT 
    DimProductID
    ,'Network' AS WarehouseState
    ,1 AS WarehouseDist
  FROM L365Sales_DimProdIDFC
),

NodeDist AS (
  SELECT  
     NODE.DimProductID
    ,NODE.WarehouseState
    ,(NODE.NodeSales / NETWORK.NetworkSales) AS WarehouseDist
  FROM L365Sales_DimProdIDFC AS NODE
  LEFT JOIN L365Sales_DimProdID AS NETWORK ON NETWORK.DimProductID = NODE.DimProductID
  ORDER BY NODE.DimProductID, NODE.WarehouseState
)

SELECT * FROM NodeDist
UNION ALL 
SELECT * FROM NetworkDist
ORDER BY DimProductID, WarehouseState
""")
input_networknode.createOrReplaceTempView('input_networknode')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE OR REPLACE TABLE silver_thor.input_networknode
# MAGIC AS
# MAGIC SELECT * FROM input_networknode
