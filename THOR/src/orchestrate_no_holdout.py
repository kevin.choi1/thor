# Databricks notebook source
# MAGIC %run ./fitter

# COMMAND ----------

# MAGIC %run ./train_test

# COMMAND ----------

# MAGIC %run ./pre_process

# COMMAND ----------

# MAGIC %run ./post_process

# COMMAND ----------

import warnings
warnings.filterwarnings("ignore")

class Orchestrate:
    
    def __init__(self, forecast_horizon, holdout_length, sku_id, time_input, forecast_output, forecast_type, target, features, time_grain, model):
        self.forecast_horizon = forecast_horizon
        self.holdout_length = holdout_length
        self.sku_id = sku_id
        self.time_input = time_input
        self.forecast_output = forecast_output
        self.forecast_type = forecast_type
        self.target = target
        self.features = features
        self.time_grain = time_grain
        self.model = model
        
    def compile_results(self, input):

        ### Main Program
        forecast_list = []
        
        #####  MAIN CODE
        id = input[self.sku_id].min()
        ts_category = input['ts_category'].min()
        start = input[self.time_input].min()

        # used to calculate the time series from start - end
        data_length = input['data_length'].min()

        # pre-process
        model_dictionary = {}

        # forecast_horizon must be the longest time length
        time_series = Preprocess.start_end_date(data_length=data_length, 
                                                start=start,
                                                forecast_horizon=forecast_horizon[3], 
                                                date_type=self.time_grain)
        
        model_dictionary['time_series'] = np.array(time_series)

        if ts_category == 'npr':
            
            train_test_obj = Train_Test(self.time_grain, self.holdout_length)
            train = input
            test = input.head(1)
        
        else:
           
            train_test_obj = Train_Test(self.time_grain, self.holdout_length)
            train = input
            test = input.head(1)

        # Test set with target
        y_train = train[target]
        y_train = pd.Series(np.array(train[self.target]), index=np.array(pd.to_datetime(train[self.time_input])))
        y_train = y_train.asfreq(self.time_grain)
        y_test = test[self.target]
        train_length = len(train)
        test_length = len(test)
        
        # best model
        model_name = input[self.model].min()
        
        if self.forecast_type['multivariate']:
            #x_train = train['Price'].to_numpy().reshape(-1,1)
            # for now make everything x_train = None (does not work for NPR skus)
            x_train = None
        else:
            x_train = None

        if ts_category == 'npr' or ts_category == 'less_half_year':

            model_dictionary = Fitter(train_length = train_length,
                                      test_length = test_length,
                                      holdout_length = self.holdout_length, 
                                      forecast_horizon = self.forecast_horizon,
                                      forecast_output = self.forecast_output).model_fit_fcst(y_train = y_train, 
                                                                                           y_test = y_test,
                                                                                           train = train,
                                                                                           target = target, 
                                                                                           model = 'ewm',
                                                                                           time_input=self.time_input,
                                                                                           ts_category = ts_category,
                                                                                           features = self.features,
                                                                                           x_train = x_train,
                                                                                           model_dictionary=None)

            # Post Process
            postprocess_obj = Postprocess(holdout_length = self.holdout_length, forecast_horizon = self.forecast_horizon)
            
            
            try:
                # Compile Results to fix data structure
                model_results = postprocess_obj.compile_results(
                                                               fit_results = model_dictionary,
                                                               train_length = train_length,
                                                               test_length = test_length,
                                                               npr = ts_category,
                                                               columns = ['date_length', 'holdout_forecast', 'five_forecast', 'nine_forecast', 'thirteen_forecast', 'fifty_two_forecast', 'y_test'])

                # Calculate Accuracy
                model_results = postprocess_obj.calculate_accuracy(
                                                                   model_results = model_results,
                                                                   holdout_forecast_name = 'holdout_forecast',
                                                                   y_test_name = 'y_test'
                                                                  )
                model_results['id'] = id
                model_results['model'] = 'ewm'
                model_results['ts'] = time_series
                model_results['data_length'] = data_length
                model_results['training_length'] = train_length
                model_results['holdout_length'] = test_length
                model_results['label'] = input['Label']
                model_results['ts_category'] = ts_category

                # appended dataframe model_results
                forecast_list.append(model_results)
            
            except Exception as e:
                model_results['id'] = id
                model_results['model'] = model
                model_results['ts'] = time_series
                model_results['data_length'] = data_length
                model_results['training_length'] = train_length
                model_results['holdout_length'] = holdout_length
                model_results['label'] = input['Label']
                model_results['ts_category'] = ts_category
                model_results['holdout_mae'] = np.nan
                model_results['holdout_mse'] = np.nan
                model_results['holdout_mape'] = np.nan
                model_results['holdout_smape'] = np.nan
                model_results['holdout_wfa'] = np.nan
                model_results['fitted_values'] = np.nan
                model_results['y_train'] = np.nan
                model_results['error'] = str(e)
                model_results['five_forecast'] = np.nan
                model_results['nine_forecast'] = np.nan
                model_results['thirteen_forecast'] = np.nan
                model_results['y_test'] = np.nan
                model_results['holdout_forecast'] = np.nan
                    
                # appended dataframe model_results
                forecast_list.append(model_results)

        else:
        # ts category is more than 16 weeks
            for model in [model_name]:
            # Fitter output as model dictionary
                model_dictionary = Fitter(train_length = train_length,
                                          test_length = test_length,
                                          holdout_length = self.holdout_length, 
                                          forecast_horizon = self.forecast_horizon, 
                                          forecast_output = self.forecast_output).model_fit_fcst(y_train = y_train, 
                                                                                                 y_test = y_test,
                                                                                                 train = train,
                                                                                                 target = target, 
                                                                                                 model = model,
                                                                                                 time_input = self.time_input, 
                                                                                                 ts_category = ts_category,
                                                                                                 features = self.features,
                                                                                                 x_train = x_train, 
                                                                                                 model_dictionary=None)
                # Post Process
                postprocess_obj = Postprocess(holdout_length = test_length, forecast_horizon = self.forecast_horizon)
                
                # - bug where model_dictionary is length 14 or test size but needs to be 66 or full forecast length
                
                # Compile Results to fix data structure
                try:
                    model_results = postprocess_obj.compile_results(fit_results = model_dictionary,
                                                                    train_length = train_length,
                                                                    test_length = test_length,
                                                                    npr = ts_category,
                                                                    columns = ['date_length', 'holdout_forecast', 'five_forecast', 'nine_forecast', 'thirteen_forecast', 'fifty_two_forecast', 'y_test'])

                    # Calculate Accuracy
                    model_results = postprocess_obj.calculate_accuracy(
                                                                       model_results = model_results,
                                                                       holdout_forecast_name = 'holdout_forecast',
                                                                       y_test_name = 'y_test'
                                                                       )
                    model_results['id'] = id
                    model_results['model'] = model
                    model_results['ts'] = time_series
                    model_results['data_length'] = data_length
                    model_results['training_length'] = train_length
                    model_results['holdout_length'] = holdout_length
                    model_results['label'] = input['Label']
                    model_results['ts_category'] = ts_category

                    # appended dataframe model_results
                    forecast_list.append(model_results)
                    
                except Exception as e:
                    model_results['id'] = id
                    model_results['model'] = model
                    model_results['ts'] = time_series
                    model_results['data_length'] = data_length
                    model_results['training_length'] = train_length
                    model_results['holdout_length'] = holdout_length
                    model_results['label'] = input['Label']
                    model_results['ts_category'] = ts_category
                    model_results['holdout_mae'] = np.nan
                    model_results['holdout_mse'] = np.nan
                    model_results['holdout_mape'] = np.nan
                    model_results['holdout_smape'] = np.nan
                    model_results['holdout_wfa'] = np.nan
                    model_results['fitted_values'] = np.nan
                    model_results['y_train'] = np.nan
                    model_results['error'] = str(e)
                    model_results['five_forecast'] = np.nan
                    model_results['nine_forecast'] = np.nan
                    model_results['thirteen_forecast'] = np.nan
                    model_results['y_test'] = np.nan
                    model_results['holdout_forecast'] = np.nan
                    
                    # appended dataframe model_results
                    forecast_list.append(model_results)
                    
        
        forecast_model_results = pd.concat(forecast_list)
        
        return(forecast_model_results)
