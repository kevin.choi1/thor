# Databricks notebook source
# MAGIC %run ./models/Model_Utils

# COMMAND ----------

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 09:48:49 2021

@author: kevin.choi
"""
## need to create a post process function that collects the various forecast with their appropriate dates
import pandas as pd
import numpy as np

class Postprocess:
    
    def __init__(self, holdout_length, forecast_horizon):
        self.holdout_length = holdout_length
        # get the longest forecast horizon to account for time series length
        self.forecast_horizon = forecast_horizon[3]

    def compile_results(self, fit_results, train_length, test_length, npr, columns):
        """
        :params fit results: dictionary of forecasted values
        :params columns: list of column names
        :return reindex_table: reindex table to include forecast results at the appropriate dates
        """
        # create pandas dataframe for training data (before holdout)
        train_table = pd.DataFrame(dict([(k,pd.Series(v)) for k,v in fit_results.items() if k not in columns]))
        # create pandas dataframe for holdout and forecast
        forecast_table = pd.DataFrame(dict([(k,pd.Series(v)) for k,v in fit_results.items() if k in columns]))
        
        # npr forecast_index should not include holdout as training/test is holdout
        if npr == 'npr':
            
            # NPR holdout is train/test
            forecast_index = list(range(train_length, train_length + self.forecast_horizon))
        
        elif npr == 'less_half_year':
            
            forecast_index = list(range(train_length, train_length + test_length + self.forecast_horizon))
            
        elif npr == 'half_year_plus':
            
            # less_half_year holdout is 1/3 of data
            forecast_index = list(range(train_length, train_length + test_length + self.forecast_horizon))
            
        else:
            
            forecast_index = list(range(train_length, train_length + test_length + self.forecast_horizon))
            
        # set index
        forecast_table = forecast_table.set_index([pd.Index(forecast_index)])
        
        # merge both tables
        reindex_table = pd.merge(train_table, forecast_table, how='outer', left_index=True, right_index=True)
        
        return(reindex_table)
        
    def calculate_accuracy(self, model_results, holdout_forecast_name, y_test_name):
        # remove na values
        holdout_forecast = model_results[holdout_forecast_name].dropna()
        y_test = model_results[y_test_name].dropna()
        
        # Validation
        try:
            model_results['holdout_mae'] = Model_Utils().compute_mae(y_test, holdout_forecast)
            model_results['holdout_mse'] = Model_Utils().compute_mse(y_test, holdout_forecast)
            model_results['holdout_mape'] = Model_Utils().compute_mape(y_test, holdout_forecast)
            model_results['holdout_smape'] = Model_Utils().compute_smape(y_test, holdout_forecast)
            model_results['holdout_wfa'] = Model_Utils().compute_wfa(y_test, holdout_forecast)
        except:
            model_results['holdout_mae'] = np.nan
            model_results['holdout_mse'] = np.nan
            model_results['holdout_mape'] = np.nan
            model_results['holdout_smape'] = np.nan
            model_results['holdout_wfa'] = np.nan
        
        return(model_results)
