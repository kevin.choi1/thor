# Databricks notebook source
# MAGIC %run ./train_test

# COMMAND ----------

from datetime import datetime, timedelta
import pandas as pd

class Preprocess:
    def __init__(self, time_grain, holdout_length, target, features, time_input):
        self.time_grain = time_grain
        self.holdout_length = holdout_length
        self.target = target
        self.features = features
        self.time_input = time_input
        
    def clean_missing(data, columns):
        """
        Cleaning function to fill missing NAN to 0
    
        Parameters
        ----------
        data: TYPE pandas dataframe
            input data time series
            
        columns : TYPE string or list
            columns of data that need to be cleaned can be a string or a list of strings (exs) 'sales_order_history' and 'sales_order_history', 'clean_sales_order_history'
          
        Returns
        -------
        pandas dataframe: TYPE
            cleaned pandas dataframe
        """
        data[columns] = data[columns].fillna(0)
        
        return(data)
    
    def start_end_date(data_length, start, forecast_horizon, date_type):
        """
        :param data_length: INT data length
        :param columns: date column name
        :param date_type: daily (D), weekly (W), monthly (M)
        :param start: start date yyyy-mm-dd
        :param end: end date yyyy-mm-dd
        :return date_range: time date array complete from start to end
        """
        date_range = pd.date_range(start=start, periods=data_length+forecast_horizon, freq=date_type).to_pydatetime().tolist()
        
        return(date_range)
      
    def start_end_week_number(date_column):
        """
        :param date_column: a list of dates
        :return week_range: converted week number as a list
        """
        week_range = pd.Series(date_column).dt.week
        
        return(week_range)
    
    def start_end_year_number(date_column):
        """
        :param date_column: a list of dates
        :return year_range: converted week number as a list
        """
        year_range = pd.Series(date_column).dt.year
        
        return(year_range)

    def rolling_thirty_average(data, variable, time_variable):
        """
        :params data:
        :params variable:
        :params time_variable:
        :return constant value:
        """
        latest_date = data[time_variable].max()
        last_thirty_days = latest_date - timedelta(days=30)
        data['avg_30_day_price'] = data[(data[time_variable]<=latest_date) & (data[time_variable]>=last_thirty_days)]
        
        return(data)

    def group_for_parallel(self, data, response, group):
        """
        :param data: pandas dataframe
        :param response: dependent variable string
        :param group: group by statement
        :param time_input: time_series based column
        :return: list of group by dataframes
        """
        groups = list()

        for k, v in data.groupby(group):
            if(v[response].sum() != 0):
                # sort months
                #v = v.sort_values(by=time_input) 
                # Calculate group by columns
                v['data_length'] = len(v)
                train_test_obj = Train_Test(self.time_grain, self.holdout_length)
                v = train_test_obj.train_test_label(v, self.time_input)
                groups.append(v)
                
            else:
                pass

        return(groups) 
