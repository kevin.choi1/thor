# Databricks notebook source
# MAGIC %run ./post_process

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,StringType,TimestampType,DoubleType,IntegerType, DateType
from pyspark.sql.functions import pandas_udf, PandasUDFType

class ExternalForecast:
    ### all parameters and column names are hardcoded due to pandas_udf being stupid, and it is a static method
    
    result_schema = StructType([
    StructField('created_on', DateType(), True),
    StructField('id', IntegerType(), True), 
    StructField('model', StringType(), True),
    StructField('label', StringType(), True),
    StructField('ts_category', StringType(), True),
    StructField('error', StringType(), True),
    StructField('ts', DateType(), True),
    StructField('data_length', IntegerType(), True),
    StructField('training_length', IntegerType(), True),
    StructField('holdout_length', IntegerType(), True),
    StructField('y_train', DoubleType(), True),
    StructField('fitted_values', DoubleType(), True),
    StructField('holdout_forecast', DoubleType(), True),
    StructField('five_forecast', DoubleType(), True),
    StructField('nine_forecast', DoubleType(), True),
    StructField('thirteen_forecast', DoubleType(), True),
    StructField('fifty_two_forecast', DoubleType(), True),
    StructField('holdout_mae', DoubleType(), True),
    StructField('holdout_mse', DoubleType(), True),
    StructField('holdout_mape', DoubleType(), True),
    StructField('holdout_smape', DoubleType(), True),
    StructField('holdout_wfa', DoubleType(), True)
    ])
    
    @pandas_udf(result_schema, PandasUDFType.GROUPED_MAP)  
    def npr_spark_wrapper(data):
        # convert to time based data types
        data['ts'] = pd.to_datetime(data['ts'])
        data['created_on'] = pd.to_datetime(data['created_on'])
        # sort values based on time column
        data = data.sort_values(by='ts')
        
        # create a new dataframe to store
        v = pd.DataFrame()
        
        # column calculation
        v['created_on'] = data['created_on']
        v['id'] = data['id']
        v['model'] = 'npr'
        v['label'] = data['label']
        v['ts_category'] = 'npr'
        v['error'] = np.nan
        v['ts'] = data['ts']
        v['data_length'] = 16
        v['training_length'] = 16
        v['holdout_length'] = 16
        v['y_train'] = data['WeeklySalesOOS']
        v['fitted_values'] = data['forecast']
        # only 16 weeks of holdout are before created_on
        v['holdout_forecast'] = np.where(data['ts'] < data['created_on'].min(), data['forecast'], np.nan)
        v['five_forecast'] = data[data['label'] == 'Forecast']['forecast'][0:5]
        v['nine_forecast'] = data[data['label'] == 'Forecast']['forecast'][0:9]
        v['thirteen_forecast'] = data[data['label'] == 'Forecast']['forecast'][0:13]
        v['fifty_two_forecast'] = np.where(data['label'] == 'Forecast', data['forecast'], np.nan)
            
        # calculate error metrics in holdout period
        v = Postprocess(holdout_length = 16, forecast_horizon = [5, 9, 13, 52]).calculate_accuracy(model_results = v, holdout_forecast_name = 'y_train', y_test_name = 'holdout_forecast')
    
        return(v) 
    
    @pandas_udf(result_schema, PandasUDFType.GROUPED_MAP)  
    def vcs_spark_wrapper(data):
        # convert to time based data types
        data['ts'] = pd.to_datetime(data['ts'])
        data['created_on'] = pd.to_datetime(data['created_on'])
        # sort values based on time column
        data = data.sort_values(by='ts')
        
        # create a new dataframe to store
        v = pd.DataFrame()
        
        # column calculation
        v['created_on'] = data['created_on']
        v['id'] = data['id']
        v['model'] = 'vcs'
        v['label'] = data['label']
        v['ts_category'] = 'vcs'
        v['error'] = np.nan
        v['ts'] = data['ts']
        v['data_length'] = 16
        v['training_length'] = 16
        v['holdout_length'] = 16
        # need a better way to calculate the data length for now 16 is a placeholder
        v['data_length'] = 16
        v['training_length'] = 16
        v['holdout_length'] = 16
        v['y_train'] = data['WeeklySalesOOS']
        v['fitted_values'] = data['forecast']
        # only 16 weeks of holdout are before created_on
        v['holdout_forecast'] = np.where(data['ts'] < data['created_on'].min(), data['forecast'], np.nan)
        v['five_forecast'] = data[data['label'] == 'Forecast']['forecast'][0:5]
        v['nine_forecast'] = data[data['label'] == 'Forecast']['forecast'][0:9]
        v['thirteen_forecast'] = data[data['label'] == 'Forecast']['forecast'][0:13]
        v['fifty_two_forecast'] = np.where(data['label'] == 'Forecast', data['forecast'], np.nan)
            
        # calculate error metrics in holdout period
        v = Postprocess(holdout_length = v['training_length'].min(), forecast_horizon = [5, 9, 13, 52]).calculate_accuracy(model_results = v, holdout_forecast_name = 'y_train', y_test_name = 'holdout_forecast')
    
        return(v) 
