# Databricks notebook source
from pyspark.sql.types import DoubleType
from pyspark.sql.functions import col, lit, udf, when

def replace_infs(c, v):
    is_infinite = c.isin([
        lit("+Infinity").cast("double"),
        lit("-Infinity").cast("double")
    ])
    return when(c.isNotNull() & is_infinite, v).otherwise(c)
