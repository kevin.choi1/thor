# Databricks notebook source
import numpy as np

class Train_Test:
    
    def __init__(self, time_grain, holdout_length):
        self.time_grain = time_grain
        self.holdout_length = holdout_length
        
    def train_test_split(self, data):
        """
        Cleaning function to fill missing NAN to 0
    
        Parameters
        ----------
        data: TYPE pandas dataframe
            input of time series data
          
        Returns
        -------
        pandas dataframe: TYPE
            cleaned pandas dataframe with train and test
        """
        
        # Create train/test data
        #train = data[:len(data)-self.test_length]
        #test = data[len(data)-self.test_length:]
        
        # Create train/test data
        train = data[data['Label'] == 'Train']
        test = data[data['Label'] == 'Test']

        return(train, test)
    
    def train_test_npr_split(self, data):
        train = data[data['Label'] == 'NPR: Train and Test']
        test = data[data['Label'] == 'NPR: Train and Test']
        
        return(train, test)
    
    def train_test_label(self, data, time_input):
        """
        :params data:
        :params time_input:
        :return data: train, test and train less than holdout length is all train  
        """
        # idea anything bellow 52 weeks maybe train test split should be 1/3 test and 2/3 train
        data = data.sort_values(by=time_input)
        data.reset_index(drop=True, inplace=True)
        
        if data['data_length'].min() <= self.holdout_length:
            
            data.loc[0:len(data),'Label'] = 'NPR: Train and Test'
            data['ts_category'] = 'npr'

        elif((data['data_length'].min() > self.holdout_length) & ((data['data_length'].min() <= 24))):
            
            data.loc[0:self.holdout_length-1, 'Label'] = 'Train'
            data.loc[self.holdout_length:, 'Label'] = 'Test'
            data['ts_category'] = 'less_half_year'
            
        # at this point we want more training data than test
        elif((data['data_length'].min() > 24) & ((data['data_length'].min() <= 52))):
            
            test_num = int(len(data) * 0.33)
            train_num = len(data) - test_num
            data.loc[0:train_num-1, 'Label'] = 'Train'
            data.loc[train_num:, 'Label'] = 'Test'
            data['ts_category'] = 'half_year_plus'
            
        # at this point we are test being 1/3 of the data
        else:
            
            data.loc[0:len(data)-self.holdout_length, 'Label'] = 'Train'
            data.loc[len(data)-self.holdout_length:len(data)-1, 'Label'] = 'Test'
            data['ts_category'] = 'year_plus'           
            
        return(data)
 
    def X_Y_split_1D(self, train, test, features):
        """
        Cleaning function to fill missing NAN to 0
    
        Parameters
        ----------
        data: TYPE pandas dataframe
            input of time series data
            
        time_grain : TYPE string
            string input either 'monthly', 'weekly', 'daily'

        test_length : TYPE INT
            will determine the length test
          
        Returns
        -------
        pandas dataframe: TYPE
            cleaned pandas dataframe with train and test
        """
            
        X_train, Y_train = np.array(train[self.features]), np.array(train[self.target])
        X_test, Y_test = np.array(test[self.features]), np.array(test[self.target])
        
        return(X_train, Y_train, X_test, Y_test)
    
    def X_Y_split_2D(self, train, test, features):
        """
        Cleaning function to fill missing NAN to 0
    
        Parameters
        ----------
        data: TYPE pandas dataframe
            input of time series data
            
        time_grain : TYPE string
            string input either 'monthly', 'weekly', 'daily'

        test_length : TYPE INT
            will determine the length test
          
        Returns
        -------
        pandas dataframe: TYPE
            cleaned pandas dataframe with train and test
        """
            
        X_train, Y_train = np.vstack(train[self.features]), np.vstack(train[self.target])
        X_test, Y_test = np.vstack(test[self.features]), np.vstack(test[self.target])
        
        return(X_train, Y_train, X_test, Y_test)     
