# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ## Historial Forecasts
# MAGIC  
# MAGIC | Detail Tag | Information |
# MAGIC |------------|-------------|
# MAGIC |Created By: | Kevin Choi |
# MAGIC |Support email: | kevin.choi@spreetail.com|
# MAGIC |Business or Technical purpose: |Automation of weekly historical forecasts |
# MAGIC |Input Data Source |<li>gold_thor.fcst_best_model_results</li><li>gold_thor.fcst_best_model_results_archive</li><li>silver_thor.input_oossales</li>|
# MAGIC |Update Datasets |<li>gold_thor.fcst_snapshot_archive</li>|
# MAGIC |Output Datasets |<li>gold_thor.fcst_snapshot_archive</li>|
# MAGIC 
# MAGIC ## Development Log
# MAGIC 
# MAGIC | Date | Developed By | Reason |
# MAGIC |:----:|--------------|--------|
# MAGIC |15th March 2022 | Kevin Choi |Automation of weekly historical forecasts to create metrics|
# MAGIC 
# MAGIC **Note:** Add any specific note that requires attention.

# COMMAND ----------

# MAGIC %md ##### Step 1: Table ingestion

# COMMAND ----------

# latest created_on date renamed as publish date
forecast_created_on = spark.sql("""select distinct(created_on) as forecast_created_on from silver_thor.input_oossales""").toPandas().min().astype(str)[0]
# year, month, day of created_on date
year_created_on = forecast_created_on[0:4]
month_created_on = forecast_created_on[5:7]
day_created_on = forecast_created_on[8:10]
concat_date = year_created_on + month_created_on + day_created_on

print('year: ', year_created_on)
print('month: ', month_created_on)
print('day: ', day_created_on)
print('concat_date: ', concat_date)

concat = 'bronze_spreedw_{}'.format(concat_date)
print('concat_date for incremental load: ', concat)

# COMMAND ----------

# latest created_on date renamed as publish date
publish_created_on = spark.sql("""select max(created_on) as publish_created_on from gold_thor.fcst_best_model_results""").toPandas().min().astype(str)[0]
# year, month, day of created_on date
year_created_on = publish_created_on[0:4]
month_created_on = publish_created_on[5:7]
day_created_on = publish_created_on[8:10]

print('year: ', year_created_on)
print('month: ', month_created_on)
print('day: ', day_created_on)

# create publish_created_on as string
publish_created_on = """'{}'""".format(publish_created_on)
print('publish date: ', publish_created_on)

# COMMAND ----------

# MAGIC %md ##### Step 2: Insert the newly archived forecasts

# COMMAND ----------

# run seasonality
fcst_top_ranking_seasonality_table = spark.sql('SELECT VendorCategory, CategoryID, Correlation, LPAD(WeekNumber, 2, "00") AS WeekNumber, Seasonality FROM silver_thor.fcst_top_vendor_category_correlation')
fcst_top_ranking_seasonality_table.createOrReplaceTempView('fcst_top_ranking_seasonality_table')

# COMMAND ----------

# run input_oossales
spark.sql(
"""
WITH
DateTable AS (
SELECT DISTINCT
MIN(DimDateID) AS DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) as WeekStartDate
,CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")) AS FiscalYearWeek
,FiscalMonth
,CONCAT(FiscalYear, LPAD(FiscalMonth, 2, "00")) AS FiscalYearMonth
,FiscalYear
,LPAD(FiscalWeek, 2, "00") AS WeekNumber
FROM {1}.dim_date
GROUP BY DATE((LEFT(FiscalWeekRange, 10))), CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")), LPAD(FiscalWeek, 2, "00"), FiscalMonth, FiscalYear
ORDER BY DimDateID
),

ThisWeek AS (
SELECT
{0} AS TodaysDate
,DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) AS ThisWeek
FROM {1}.dim_date
WHERE DATE(Date) = DATE({0})
),

QuantityOnHand AS (
SELECT
CAT.DimProductID
,CAT.ItemID
,CAT.PartNumber
,QOH.DimDateID
,CAL.WeekStartDate
,QOH.QuantityOnHandStartOfDayNetwork AS QuantityOnHandUnfilled
FROM {1}.fact_productperformanceinventory AS QOH
LEFT JOIN {1}.dim_product AS CAT ON QOH.ItemID = CAT.ItemID
LEFT JOIN DateTable AS CAL ON QOH.DimDateID = CAL.DimDateID
WHERE CAL.WeekStartDate IS NOT NULL --AND CAT.DimProductID IN (10319, 60743, 62850)
),

HistoricalSales AS (
SELECT
VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,DATE((LEFT(CAL.FiscalWeekRange, 10))) AS WeekStartDate
,SUM(VC.Quantity) AS ItemsShipped
FROM {1}.fact_valuechain AS VC
LEFT JOIN {1}.dim_date AS CAL ON CAL.DimDateID = VC.DimDateOfOrderID
LEFT JOIN {1}.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
GROUP BY VC.DimShippedProductID, PROD.ItemID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
ORDER BY VC.DimShippedProductID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
),

DimShippedIDList AS (
SELECT DISTINCT
CONCAT(PROD.Supplier, "_", PROD.GoogleCategoryLevel1) AS VendorCategory
,PROD.Supplier
,VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,PROD.GoogleCategoryLevel1
FROM {1}.fact_valuechain AS VC
LEFT JOIN {1}.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
--WHERE VC.DimShippedProductID IN (10319, 60743, 62850)
),

KeyTable AS (
SELECT DISTINCT
DimShippedIDList.*
,WeekStartDate
FROM DimShippedIDList
CROSS JOIN DateTable
),

SalesQOHUnfilled AS (
SELECT
KeyTable.VendorCategory
,KeyTable.Supplier
,KeyTable.PartNumber
,KeyTable.ItemID
,KeyTable.DimShippedProductID
,KeyTable.GoogleCategoryLevel1
,SEAS.CategoryID
,SEAS.Correlation
,DateTable.DimDateID
,KeyTable.WeekStartDate
,DateTable.FiscalYearWeek
,DateTable.WeekNumber
,DateTable.FiscalMonth
,DateTable.FiscalYearMonth
,DateTable.FiscalYear
,COALESCE(HistoricalSales.ItemsShipped, 0) AS ItemsShipped
,QuantityOnHand.QuantityOnHandUnfilled
,COALESCE(SEAS.Seasonality, 0.015) AS Seasonality
FROM KeyTable
LEFT JOIN QuantityOnHand ON QuantityOnHand.DimProductID = KeyTable.DimShippedProductID AND QuantityOnHand.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN HistoricalSales ON HistoricalSales.DimShippedProductID = KeyTable.DimShippedProductID AND HistoricalSales.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN DateTable ON DateTable.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN fcst_top_ranking_seasonality_table AS SEAS ON SEAS.VendorCategory = KeyTable.VendorCategory AND SEAS.WeekNumber = DateTable.WeekNumber
WHERE KeyTable.WeekStartDate <= CURRENT_TIMESTAMP() AND KeyTable.WeekStartDate >= '2014-12-21' --AND KeyTable.DimShippedProductID IN (10319, 60743, 62850, 72790)
ORDER BY KeyTable.DimShippedProductID, KeyTable.WeekStartDate
),

CumulativeSum AS (
SELECT
SalesQOHUnfilled.*
,SUM(ItemsShipped) OVER (PARTITION BY DimShippedProductID ORDER BY WeekStartDate) AS CumulativeSum
,SUM(SalesQOHUnfilled.ItemsShipped) OVER (PARTITION BY SalesQOHUnfilled.DimShippedProductID ORDER BY SalesQOHUnfilled.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING) AS AnnualSales
FROM SalesQOHUnfilled
ORDER BY DimShippedProductID, WeekStartDate
),

ActiveStatus AS (
SELECT
CumulativeSum.*
,CASE WHEN CumulativeSum.CumulativeSum = 0 THEN 'Inactive' ELSE 'Active' END AS ActiveStatus
FROM CumulativeSum
),

InStockStatus AS (
SELECT
ActiveStatus.*
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 0 ELSE ActiveStatus.QuantityOnHandUnfilled END AS QuantityOnHand
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 'Inactive' WHEN ActiveStatus.QuantityOnHandUnfilled < 1 THEN 'OOS' ELSE 'InStock' END AS InStockStatus
FROM ActiveStatus
ORDER BY ActiveStatus.DimShippedProductID, ActiveStatus.WeekStartDate
),

QOHFilled AS (
SELECT
q.*
,QOHPartition
,FIRST_VALUE(q.QuantityOnHand) OVER (PARTITION BY QOHPartition ORDER BY q.DimShippedProductID, q.WeekStartDate) AS QuantityOnHandFilled
FROM (
SELECT
InStockStatus.*
,SUM(CASE WHEN QuantityOnHand IS NULL THEN 0 ELSE 1 END) OVER (ORDER BY DimShippedProductID, WeekStartDate) AS QOHPartition
FROM InStockStatus
ORDER BY DimShippedProductID
) AS q
),

RelevantVariables AS (
SELECT
QOHFilled.VendorCategory
,QOHFilled.Supplier
,QOHFilled.PartNumber
,QOHFilled.DimShippedProductID
,QOHFilled.ItemID
,QOHFilled.GoogleCategoryLevel1
,QOHFilled.CategoryID
,QOHFilled.Correlation
,QOHFilled.DimDateID
,QOHFilled.WeekStartDate
,QOHFilled.FiscalYearWeek
,QOHFilled.WeekNumber
,QOHFilled.FiscalYear
,QOHFilled.FiscalMonth
,QOHFilled.FiscalYearMonth
,QOHFilled.ItemsShipped
,QOHFilled.QuantityOnHandFilled AS QuantityOnHand
,QOHFilled.Seasonality
,QOHFilled.CumulativeSum
,QOHFilled.ActiveStatus
,QOHFilled.InStockStatus
,QOHFilled.AnnualSales
,CASE WHEN QOHFilled.InStockStatus = 'InStock' OR QOHFilled.InStockStatus = 'Inactive' THEN QOHFilled.Seasonality ELSE 0 END AS WeeklyInStockSeasonality
FROM QOHFilled
),

InStockSeasonality AS (
SELECT
RelevantVariables.*
,ROUND(SUM(RelevantVariables.WeeklyInStockSeasonality) OVER (PARTITION BY RelevantVariables.DimShippedProductID ORDER BY RelevantVariables.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING), 4) AS InStockSeasonality
FROM RelevantVariables
ORDER BY RelevantVariables.DimShippedProductID, RelevantVariables.WeekStartDate
),

AnnualSalesOOS AS (
SELECT
InStockSeasonality.*
,CASE
WHEN InStockSeasonality >= .9999 THEN AnnualSales
WHEN (InStockSeasonality < .1) THEN (AnnualSales / .1)
ELSE (AnnualSales / InStockSeasonality)
END AS AnnualSalesOOS
FROM InStockSeasonality
),

FinalQuery AS (
SELECT
AnnualSalesOOS.*
,CASE WHEN InStockStatus = 'OOS' THEN Seasonality * AnnualSalesOOS ELSE ItemsShipped END AS WeeklySalesOOS
,{0} AS TodaysDate
FROM AnnualSalesOOS
LEFT JOIN ThisWeek ON ThisWeek.TodaysDate = TodaysDate
WHERE DATE(AnnualSalesOOS.WeekStartDate) < DATE(ThisWeek.ThisWeek)
ORDER BY Supplier, DimShippedProductID, WeekStartDate
)

SELECT
VendorCategory
,Supplier
,PartNumber
,ItemID
,DimShippedProductID AS DimProductID
,GoogleCategoryLevel1
,CategoryID
,Correlation
,DimDateID
,FiscalYearWeek
,FiscalYear
,WeekNumber
,WeekStartDate
,FiscalYearMonth
,FiscalMonth
,ActiveStatus
,InStockStatus
,Seasonality
,COALESCE(InStockSeasonality, 0) AS InStockSeasonality
,QuantityOnHand
,COALESCE(ItemsShipped, 0) AS ItemsShipped
,COALESCE(WeeklySalesOOS, 0) AS WeeklySalesOOS
,COALESCE(AnnualSales, 0) AS AnnualSales
,COALESCE(AnnualSalesOOS, 0) AS AnnualSalesOOS
,DATE({0}) AS created_on
FROM FinalQuery
WHERE WeekStartDate >= '2015-12-27'
ORDER BY Supplier, DimProductID, WeekStartDate
""".format(forecast_created_on, concat)).createOrReplaceTempView('input_oossales')

# COMMAND ----------

# MAGIC %md ##### Step 2: Join in new weekly sales data to historical forecasts

# COMMAND ----------

# join in new weekly sales data to historical forecasts
spark.sql(
"""
-- join the latest archived forecasts with PartNumber ,GCL1, ItemGrade
WITH
fcst_best_model AS (
    SELECT  
    b.PartNumber, 
    b.GoogleCategoryLevel1, 
    ig.ItemGrade,
    b.ItemID, 
    a.* 
        FROM (SELECT
              * 
              FROM gold_thor.fcst_best_model_results_archive) AS a
        LEFT JOIN (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM input_oossales GROUP BY GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b ON a.ID = b.DimProductID
        LEFT JOIN silver_thor.fcst_itemgrade ig ON a.id = ig.DimProductID
),

-- thirteen week future forecast
thirteen_fcst_best_model AS (
    SELECT 
    a.*
        FROM
            (SELECT
             PartNumber, 
             ItemGrade, 
             GoogleCategoryLevel1, 
             ItemID, 
             id, 
             created_on, 
             ts, 
             model, 
             label, 
             ts_category, 
             data_length, 
             y_train AS actuals, 
             thirteen_forecast,
             adj_thirteen_forecast,
             positive_margin_sales_percent,
             holdout_mae, 
             holdout_mse, 
             holdout_mape, 
             holdout_smape,
             holdout_wfa, 
             RANK() over (partition BY created_on, id ORDER BY ts ASC) AS forecast_week 
                 FROM fcst_best_model) AS a
    WHERE a.forecast_week <= 13
    ORDER BY a.id, a.forecast_week ASC
),

-- lastest week of actuals
latest_week_actuals AS (SELECT 
                        * 
                            FROM input_oossales 
                        WHERE WeekStartDate = (SELECT
                                               max(WeekStartDate) 
                                                   FROM input_oossales)  
)

--latest_week_actuals AS (SELECT
--                        *
--                            FROM input_oossales WHERE WeekStartDate = '2022-02-06')

-- thirteen week forecast table and updating the first week of actuals
SELECT 
r.PartNumber, 
r.GoogleCategoryLevel1,
ig.ItemGrade,
case
when r.data_length <= 24 then 'less than 24' 
when r.data_length > 24 AND r.data_length <= 52 then 'between 24 and 52'
when r.data_length > 52 then 'over 52 weeks'
when r.data_length > 104 then 'over 104 weeks'
end AS time_series_category,
r.ItemID, 
r.id as DimProductID , 
r.created_on, 
r.ts,
r.forecast_week,
r.model, 
r.label, 
r.ts_category, 
r.data_length, 
COALESCE(r.actuals, actuals.WeeklySalesOOS) AS actuals,
r.thirteen_forecast,
r.adj_thirteen_forecast,
r.positive_margin_sales_percent,
r.holdout_mae, 
r.holdout_mse, 
r.holdout_mape, 
r.holdout_smape,
r.holdout_wfa
    FROM thirteen_fcst_best_model as r
    LEFT JOIN latest_week_actuals AS actuals ON r.id = actuals.DimProductID AND r.ts = actuals.WeekStartDate
    LEFT JOIN silver_thor.fcst_itemgrade ig ON r.id = ig.DimProductID
WHERE r.id != 212506
ORDER BY r.id, r.forecast_week
""").createOrReplaceTempView('insert_data')

# COMMAND ----------

# MAGIC %md ##### Step 3: Create and replace historical forecast

# COMMAND ----------

# DBTITLE 0,Update Archive with this week's new actuals
# MAGIC %sql
# MAGIC CREATE OR REPLACE TABLE gold_thor.fcst_snapshot_archive  AS
# MAGIC SELECT
# MAGIC fcst.*,
# MAGIC CASE
# MAGIC WHEN fcst.max_lag = fcst.lag THEN fcst.ts
# MAGIC END AS max_lag_date
# MAGIC FROM
# MAGIC (
# MAGIC SELECT
# MAGIC fcst.*,
# MAGIC CASE 
# MAGIC WHEN fcst.actuals IS NOT NULL THEN
# MAGIC SUM(CASE WHEN fcst.actuals IS NOT NULL THEN 1 ELSE 0 END) over (PARTITION BY fcst.created_on, fcst.DimProductID ORDER BY fcst.ts ASC)
# MAGIC WHEN fcst.actuals IS NULL THEN NULL
# MAGIC END AS lag,
# MAGIC CASE 
# MAGIC WHEN fcst.actuals IS NOT NULL THEN
# MAGIC SUM(CASE WHEN fcst.actuals IS NOT NULL THEN 1 ELSE 0 END) over (PARTITION BY fcst.created_on, fcst.DimProductID)
# MAGIC WHEN fcst.actuals IS NULL THEN NULL
# MAGIC END AS max_lag
# MAGIC FROM
# MAGIC (
# MAGIC SELECT
# MAGIC archived.PartNumber,
# MAGIC archived.GoogleCategoryLevel1,
# MAGIC archived.ItemGrade,
# MAGIC archived.time_series_category,
# MAGIC archived.ItemID,
# MAGIC archived.DimProductID,
# MAGIC archived.created_on,
# MAGIC archived.ts,
# MAGIC archived.forecast_week,
# MAGIC archived.model,
# MAGIC archived.label,
# MAGIC archived.ts_category,
# MAGIC archived.data_length,
# MAGIC COALESCE(new_data.actuals, archived.actuals) AS actuals,
# MAGIC archived.thirteen_forecast,
# MAGIC archived.adj_thirteen_forecast,
# MAGIC archived.positive_margin_sales_percent,
# MAGIC archived.holdout_mse,
# MAGIC archived.holdout_mape,
# MAGIC archived.holdout_smape,
# MAGIC archived.holdout_wfa
# MAGIC 
# MAGIC FROM gold_thor.fcst_snapshot_archive as archived
# MAGIC 
# MAGIC LEFT JOIN (SELECT created_on, DimProductID, ts, forecast_week, actuals FROM insert_data WHERE actuals IS NOT null) AS new_data ON new_data.DimProductID = archived.DimProductID AND new_data.ts = archived.ts AND new_data.created_on = archived.created_on
# MAGIC 
# MAGIC ) AS fcst
# MAGIC ) AS fcst
