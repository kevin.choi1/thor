# Databricks notebook source
# DBTITLE 1,Top 100 SKUs by GMV in 2020-2021 (there are DimProductID/ItemID dupes)
top_hundred = spark.sql("""
SELECT 
  PROD.DimProductID
  ,PPM.ItemID
  --,CAL.FiscalYear
  ,SUM(PPM.ItemsShipped) AS ItemsShipped
  ,SUM(PPM.GMV) AS GMV
  ,SUM(PPM.COGS) AS COGS
  
FROM bronze_spreedw.fact_productperformancemutable AS PPM
LEFT JOIN bronze_spreedw.dim_date AS CAL ON PPM.DateID = CAL.DimDateID
LEFT JOIN bronze_spreedw.dim_product AS PROD ON PROD.ItemID = PPM.ItemID 
WHERE CAL.FiscalYear = '2021' OR CAL.FiscalYear = '2020'
GROUP BY   
  PROD.DimProductID
  ,PPM.ItemID
  ,CAL.FiscalYear
ORDER BY GMV DESC
LIMIT 100""")

print('Query for top 100 SKUS based on GMV. Not essential.')

# COMMAND ----------

# DBTITLE 1,Reference Date Table: Use to get WeekStartDate from WeekNumber
date_table = spark.sql("""SELECT DISTINCT
    MIN(DimDateID) AS DimDateID
    ,DATE((LEFT(FiscalWeekRange, 10))) as WeekStartDate
    ,CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")) AS FiscalYearWeek
    ,FiscalYear
    ,LPAD(FiscalWeek, 2, "00") AS WeekNumber
  FROM bronze_spreedw.dim_date
  GROUP BY DATE((LEFT(FiscalWeekRange, 10))), CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")), LPAD(FiscalWeek, 2, "00"), FiscalYear
  ORDER BY DimDateID""")

print('Ingesting date table reference table.')

date_table.createOrReplaceTempView('date_table')

# COMMAND ----------

# DBTITLE 1,Create table for HTSM forecast and join PartNumber and GoogleCategoryLevel1
# use the HTSM dataset as the source
htsm_data = spark.sql("""select * from gold_thor.Original_htsm_item_forecasts""")

print('Ingesting forecast data from HTSM.')

htsm_data.createOrReplaceTempView('htsm_data')

# COMMAND ----------

# DBTITLE 1,Find item history lengths
agg_data = spark.sql("""select DimProductID, COUNT(WeekStartDate) AS data_length, 
    case 
    when COUNT(WeekStartDate) <= 24 then 'less than 24' 
    when COUNT(WeekStartDate) > 24 and COUNT(WeekStartDate) <= 52 then 'between 24 and 52'
    when COUNT(WeekStartDate) > 52 then 'over 52 weeks'
    when COUNT(WeekStartDate) > 104 then 'over 104 weeks'
    end as time_series_category
    from silver_thor.fcst_oos_sales_qoh
    where ActiveStatus = "Active"
    group by DimProductID""")

print('Ingesting best model aggregated to the model. No time component. Includes time_series_category as well.')

agg_data.createOrReplaceTempView('agg_data')

# COMMAND ----------

# use the HTSM dataset as the source
htsm_data = spark.sql("""select p.ItemID, p.GoogleCategoryLevel1, htsm.*, agg.time_series_category from gold_thor.htsm_item_forecasts as htsm 
    left join bronze_spreedw.dim_product p ON htsm.DimProductID = p.DimProductID
    left join agg_data agg ON htsm.DimProductID = agg.DimProductID """)

print('Ingesting forecast data from HTSM.')

htsm_data.createOrReplaceTempView('htsm_data')

# COMMAND ----------

# DBTITLE 1,Create table to compare Rob forecast vs Thor forecast vs HTSM
benchmark_consensus_htsm = spark.sql("""
--- THIS IS BASE 
SELECT

base_table.ItemID,
base_table.DimProductID,
base_table.Category_Level,
base_table.Category_Name,
DATE((LEFT(base_table.date, 10))) as date,
base_table.forecast,
base_table.ROB_Forecast,
base_table.Items_Shipped,
base_table.GoogleCategoryLevel1,
base_table.time_series_category,
ig.ItemGrade,
r.ReplenishStatus

FROM
(
SELECT
htsm.*,
rob.ROB_Forecast,
rob.Items_Shipped
FROM htsm_data as htsm
INNER JOIN
(
SELECT
       
    ROB.ItemID, 
    DT.WeekStartDate, 
    ROB.Items_Shipped, 
    ROB.ROB_Forecast

FROM silver_thor.rob_consensus_forecast AS ROB
INNER JOIN date_table DT 
ON DT.WeekNumber = ROB.Fiscal_Week where DT.FiscalYear == '2021'   
) AS rob
ON DATE((LEFT(htsm.date, 10))) = DATE(rob.WeekStartDate) AND htsm.ItemID = rob.ItemID
) as base_table

---- FIRST JOIN
LEFT JOIN
silver_thor.fcst_itemgrade ig
ON base_table.DimProductID = ig.DimProductID

--- SECOND JOIN
LEFT JOIN
(
select 
ItemID,
case 
when ProcurementStatus in ('Replenish', 'Replenish - Reset', 'Replenish - Amplify', 'Replenish - Manual') then 'Replenish'
when ProcurementStatus in ('Not Ordering - Other', 'Not Ordering - Discontinued', 'Not Ordering - Low Volume', 'Not Ordering - Cost Improvement', 'Promotional Costing') then 'Non-Replenish'
when ProcurementStatus in (null) then 'null'
end ReplenishStatus
from
silver_thor.itemid_procurementstatus_list
) r
ON base_table.ItemID = r.ItemID
""")

print('Ingesting Rob vs HTSM table. Joined on WeekStartDate and ItemID.')

# Spark Sql table
benchmark_consensus_htsm.createOrReplaceTempView('benchmark_consensus_htsm_spark')

print('Converting to pandas.')

# Data filtered for 13 weeks future forecast
df_benchmark_consensus = spark.sql("""select * from benchmark_consensus_htsm_spark""").toPandas()

# create categorical columns based off of holdout (goes beyond just top 25 gmv - used for full analysis of rob vs thor)
all = spark.sql("""select * from benchmark_consensus_htsm_spark
where DimProductID != 212506""").toPandas()

# COMMAND ----------

# DBTITLE 1,Create top 25 GMV skus
#top_25_gmv = spark.sql("""SELECT 
#  top.GMV,
#  agg.*
#FROM agg_data as agg
#INNER JOIN top_hundred as top
#ON agg.id = top.DimProductID
#order by top.GMV, agg.id desc
#limit 25""")

#print('Query for top 25 SKUs based on GMV. Not essential.')
