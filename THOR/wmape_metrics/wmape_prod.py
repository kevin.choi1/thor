# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ## WMAPE Metrics
# MAGIC  
# MAGIC | Detail Tag | Information |
# MAGIC |------------|-------------|
# MAGIC |Created By: | Kevin Choi |
# MAGIC |Support email: | kevin.choi@spreetail.com|
# MAGIC |Business or Technical purpose: |Automation of weekly WMAPE metrics |
# MAGIC |Input Data Source |<li>gold_thor.fcst_snapshot_archive</li>|
# MAGIC |Update Datasets |<li>gold_thor.fcst_metrics</li>|
# MAGIC |Output Datasets |<li>gold_thor.fcst_metrics</li>|
# MAGIC 
# MAGIC ## Development Log
# MAGIC 
# MAGIC | Date | Developed By | Reason |
# MAGIC |:----:|--------------|--------|
# MAGIC |15th March 2022 | Kevin Choi |Automation of weekly WMAPE metrics|
# MAGIC 
# MAGIC **Note:** Add any specific note that requires attention.

# COMMAND ----------

# MAGIC %md ##### Step 1: Get forecast and publish created on dates

# COMMAND ----------

# latest created_on date renamed as publish date
forecast_created_on = spark.sql("""select distinct(created_on) as forecast_created_on from silver_thor.input_oossales""").toPandas().min().astype(str)[0]
# year, month, day of created_on date
year_created_on = forecast_created_on[0:4]
month_created_on = forecast_created_on[5:7]
day_created_on = forecast_created_on[8:10]
concat_date = year_created_on + month_created_on + day_created_on

print('year: ', year_created_on)
print('month: ', month_created_on)
print('day: ', day_created_on)
print('concat_date: ', concat_date)

concat = 'bronze_spreedw_{}'.format(concat_date)
print('concat_date for incremental load: ', concat)

# COMMAND ----------

# latest created_on date renamed as publish date
publish_created_on = spark.sql("""select max(created_on) as publish_created_on from gold_thor.fcst_best_model_results""").toPandas().min().astype(str)[0]

# temp for now
publish_created_on = '2022-03-13'
# year, month, day of created_on date
year_created_on = publish_created_on[0:4]
month_created_on = publish_created_on[5:7]
day_created_on = publish_created_on[8:10]

print('year: ', year_created_on)
print('month: ', month_created_on)
print('day: ', day_created_on)

# create publish_created_on as string
publish_created_on = """'{}'""".format(publish_created_on)
print('publish date: ', publish_created_on)

# COMMAND ----------

# MAGIC %md ##### Step 2: Create table for max lag dates

# COMMAND ----------

# max lag date gold_thor.fcst_snapshot_archive
max_lag_date = spark.sql("""select max(max_lag_date) from gold_thor.fcst_snapshot_archive""").toPandas().min().astype(str)[0]
# format into proper string
max_lag_date_string = """'{}'""".format(max_lag_date)
print('max lag date: ', max_lag_date_string)

# COMMAND ----------

spark.sql(
"""
SELECT
PartNumber,
GoogleCategoryLevel1,
ItemGrade,
time_series_category,
ItemID,
DimProductID,
created_on,
ts,
forecast_week,
model,
label,
ts_category,
data_length,
actuals AS actuals,
thirteen_forecast,
adj_thirteen_forecast,
positive_margin_sales_percent,
holdout_mse,
holdout_mape,
holdout_smape,
holdout_wfa,
max_lag,
lag,
CASE
WHEN lag <= max_lag THEN {0}
ELSE NULL
END AS max_lag_date
FROM gold_thor.fcst_snapshot_archive 
""".format(max_lag_date_string)).createOrReplaceTempView('fcst_with_max_lag_date')

# COMMAND ----------

# for now only start with 2022-02-06 snapshot (rolling cumulative for now)
max_lag = 7
spark.sql("""select * from fcst_with_max_lag_date where max_lag = {0} and max_lag_date = {1} and created_on = '2022-01-23' """.format(max_lag, max_lag_date_string)).createOrReplaceTempView('input_data')

# COMMAND ----------

# MAGIC %md ##### sku level metrics

# COMMAND ----------

sku_thirteen_metrics = spark.sql("""
SELECT 
PartNumber, 
GoogleCategoryLevel1, 
ItemGrade, 
DimProductID, 
created_on as forecast_created_on,
{0} AS publish_created_on,
max_lag AS lag_length,
time_series_category, 
data_length, 
model, 
holdout_mape, 
holdout_smape, 
-- forecasts
SUM(thirteen_forecast) AS cumulative_thor_forecast,
SUM(adj_thirteen_forecast) AS cumulative_thor_adj_forecast,
-- actuals
SUM(actuals) AS cumulative_actuals,
-- wmape
( ABS( SUM(actuals) - SUM(thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) AS sku_thor_wmape,
( ABS( SUM(actuals) - SUM(thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) * SUM(actuals) AS sku_thor_wmape_coef,
-- wmape adj
( ABS( SUM(actuals) - SUM(adj_thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) AS sku_thor_adj_wmape,
( ABS( SUM(actuals) - SUM(adj_thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) * SUM(actuals) AS sku_thor_adj_wmape_coef,
-- bias
( SUM(thirteen_forecast) - SUM(actuals) ) / ( SUM(thirteen_forecast) + SUM(actuals)) AS sku_thor_bias,
-- bias adj
( SUM(adj_thirteen_forecast) - SUM(actuals) ) / ( SUM(adj_thirteen_forecast) + SUM(actuals)) AS sku_thor_adj_bias

  FROM input_data 
  GROUP BY PartNumber, GoogleCategoryLevel1, ItemGrade, DimProductID, created_on, max_lag, time_series_category, data_length, model, holdout_mape, holdout_smape
""".format(publish_created_on))

sku_thirteen_metrics.createOrReplaceTempView('sku_thirteen_metrics')

display(sku_thirteen_metrics)

# COMMAND ----------

# MAGIC %md ##### overall metrics

# COMMAND ----------

overall_metrics = spark.sql("""
SELECT 
'overall' AS metrics_categories_label,
forecast_created_on, 
publish_created_on,
lag_length,
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY forecast_created_on, publish_created_on, lag_length
""")

overall_metrics.createOrReplaceTempView('overall_metrics')

display(overall_metrics)

# COMMAND ----------

# MAGIC %md ##### google category level 1 metrics

# COMMAND ----------

gcl1_thirteen_metrics = spark.sql("""
SELECT 
'gcl1' AS metrics_categories_label,
GoogleCategoryLevel1,  
forecast_created_on,
publish_created_on,
lag_length,
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY GoogleCategoryLevel1, forecast_created_on, publish_created_on, lag_length
""")

gcl1_thirteen_metrics.createOrReplaceTempView('gcl1_thirteen_metrics')

display(gcl1_thirteen_metrics)

# COMMAND ----------

# MAGIC %md ##### item grade metrics

# COMMAND ----------

itemgrade_thirteen_metrics = spark.sql("""
SELECT 
'itemgrade' AS metrics_categories_label,
ItemGrade,  
forecast_created_on, 
publish_created_on,
lag_length, 
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY ItemGrade, forecast_created_on, publish_created_on, lag_length
""")

itemgrade_thirteen_metrics.createOrReplaceTempView('itemgrade_thirteen_metrics')

display(itemgrade_thirteen_metrics)

# COMMAND ----------

# MAGIC %md ##### model metrics

# COMMAND ----------

model_thirteen_metrics = spark.sql("""
SELECT 
'model' AS metrics_categories_label,
model,  
forecast_created_on, 
publish_created_on, 
lag_length,
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY model, forecast_created_on, publish_created_on, lag_length
""")

model_thirteen_metrics.createOrReplaceTempView('model_thirteen_metrics')

display(model_thirteen_metrics)

# COMMAND ----------

# MAGIC %md ##### Union all WMAPE metrics

# COMMAND ----------

# DBTITLE 0,Union all WMAPE metrics
spark.sql(
"""
SELECT metrics_categories_label, 'overall' as metrics_categories, forecast_created_on, DATE(publish_created_on) AS publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM overall_metrics

UNION ALL

SELECT metrics_categories_label, GoogleCategoryLevel1 as metrics_categories, forecast_created_on, DATE(publish_created_on) AS publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM gcl1_thirteen_metrics

UNION ALL

SELECT metrics_categories_label, ItemGrade as metrics_categories, forecast_created_on, DATE(publish_created_on) AS publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM itemgrade_thirteen_metrics

UNION ALL

SELECT metrics_categories_label, model as metrics_categories, forecast_created_on, DATE(publish_created_on) AS publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM model_thirteen_metrics
""").createOrReplaceTempView('metrics')

# COMMAND ----------

# MAGIC %sql
# MAGIC create or replace table gold_thor.fcst_metrics
# MAGIC as
# MAGIC select * from metrics

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from gold_thor.fcst_metrics
