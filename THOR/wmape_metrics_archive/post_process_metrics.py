# Databricks notebook source
# MAGIC %md ##### Step 1: Functions / Import helper functions for calculating accuracy

# COMMAND ----------

# MAGIC %run /Shared/Forecasting/THOR/src/models/Model_Utils

# COMMAND ----------

# MAGIC %run ./src/pre_process

# COMMAND ----------

# MAGIC %md ##### Step 2: SQL queries data ingestion

# COMMAND ----------

# MAGIC %run ./src/sql

# COMMAND ----------

print('Count of time series category based IDS: ')
len(all['DimProductID'].unique())

# COMMAND ----------

# MAGIC %md ##### Step 3: Create categorical columns for 5, 9, 13 week metrics

# COMMAND ----------

#five_forecasts = num_group_for_parallel(data = demo, group = 'DimProductID', num = 4)
#nine_forecasts = num_group_for_parallel(data = demo, group = 'DimProductID', num = 8)
#thirteen_forecasts = num_group_for_parallel(data = demo, group = 'DimProductID', num = 12)
all_thirteen_forecasts = num_group_for_parallel(data = all, group = ['DimProductID'], num = 12, time_input_name = 'ts_date', dependent_variable_name = 'Items_shipped')

# COMMAND ----------

# DBTITLE 1,13 Week All Rob vs Thor (correct WMAPE) - Stop after this if you want to compare SKU based WMAPE
import pandas as pd
from numpy import inf

epilson = 0.001
all_thirteen_out = list()
for input in all_thirteen_forecasts:
    output = {}
    output['DimProductID'] = input['DimProductID'].min()
    output['time_series_category'] = input['time_series_category'].min()
    output['PartNumber'] = input['PartNumber'].min()
    output['GoogleCategoryLevel1'] = input['GoogleCategoryLevel1'].min()
    output['ItemGrade'] = input['ItemGrade'].min()
    output['ReplenishStatus'] = input['ReplenishStatus'].min()
    output['ListingType'] = input['ListingType'].min()
    output['ItemID'] = input['ItemID'].min()
    output['model'] = input['model'].min()
    output['data_length'] = input['data_length'].min()
    output['time_series_category'] = input['time_series_category'].min()
    output['holdout_mape'] = input['holdout_mape'].min()
    output['holdout_smape'] = input['holdout_smape'].min()
    output['cumulative_rob_forecast'] = input['ROB_Forecast'].sum() 
    output['cumulative_thor_forecast'] = input['thirteen_forecast'].sum()
    output['cumulative_actuals'] = input['Items_shipped'].sum()
    # WMAPE caculation
    output['sku_rob_wmape'] = abs(output['cumulative_actuals']-output['cumulative_rob_forecast'])/output['cumulative_actuals'] + epilson
    output['sku_thor_wmape'] = abs(output['cumulative_actuals']-output['cumulative_thor_forecast'])/output['cumulative_actuals'] + epilson
    output['sku_rob_wmape_coef'] = output['cumulative_actuals'] * output['sku_rob_wmape']
    output['sku_thor_wmape_coef'] = output['cumulative_actuals'] * output['sku_thor_wmape']
    # BIAS calculation
    output['sku_bias_thor'] = (output['cumulative_thor_forecast'] - output['cumulative_actuals']) / (output['cumulative_thor_forecast'] + output['cumulative_actuals'])
    output['sku_bias_rob'] =  (output['cumulative_rob_forecast'] - output['cumulative_actuals']) / (output['cumulative_rob_forecast'] + output['cumulative_actuals'])
    all_thirteen_out.append(output)
    
all_thirteen = pd.DataFrame(all_thirteen_out)

# COMMAND ----------

all_thirteen.head()

# COMMAND ----------

# write to db
spark_thirteen_week_wmape_benchmark = spark.createDataFrame(all_thirteen)
spark_thirteen_week_wmape_benchmark.createOrReplaceTempView('spark_thirteen_week_wmape_benchmark')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE or replace TABLE gold_thor.thirteen_week_wmape_benchmark
# MAGIC as
# MAGIC SELECT * from spark_thirteen_week_wmape_benchmark

# COMMAND ----------

# DBTITLE 1,Creates the final WMAPE calculation based off of ListingType
ts_thirteen = group_for_parallel(data = all_thirteen, group = ['ListingType'])

thirteen_output = list()
for input in ts_thirteen:
    output = {}
    output['ListingType'] = input['ListingType'].min()
    output['id_count'] = input['DimProductID'].count()
    output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
    output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
    output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
    output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
    output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
    output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
    output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
    thirteen_output.append(output)
output_wmape_r = pd.DataFrame(thirteen_output)

output_wmape_r.head(10)

# COMMAND ----------

# DBTITLE 1,Creates the final WMAPE calculation based off of ReplenishStatus
ts_thirteen = group_for_parallel(data = all_thirteen, group = ['ReplenishStatus'])

thirteen_output = list()
for input in ts_thirteen:
    output = {}
    output['ReplenishStatus'] = input['ReplenishStatus'].min()
    output['id_count'] = input['DimProductID'].count()
    output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
    output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
    output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
    output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
    output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
    output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
    output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
    thirteen_output.append(output)
output_wmape_r = pd.DataFrame(thirteen_output)

output_wmape_r.head(10)

# COMMAND ----------

# DBTITLE 1,Creates the final WMAPE calculation based off of ItemGrade
ts_thirteen = group_for_parallel(data = all_thirteen, group = ['ItemGrade'])

thirteen_output = list()
for input in ts_thirteen:
    output = {}
    output['ItemGrade'] = input['ItemGrade'].min()
    output['id_count'] = input['DimProductID'].count()
    output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
    output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
    output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
    output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
    output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
    output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
    output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
    thirteen_output.append(output)
output_wmape_ig = pd.DataFrame(thirteen_output)

output_wmape_ig.head(10)

# COMMAND ----------

# DBTITLE 1,Creates the final WMAPE calculation based off of time series category
ts_thirteen = group_for_parallel(data = all_thirteen, group = ['time_series_category'])

thirteen_output = list()
for input in ts_thirteen:
    output = {}
    output['time_series_category'] = input['time_series_category'].min()
    output['id_count'] = input['DimProductID'].count()
    output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
    output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
    output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
    output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
    output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
    output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
    output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
    thirteen_output.append(output)
output_wmape = pd.DataFrame(thirteen_output)

output_wmape.head()

# COMMAND ----------

# DBTITLE 1,Creates the final WMAPE calculation based off of GoogleCategoryLevel1
ts_thirteen = group_for_parallel(data = all_thirteen, group = ['GoogleCategoryLevel1'])

thirteen_output = list()
for input in ts_thirteen:
    output = {}
    output['GoogleCategoryLevel1'] = input['GoogleCategoryLevel1'].min()
    output['id_count'] = input['DimProductID'].count()
    output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
    output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
    output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
    output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
    output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
    output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
    output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
    output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
    thirteen_output.append(output)
output_wmape_category = pd.DataFrame(thirteen_output)

output_wmape_category
