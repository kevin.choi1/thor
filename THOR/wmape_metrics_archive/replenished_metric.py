# Databricks notebook source
# MAGIC %md ##### Step 1: Functions / Import helper functions for calculating accuracy

# COMMAND ----------

# model name [CHANGE THIS]
forecast_model_name = 'thor'

# parameters to save data to database [TRUE = write to db / FALSE = DO NOT (use FALSE if you want to simply see results in memory)]
ts = False
item_grade = False
sku_level = False
gc = False

# input table names [CHANGE THIS]
table_name = 'gold_thor.best_model_forecast_results'

# table with rob forecast and actuals [DO NOT CHANGE THIS]
compare_table = 'silver_thor.rob_consensus_forecast'

#output table names
benchmark_wmape = 'gold_thor.benchmark_{}_sku_13w_wmape'.format(forecast_model_name)
item_grade_wmape = 'gold_thor.benchmark_{}_itemgrade_13w_wmape'.format(forecast_model_name)
ts_wmape = 'gold_thor.benchmark_{}_ts_13w_wmape'.format(forecast_model_name)
gc_wmape = 'gold_thor.benchmark_{}_gcl1_13w_wmape'.format(forecast_model_name)

# COMMAND ----------

benchmark_wmape

# COMMAND ----------

# MAGIC %run /Shared/Forecasting/THOR/src/models/Model_Utils

# COMMAND ----------

# MAGIC %run ./src/pre_process

# COMMAND ----------

# MAGIC %md ##### Step 2: SQL queries data ingestion

# COMMAND ----------

# MAGIC %run ./src/sql

# COMMAND ----------

# MAGIC %md ##### Step 3: Create categorical columns for 13 week metrics

# COMMAND ----------

all_thirteen_forecasts = num_group_for_parallel(data = all, group = ['DimProductID'], num = 12, time_input_name = 'ts_date', dependent_variable_name = 'Items_shipped')

# COMMAND ----------

sku_level_forecasts = sku_lvl_thirteen_forecast(epilson = 0.001, all_thirteen_forecasts = all_thirteen_forecasts)
sku_level_forecasts.head()

# COMMAND ----------

if sku_level == True:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(sku_level_forecasts).createOrReplaceTempView('spark_thirteen_week_wmape_benchmark')

    # spark query
    query = 'CREATE OR REPLACE TABLE {} AS SELECT * FROM {}'.format(benchmark_wmape, 'spark_thirteen_week_wmape_benchmark')

    # spark sql
    spark.sql(query)

    print('Successfully stored {}'.format(benchmark_wmape))
else:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(sku_level_forecasts).createOrReplaceTempView('spark_thirteen_week_wmape_benchmark')

# COMMAND ----------

# MAGIC %md ##### Step 4: Creates the final WMAPE calculation based off of ItemGrade for 13 week metrics

# COMMAND ----------

# DBTITLE 0,Creates the final WMAPE calculation based off of ItemGrade
ts_thirteen = group_for_parallel(data = sku_level_forecasts , group = ['ItemGrade'])

output_wmape_ig = item_grade_lvl_thirteen_forecast(ts_thirteen)

output_wmape_ig.head(10)

# COMMAND ----------

if item_grade == True:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(output_wmape_ig).createOrReplaceTempView('ig_spark_thirteen_week_wmape_benchmark')

    # spark query
    query = 'CREATE OR REPLACE TABLE {} AS SELECT * FROM {}'.format(item_grade_wmape, 'ig_spark_thirteen_week_wmape_benchmark')

    # spark sql
    spark.sql(query)

    print('Successfully stored {}'.format(item_grade_wmape))

else:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(output_wmape_ig).createOrReplaceTempView('ig_spark_thirteen_week_wmape_benchmark')

# COMMAND ----------

# MAGIC %md ##### Step 5: Creates the final WMAPE calculation based off of time series category for 13 week metrics

# COMMAND ----------

# DBTITLE 0,Creates the final WMAPE calculation based off of time series category
ts_thirteen = group_for_parallel(data = sku_level_forecasts, group = ['time_series_category'])

ts_output_wmape = time_series_thirteen_forecast(ts_thirteen)

ts_output_wmape.head()

# COMMAND ----------

# DBTITLE 0,Creates the final WMAPE calculation based off of GoogleCategoryLevel1
if ts == True:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(ts_output_wmape).createOrReplaceTempView('ts_spark_thirteen_week_wmape_benchmark')

    # spark query
    query = 'CREATE OR REPLACE TABLE {} AS SELECT * FROM {}'.format(ts_wmape, 'ts_spark_thirteen_week_wmape_benchmark')

    # spark sql
    spark.sql(query)

    print('Successfully stored {}'.format(ts_wmape))
else:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(ts_output_wmape).createOrReplaceTempView('ts_spark_thirteen_week_wmape_benchmark')

# COMMAND ----------

# MAGIC %md ##### Step 6: Creates the final WMAPE calculation based off of Google Category for 13 week metrics

# COMMAND ----------

gc_thirteen = group_for_parallel(data = sku_level_forecasts, group = ['GoogleCategoryLevel1'])

gc_output_wmape = google_category_thirteen_forecast(gc_thirteen)

gc_output_wmape.head(20)

# COMMAND ----------

if gc == True:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(gc_output_wmape).createOrReplaceTempView('gc_spark_thirteen_week_wmape_benchmark')

    # spark query
    query = 'CREATE OR REPLACE TABLE {} AS SELECT * FROM {}'.format(gc_wmape, 'gc_spark_thirteen_week_wmape_benchmark')

    # spark sql
    spark.sql(query)

    print('Successfully stored {}'.format(gc_wmape))
else:
    # create spark sql dataframe for forecasts
    spark.createDataFrame(gc_output_wmape).createOrReplaceTempView('gc_spark_thirteen_week_wmape_benchmark')

# COMMAND ----------

# MAGIC %md ##### Step 7:  Overll data metrics

# COMMAND ----------

# MAGIC %sql
# MAGIC select ReplenishStatus, ListingType, count(DimProductID) from raw_benchmark_consensus_thor_spark group by ReplenishStatus, ListingType

# COMMAND ----------

# MAGIC %sql
# MAGIC select ReplenishStatus, ListingType, count(distinct(DimProductID)) from raw_benchmark_consensus_thor_spark group by ReplenishStatus, ListingType
