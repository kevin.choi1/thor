# Databricks notebook source
import pandas as pd
from numpy import inf

def sku_lvl_thirteen_forecast(epilson, all_thirteen_forecasts):
    all_thirteen_out = list()
    for input in all_thirteen_forecasts:
        output = {}
        output['DimProductID'] = input['DimProductID'].min()
        output['created_on'] = pd.to_datetime('today').date()
        output['time_series_category'] = input['time_series_category'].min()
        output['PartNumber'] = input['PartNumber'].min()
        output['GoogleCategoryLevel1'] = input['GoogleCategoryLevel1'].min()
        output['ItemGrade'] = input['ItemGrade'].min()
        output['ReplenishStatus'] = input['ReplenishStatus'].min()
        output['ListingType'] = input['ListingType'].min()
        output['ItemID'] = input['ItemID'].min()
        output['model'] = input['model'].min()
        output['data_length'] = input['data_length'].min()
        output['time_series_category'] = input['time_series_category'].min()
        output['holdout_mape'] = input['holdout_mape'].min()
        output['holdout_smape'] = input['holdout_smape'].min()
        output['cumulative_rob_forecast'] = input['ROB_Forecast'].sum() 
        output['cumulative_thor_forecast'] = input['thirteen_forecast'].sum()
        output['cumulative_actuals'] = input['Items_shipped'].sum()
        # WMAPE caculation
        output['sku_rob_wmape'] = abs(output['cumulative_actuals']-output['cumulative_rob_forecast'])/output['cumulative_actuals'] + epilson
        output['sku_thor_wmape'] = abs(output['cumulative_actuals']-output['cumulative_thor_forecast'])/output['cumulative_actuals'] + epilson
        output['sku_rob_wmape_coef'] = output['cumulative_actuals'] * output['sku_rob_wmape']
        output['sku_thor_wmape_coef'] = output['cumulative_actuals'] * output['sku_thor_wmape']
        # BIAS calculation
        output['sku_bias_thor'] = (output['cumulative_thor_forecast'] - output['cumulative_actuals']) / (output['cumulative_thor_forecast'] + output['cumulative_actuals'])
        output['sku_bias_rob'] =  (output['cumulative_rob_forecast'] - output['cumulative_actuals']) / (output['cumulative_rob_forecast'] + output['cumulative_actuals'])
        all_thirteen_out.append(output)

    all_thirteen = pd.DataFrame(all_thirteen_out)
    return(all_thirteen)

def google_category_thirteen_forecast(ts_thirteen):
    thirteen_output = list()
    for input in ts_thirteen:
        output = {}
        output['GoogleCategoryLevel1'] = input['GoogleCategoryLevel1'].min()
        output['id_count'] = input['DimProductID'].count()
        output['created_on'] = pd.to_datetime('today').date()
        output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
        output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
        output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
        output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
        output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
        output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
        output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
        output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
        output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
        thirteen_output.append(output)
    output_wmape = pd.DataFrame(thirteen_output)
    return(output_wmape)
  
    
def item_grade_lvl_thirteen_forecast(ts_thirteen):
    thirteen_output = list()
    for input in ts_thirteen:
        output = {}
        output['ItemGrade'] = input['ItemGrade'].min()
        output['id_count'] = input['DimProductID'].count()
        output['created_on'] = pd.to_datetime('today').date()
        output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
        output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
        output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
        output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
        output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
        output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
        output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
        output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
        output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
        thirteen_output.append(output)
    output_wmape_ig = pd.DataFrame(thirteen_output)
    return(output_wmape_ig)

def time_series_thirteen_forecast(ts_thirteen):
    thirteen_output = list()
    for input in ts_thirteen:
        output = {}
        output['time_series_category'] = input['time_series_category'].min()
        output['created_on'] = pd.to_datetime('today').date()
        output['id_count'] = input['DimProductID'].count()
        output['grp_cumulative_rob_forecast'] = input['cumulative_rob_forecast'].sum() 
        output['grp_cumulative_thor_forecast'] = input['cumulative_thor_forecast'].sum()
        output['grp_cumulative_actuals'] = input['cumulative_actuals'].sum()
        output['rob_wmape_num_cumulative'] = input['sku_rob_wmape_coef'].sum()
        output['thor_wmape_num_cumulative'] = input['sku_thor_wmape_coef'].sum()
        output['rob_wmape'] = output['rob_wmape_num_cumulative']/output['grp_cumulative_actuals']
        output['thor_wmape'] = output['thor_wmape_num_cumulative']/output['grp_cumulative_actuals']
        output['rob_bias'] = (output['grp_cumulative_rob_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_rob_forecast'] + output['grp_cumulative_actuals'])
        output['thor_bias'] = (output['grp_cumulative_thor_forecast'] - output['grp_cumulative_actuals']) / (output['grp_cumulative_thor_forecast'] + output['grp_cumulative_actuals'])
        thirteen_output.append(output)
    output_wmape = pd.DataFrame(thirteen_output)
    return(output_wmape)

# group for parallel function

def num_group_for_parallel(data, group, num, time_input_name, dependent_variable_name):
    """
    :param data: pandas dataframe
    :param response: dependent variable string
    :param group: group by statement
    :param time_input: time_series based column
    :return: list of group by dataframes
    """
    groups = list()

    for k, v in data.groupby(group):
        v = v.sort_values(by=time_input_name) 
        v = v.reset_index(drop=True)
        v[dependent_variable_name] = v[dependent_variable_name].fillna(0)
        v = v.loc[0:num,]
        groups.append(v)
    
    return(groups)

def group_for_parallel(data, group):
    """
    :param data: pandas dataframe
    :param response: dependent variable string
    :param group: group by statement
    :param time_input: time_series based column
    :return: list of group by dataframes
    """
    groups = list()

    for k, v in data.groupby(group):
        groups.append(v)
    return(groups)
