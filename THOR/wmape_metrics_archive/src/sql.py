# Databricks notebook source
# MAGIC %md ##### Step 1: Create data reference table

# COMMAND ----------

# input table names [CHANGE THIS]
table_name = 'gold_thor.best_model_forecast_results'

# table with rob forecast and actuals [DO NOT CHANGE THIS]
compare_table = 'silver_thor.rob_consensus_forecast'

# COMMAND ----------

# DBTITLE 0,Reference Date Table: Use to get WeekStartDate from WeekNumber
date_table = spark.sql("""SELECT DISTINCT
    MIN(DimDateID) AS DimDateID
    ,DATE((LEFT(FiscalWeekRange, 10))) as WeekStartDate
    ,CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")) AS FiscalYearWeek
    ,FiscalYear
    ,LPAD(FiscalWeek, 2, "00") AS WeekNumber
  FROM bronze_spreedw.dim_date
  GROUP BY DATE((LEFT(FiscalWeekRange, 10))), CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")), LPAD(FiscalWeek, 2, "00"), FiscalYear
  ORDER BY DimDateID""")

print('Ingesting date table reference table.')

date_table.createOrReplaceTempView('date_table')

# COMMAND ----------

# MAGIC %md ##### Step 2: Join in Partnumber and GoogleCategoryLevel1 into the forecast table

# COMMAND ----------

raw_query = """select  b.PartNumber, b.GoogleCategoryLevel1, b.ItemID, a.* from {} a left join (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM silver_thor.fcst_oos_sales_qoh group by GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b on a.ID = b.DimProductID""".format(table_name)
raw_data = spark.sql(raw_query).createOrReplaceTempView('raw_data')

print('Ingesting best model results from Thor.')

# COMMAND ----------

# DBTITLE 1,Create table for Thor forecast at the aggregate DimProductID level
agg_query = """select id, model, data_length, 
case 
when data_length <= 24 then 'less than 24' 
when data_length > 24 and data_length <= 52 then 'between 24 and 52'
when data_length > 52 then 'over 52 weeks'
when data_length > 104 then 'over 104 weeks'
end as time_series_category, 
training_length, holdout_length, avg(holdout_mape) as holdout_mape, avg(holdout_wfa) as holdout_wfa, avg(holdout_smape) as holdout_smape, sum(fitted_values) as fitted_values, sum(y_train) as y_train, sum(five_forecast) as five_forecast, sum(nine_forecast) as nine_forecast, sum(thirteen_forecast) as thirteen_forecast, sum(fifty_two_forecast) as fifty_two_forecast, sum(y_test) as y_test from {} group by id, model, data_length, training_length, holdout_length""".format(table_name)

agg_data = spark.sql(agg_query).createOrReplaceTempView('agg_data')

print('Ingesting best model aggregated to the model. No time component. Includes time_series_category as well.')

# COMMAND ----------

# MAGIC %md ##### Step 3: Join compare table (rob or actuals) with forecast results

# COMMAND ----------

# DBTITLE 0,Create table to compare Rob forecast vs Thor forecast
# Create table to compare Rob forecast vs Thor forecast
benchmark_query = """SELECT

   thor.GoogleCategoryLevel1
  ,r.ReplenishStatus
  ,p.ListingType
  ,ig.ItemGrade
  ,thor.PartNumber
  ,thor.ItemID
  ,thor.id as DimProductID
  ,DATE((LEFT(thor.ts, 10))) as ts_date
  ,thor.model
  ,thor.label
  ,thor.data_length
  ,thor.training_length
  ,thor.holdout_length
  ,thor.fitted_values
  ,thor.y_train
  ,thor.y_test
  ,rob.Items_shipped
  ,rob.ROB_Forecast
  ,thor.five_forecast
  ,thor.nine_forecast
  ,thor.thirteen_forecast
  ,thor.fifty_two_forecast
  ,thor.holdout_forecast
  ,thor.holdout_mae
  ,thor.holdout_mse
  ,thor.holdout_mape
  ,thor.holdout_smape
  ,thor.holdout_wfa
  ,thor.error
   
FROM raw_data AS thor
INNER JOIN
(
SELECT
       
    ROB.ItemID, 
    DT.WeekStartDate, 
    ROB.Items_Shipped, 
    ROB.ROB_Forecast

FROM {} AS ROB
INNER JOIN date_table DT 
ON DT.WeekNumber = ROB.Fiscal_Week where DT.FiscalYear == '2021'   
) AS rob
ON DATE((LEFT(thor.ts, 10))) = DATE(rob.WeekStartDate) AND thor.ItemID = rob.ItemID
LEFT JOIN
silver_thor.fcst_itemgrade ig
ON thor.id = ig.DimProductID
LEFT JOIN 
bronze_spreedw.dim_product p
ON thor.ID = p.DimProductID
LEFT JOIN
(
select 
ItemID,
case 
when ProcurementStatus in ('Replenish', 'Replenish - Reset', 'Replenish - Amplify', 'Replenish - Manual') then 'Replenish'
when ProcurementStatus in ('Not Ordering - Other', 'Not Ordering - Discontinued', 'Not Ordering - Low Volume', 'Not Ordering - Cost Improvement', 'Promotional Costing') then 'Non-Replenish'
when ProcurementStatus in (null) then 'null'
end ReplenishStatus
from
silver_thor.itemid_procurementstatus_list
) r
ON thor.ItemID = r.ItemID""".format(compare_table)

spark.sql(benchmark_query).createOrReplaceTempView('benchmark_consensus_thor_spark')

print('Ingesting Rob vs Thor table. Joined on WeekStartDate and ItemID.')

# create categorical columns based off of holdout (goes beyond just top 25 gmv - used for full analysis of rob vs thor)
all = spark.sql("""select *, case 
when data_length <= 24 then 'less than 24' 
when data_length > 24 and data_length <= 52 then 'between 24 and 52'
when data_length > 52 then 'over 52 weeks'
when data_length > 104 then 'over 104 weeks'
end as time_series_category from benchmark_consensus_thor_spark
where DimProductID != 212506""").toPandas()

print('Use table name "all" as a pandas dataframe.')

print('Count of time series category based IDS: ')
len(all['DimProductID'].unique())
print('Using table name:' )
print(table_name)

# COMMAND ----------

raw_benchmark_query = """SELECT

   thor.GoogleCategoryLevel1
  ,r.ReplenishStatus
  ,p.ListingType
  ,ig.ItemGrade
  ,thor.PartNumber
  ,thor.ItemID
  ,thor.id as DimProductID
  ,DATE((LEFT(thor.ts, 10))) as ts_date
  ,thor.model
  ,thor.label
  ,thor.data_length
  ,thor.training_length
  ,thor.holdout_length
  ,thor.fitted_values
  ,thor.y_train
  ,thor.y_test
  ,thor.five_forecast
  ,thor.nine_forecast
  ,thor.thirteen_forecast
  ,thor.fifty_two_forecast
  ,thor.holdout_forecast
  ,thor.holdout_mae
  ,thor.holdout_mse
  ,thor.holdout_mape
  ,thor.holdout_smape
  ,thor.holdout_wfa
  ,thor.error
   
FROM raw_data AS thor

LEFT JOIN
silver_thor.fcst_itemgrade ig
ON thor.id = ig.DimProductID
LEFT JOIN 
bronze_spreedw.dim_product p
ON thor.ID = p.DimProductID
LEFT JOIN
(
select 
ItemID,
case 
when ProcurementStatus in ('Replenish', 'Replenish - Reset', 'Replenish - Amplify', 'Replenish - Manual') then 'Replenish'
when ProcurementStatus in ('Not Ordering - Other', 'Not Ordering - Discontinued', 'Not Ordering - Low Volume', 'Not Ordering - Cost Improvement', 'Promotional Costing') then 'Non-Replenish'
when ProcurementStatus in (null) then 'null'
end ReplenishStatus
from
silver_thor.itemid_procurementstatus_list
) r
ON thor.ItemID = r.ItemID"""

spark.sql(raw_benchmark_query).createOrReplaceTempView('raw_benchmark_consensus_thor_spark')

print('Use raw_benchmark_consensus_thor_spark for any non-compared table')

# COMMAND ----------

# rolling wmape needs to be pulling actuals from the current week and needs to be added to the last run
