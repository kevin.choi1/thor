# Databricks notebook source
# MAGIC %sql
# MAGIC select * from silver_thor.fcst_loki

# COMMAND ----------

# MAGIC %md ##### Step 1: Table ingestion

# COMMAND ----------

old_table_name = 'gold_thor.best_model_forecast_results'
table_name = 'gold_thor.fcst_best_model_results'
archive_table_name = 'gold_thor.fcst_best_model_results_archive'
actual_table = 'silver_thor.input_oossales

# COMMAND ----------

# notes
# 1.) 52W forecast for demand planning (not versioning)
# 2.) 13W forecast for WMAPE/BIAS (versioning) - this will have actuals joined to the forecast per snapshot date

# COMMAND ----------

# MAGIC %md ##### Step 2: Insert the newly archived forecasts

# COMMAND ----------

fcst_top_ranking_seasonality_table = spark.sql('SELECT VendorCategory, CategoryID, Correlation, LPAD(WeekNumber, 2, "00") AS WeekNumber, Seasonality FROM silver_thor.fcst_top_vendor_category_correlation')
fcst_top_ranking_seasonality_table.createOrReplaceTempView('fcst_top_ranking_seasonality_table')

# COMMAND ----------

# back fill for 2022-02-06
input_oossales = spark.sql(
"""
WITH
DateTable AS (
SELECT DISTINCT
MIN(DimDateID) AS DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) as WeekStartDate
,CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")) AS FiscalYearWeek
,FiscalMonth
,CONCAT(FiscalYear, LPAD(FiscalMonth, 2, "00")) AS FiscalYearMonth
,FiscalYear
,LPAD(FiscalWeek, 2, "00") AS WeekNumber
FROM bronze_spreedw_20220220.dim_date
GROUP BY DATE((LEFT(FiscalWeekRange, 10))), CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")), LPAD(FiscalWeek, 2, "00"), FiscalMonth, FiscalYear
ORDER BY DimDateID
),

ThisWeek AS (
SELECT
'2022-02-20' AS TodaysDate
,DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) AS ThisWeek
FROM bronze_spreedw_20220220.dim_date
WHERE DATE(Date) = DATE('2022-02-20')
),

QuantityOnHand AS (
SELECT
CAT.DimProductID
,CAT.ItemID
,CAT.PartNumber
,QOH.DimDateID
,CAL.WeekStartDate
,QOH.QuantityOnHandStartOfDayNetwork AS QuantityOnHandUnfilled
FROM bronze_spreedw_20220220.fact_productperformanceinventory AS QOH
LEFT JOIN bronze_spreedw_20220220.dim_product AS CAT ON QOH.ItemID = CAT.ItemID
LEFT JOIN DateTable AS CAL ON QOH.DimDateID = CAL.DimDateID
WHERE CAL.WeekStartDate IS NOT NULL --AND CAT.DimProductID IN (10319, 60743, 62850)
),

HistoricalSales AS (
SELECT
VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,DATE((LEFT(CAL.FiscalWeekRange, 10))) AS WeekStartDate
,SUM(VC.Quantity) AS ItemsShipped
FROM bronze_spreedw_20220220.fact_valuechain AS VC
LEFT JOIN bronze_spreedw_20220220.dim_date AS CAL ON CAL.DimDateID = VC.DimDateOfOrderID
LEFT JOIN bronze_spreedw_20220220.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
GROUP BY VC.DimShippedProductID, PROD.ItemID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
ORDER BY VC.DimShippedProductID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
),

DimShippedIDList AS (
SELECT DISTINCT
CONCAT(PROD.Supplier, "_", PROD.GoogleCategoryLevel1) AS VendorCategory
,PROD.Supplier
,VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,PROD.GoogleCategoryLevel1
FROM bronze_spreedw_20220220.fact_valuechain AS VC
LEFT JOIN bronze_spreedw_20220220.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
--WHERE VC.DimShippedProductID IN (10319, 60743, 62850)
),

KeyTable AS (
SELECT DISTINCT
DimShippedIDList.*
,WeekStartDate
FROM DimShippedIDList
CROSS JOIN DateTable
),

SalesQOHUnfilled AS (
SELECT
KeyTable.VendorCategory
,KeyTable.Supplier
,KeyTable.PartNumber
,KeyTable.ItemID
,KeyTable.DimShippedProductID
,KeyTable.GoogleCategoryLevel1
,SEAS.CategoryID
,SEAS.Correlation
,DateTable.DimDateID
,KeyTable.WeekStartDate
,DateTable.FiscalYearWeek
,DateTable.WeekNumber
,DateTable.FiscalMonth
,DateTable.FiscalYearMonth
,DateTable.FiscalYear
,COALESCE(HistoricalSales.ItemsShipped, 0) AS ItemsShipped
,QuantityOnHand.QuantityOnHandUnfilled
,COALESCE(SEAS.Seasonality, 0.015) AS Seasonality
FROM KeyTable
LEFT JOIN QuantityOnHand ON QuantityOnHand.DimProductID = KeyTable.DimShippedProductID AND QuantityOnHand.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN HistoricalSales ON HistoricalSales.DimShippedProductID = KeyTable.DimShippedProductID AND HistoricalSales.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN DateTable ON DateTable.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN fcst_top_ranking_seasonality_table AS SEAS ON SEAS.VendorCategory = KeyTable.VendorCategory AND SEAS.WeekNumber = DateTable.WeekNumber
WHERE KeyTable.WeekStartDate <= CURRENT_TIMESTAMP() AND KeyTable.WeekStartDate >= '2014-12-21' --AND KeyTable.DimShippedProductID IN (10319, 60743, 62850, 72790)
ORDER BY KeyTable.DimShippedProductID, KeyTable.WeekStartDate
),

CumulativeSum AS (
SELECT
SalesQOHUnfilled.*
,SUM(ItemsShipped) OVER (PARTITION BY DimShippedProductID ORDER BY WeekStartDate) AS CumulativeSum
,SUM(SalesQOHUnfilled.ItemsShipped) OVER (PARTITION BY SalesQOHUnfilled.DimShippedProductID ORDER BY SalesQOHUnfilled.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING) AS AnnualSales
FROM SalesQOHUnfilled
ORDER BY DimShippedProductID, WeekStartDate
),

ActiveStatus AS (
SELECT
CumulativeSum.*
,CASE WHEN CumulativeSum.CumulativeSum = 0 THEN 'Inactive' ELSE 'Active' END AS ActiveStatus
FROM CumulativeSum
),

InStockStatus AS (
SELECT
ActiveStatus.*
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 0 ELSE ActiveStatus.QuantityOnHandUnfilled END AS QuantityOnHand
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 'Inactive' WHEN ActiveStatus.QuantityOnHandUnfilled < 1 THEN 'OOS' ELSE 'InStock' END AS InStockStatus
FROM ActiveStatus
ORDER BY ActiveStatus.DimShippedProductID, ActiveStatus.WeekStartDate
),

QOHFilled AS (
SELECT
q.*
,QOHPartition
,FIRST_VALUE(q.QuantityOnHand) OVER (PARTITION BY QOHPartition ORDER BY q.DimShippedProductID, q.WeekStartDate) AS QuantityOnHandFilled
FROM (
SELECT
InStockStatus.*
,SUM(CASE WHEN QuantityOnHand IS NULL THEN 0 ELSE 1 END) OVER (ORDER BY DimShippedProductID, WeekStartDate) AS QOHPartition
FROM InStockStatus
ORDER BY DimShippedProductID
) AS q
),

RelevantVariables AS (
SELECT
QOHFilled.VendorCategory
,QOHFilled.Supplier
,QOHFilled.PartNumber
,QOHFilled.DimShippedProductID
,QOHFilled.ItemID
,QOHFilled.GoogleCategoryLevel1
,QOHFilled.CategoryID
,QOHFilled.Correlation
,QOHFilled.DimDateID
,QOHFilled.WeekStartDate
,QOHFilled.FiscalYearWeek
,QOHFilled.WeekNumber
,QOHFilled.FiscalYear
,QOHFilled.FiscalMonth
,QOHFilled.FiscalYearMonth
,QOHFilled.ItemsShipped
,QOHFilled.QuantityOnHandFilled AS QuantityOnHand
,QOHFilled.Seasonality
,QOHFilled.CumulativeSum
,QOHFilled.ActiveStatus
,QOHFilled.InStockStatus
,QOHFilled.AnnualSales
,CASE WHEN QOHFilled.InStockStatus = 'InStock' OR QOHFilled.InStockStatus = 'Inactive' THEN QOHFilled.Seasonality ELSE 0 END AS WeeklyInStockSeasonality
FROM QOHFilled
),

InStockSeasonality AS (
SELECT
RelevantVariables.*
,ROUND(SUM(RelevantVariables.WeeklyInStockSeasonality) OVER (PARTITION BY RelevantVariables.DimShippedProductID ORDER BY RelevantVariables.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING), 4) AS InStockSeasonality
FROM RelevantVariables
ORDER BY RelevantVariables.DimShippedProductID, RelevantVariables.WeekStartDate
),

AnnualSalesOOS AS (
SELECT
InStockSeasonality.*
,CASE
WHEN InStockSeasonality >= .9999 THEN AnnualSales
WHEN (InStockSeasonality < .1) THEN (AnnualSales / .1)
ELSE (AnnualSales / InStockSeasonality)
END AS AnnualSalesOOS
FROM InStockSeasonality
),

FinalQuery AS (
SELECT
AnnualSalesOOS.*
,CASE WHEN InStockStatus = 'OOS' THEN Seasonality * AnnualSalesOOS ELSE ItemsShipped END AS WeeklySalesOOS
,'2022-02-20' AS TodaysDate
FROM AnnualSalesOOS
LEFT JOIN ThisWeek ON ThisWeek.TodaysDate = TodaysDate
WHERE DATE(AnnualSalesOOS.WeekStartDate) < DATE(ThisWeek.ThisWeek)
ORDER BY Supplier, DimShippedProductID, WeekStartDate
)

SELECT
VendorCategory
,Supplier
,PartNumber
,ItemID
,DimShippedProductID AS DimProductID
,GoogleCategoryLevel1
,CategoryID
,Correlation
,DimDateID
,FiscalYearWeek
,FiscalYear
,WeekNumber
,WeekStartDate
,FiscalYearMonth
,FiscalMonth
,ActiveStatus
,InStockStatus
,Seasonality
,COALESCE(InStockSeasonality, 0) AS InStockSeasonality
,QuantityOnHand
,COALESCE(ItemsShipped, 0) AS ItemsShipped
,COALESCE(WeeklySalesOOS, 0) AS WeeklySalesOOS
,COALESCE(AnnualSales, 0) AS AnnualSales
,COALESCE(AnnualSalesOOS, 0) AS AnnualSalesOOS
,DATE('2022-02-20') AS created_on
FROM FinalQuery
WHERE WeekStartDate >= '2015-12-27'
ORDER BY Supplier, DimProductID, WeekStartDate
"""
)
input_oossales.createOrReplaceTempView('input_oossales')

# COMMAND ----------

insert_data = spark.sql(
"""
-- join the latest archived forecasts with PartNumber ,GCL1, ItemGrade
WITH
fcst_best_model AS (
    SELECT  
    b.PartNumber, 
    b.GoogleCategoryLevel1, 
    ig.ItemGrade,
    b.ItemID, 
    a.* 
        FROM (SELECT
              * 
              FROM gold_thor.fcst_best_model_results_test_archive) AS a
        LEFT JOIN (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM input_oossales GROUP BY GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b ON a.ID = b.DimProductID
        LEFT JOIN silver_thor.fcst_itemgrade ig ON a.id = ig.DimProductID
),

-- thirteen week future forecast
thirteen_fcst_best_model AS (
    SELECT 
    a.*
        FROM
            (SELECT
             PartNumber, 
             ItemGrade, 
             GoogleCategoryLevel1, 
             ItemID, 
             id, 
             created_on, 
             ts, 
             model, 
             label, 
             ts_category, 
             data_length, 
             y_train AS actuals, 
             thirteen_forecast,
             adj_thirteen_forecast,
             positive_margin_sales_percent,
             holdout_mae, 
             holdout_mse, 
             holdout_mape, 
             holdout_smape,
             holdout_wfa, 
             RANK() over (partition BY created_on, id ORDER BY ts ASC) AS forecast_week 
                 FROM fcst_best_model) AS a
    WHERE a.forecast_week <= 13
    ORDER BY a.id, a.forecast_week ASC
),

-- lastest week of actuals
latest_week_actuals AS (SELECT 
                        * 
                            FROM input_oossales 
                        WHERE WeekStartDate = (SELECT
                                               max(WeekStartDate) 
                                                   FROM input_oossales)  
)

--latest_week_actuals AS (SELECT
--                        *
--                            FROM input_oossales WHERE WeekStartDate = '2022-02-06')

-- thirteen week forecast table and updating the first week of actuals
SELECT 
r.PartNumber, 
r.GoogleCategoryLevel1,
ig.ItemGrade,
case
when r.data_length <= 24 then 'less than 24' 
when r.data_length > 24 AND r.data_length <= 52 then 'between 24 and 52'
when r.data_length > 52 then 'over 52 weeks'
when r.data_length > 104 then 'over 104 weeks'
end AS time_series_category,
r.ItemID, 
r.id as DimProductID , 
r.created_on, 
r.ts,
r.forecast_week,
r.model, 
r.label, 
r.ts_category, 
r.data_length, 
COALESCE(r.actuals, actuals.WeeklySalesOOS) AS actuals,
r.thirteen_forecast,
r.adj_thirteen_forecast,
r.positive_margin_sales_percent,
r.holdout_mae, 
r.holdout_mse, 
r.holdout_mape, 
r.holdout_smape,
r.holdout_wfa
    FROM thirteen_fcst_best_model as r
    LEFT JOIN latest_week_actuals AS actuals ON r.id = actuals.DimProductID AND r.ts = actuals.WeekStartDate
    LEFT JOIN silver_thor.fcst_itemgrade ig ON r.id = ig.DimProductID
WHERE r.id != 212506
ORDER BY r.id, r.forecast_week
""")

insert_data.createOrReplaceTempView('insert_data')

# COMMAND ----------

# DBTITLE 1,2022-02-13
# MAGIC %sql
# MAGIC SELECT * FROM insert_data where DimProductID = 9 order by created_on, ts asc

# COMMAND ----------

# DBTITLE 1,2022-02-06 (SNAPSHOT)
# MAGIC %sql
# MAGIC SELECT * FROM insert_data where DimProductID = 9 order by created_on, ts asc

# COMMAND ----------

# DBTITLE 1,Query for joining new actuals
test = spark.sql("""
SELECT
archived.PartNumber,
archived.GoogleCategoryLevel1,
archived.ItemGrade,
archived.time_series_category,
archived.ItemID,
archived.DimProductID,
archived.created_on,
archived.ts,
archived.forecast_week,
archived.model,
archived.label,
archived.ts_category,
archived.data_length,
COALESCE(new_data.actuals, archived.actuals) as actuals,
archived.thirteen_forecast,
archived.adj_thirteen_forecast,
archived.positive_margin_sales_percent,
archived.holdout_mse,
archived.holdout_mape,
archived.holdout_smape,
archived.holdout_wfa

FROM gold_thor.fcst_snapshot_archive as archived
LEFT JOIN (SELECT created_on, DimProductID, ts, forecast_week, actuals FROM insert_data where actuals is not null) as new_data ON new_data.DimProductID = archived.DimProductID AND new_data.ts = archived.ts AND new_data.created_on = archived.created_on
""").createOrReplaceTempView('test')

# COMMAND ----------

# MAGIC %sql
# MAGIC SELECT * FROM TEST where DimProductID = 9 order by created_on, ts asc

# COMMAND ----------

# DBTITLE 1,Update Archive with this week's new actuals
# MAGIC %sql
# MAGIC CREATE OR REPLACE TABLE gold_thor.fcst_snapshot_archive AS
# MAGIC SELECT
# MAGIC archived.PartNumber,
# MAGIC archived.GoogleCategoryLevel1,
# MAGIC archived.ItemGrade,
# MAGIC archived.time_series_category,
# MAGIC archived.ItemID,
# MAGIC archived.DimProductID,
# MAGIC archived.created_on,
# MAGIC archived.ts,
# MAGIC archived.forecast_week,
# MAGIC archived.model,
# MAGIC archived.label,
# MAGIC archived.ts_category,
# MAGIC archived.data_length,
# MAGIC COALESCE(new_data.actuals, archived.actuals) as actuals,
# MAGIC archived.thirteen_forecast,
# MAGIC archived.adj_thirteen_forecast,
# MAGIC archived.positive_margin_sales_percent,
# MAGIC archived.holdout_mse,
# MAGIC archived.holdout_mape,
# MAGIC archived.holdout_smape,
# MAGIC archived.holdout_wfa
# MAGIC 
# MAGIC FROM gold_thor.fcst_snapshot_archive as archived
# MAGIC LEFT JOIN (SELECT created_on, DimProductID, ts, forecast_week, actuals FROM insert_data where actuals is not null) as new_data ON new_data.DimProductID = archived.DimProductID AND new_data.ts = archived.ts AND new_data.created_on = archived.created_on

# COMMAND ----------

# MAGIC %sql
# MAGIC create or replace table gold_thor.fcst_snapshot_archive as select * from insert_data

# COMMAND ----------

# DBTITLE 1,The new data being joined to the archive forecast
# MAGIC %sql
# MAGIC SELECT created_on, DimProductID, ts, forecast_week, actuals FROM insert_data where actuals is not null and DimProductID = 9 order by created_on desc

# COMMAND ----------

# we have training data on 2-06 but it shows as null need to revisit the query

# COMMAND ----------

# MAGIC %sql
# MAGIC --CREATE OR REPLACE TABLE gold_thor.fcst_snapshot_archive AS SELECT * FROM insert_data

# COMMAND ----------

# MAGIC %sql
# MAGIC --INSERT INTO gold_thor.fcst_snapshot_archive SELECT * FROM insert_data

# COMMAND ----------

# update values in gold_thor.fcst_snapshot_archive so that the future forecast will be updated with the new week data in insert_data

# COMMAND ----------

# DBTITLE 1,Converting Python to SQL
# MAGIC %sql
# MAGIC select * from insert_data where DimProductID = 9

# COMMAND ----------

# MAGIC %md ##### sku level metrics

# COMMAND ----------

sku_thirteen_metrics = spark.sql("""
SELECT 
PartNumber, 
GoogleCategoryLevel1, 
ItemGrade, 
DimProductID, 
created_on, 
time_series_category, 
data_length, 
model, 
holdout_mape, 
holdout_smape, 
-- forecasts
SUM(thirteen_forecast) AS cumulative_thor_forecast,
SUM(adj_thirteen_forecast) AS cumulative_thor_adj_forecast,
-- actuals
SUM(actuals) AS cumulative_actuals,
-- wmape
( ABS( SUM(actuals) - SUM(thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) AS sku_thor_wmape,
( ABS( SUM(actuals) - SUM(thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) * SUM(actuals) AS sku_thor_wmape_coef,
-- wmape adj
( ABS( SUM(actuals) - SUM(adj_thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) AS sku_thor_adj_wmape,
( ABS( SUM(actuals) - SUM(adj_thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) * SUM(actuals) AS sku_thor_adj_wmape_coef,
-- bias
( SUM(thirteen_forecast) - SUM(actuals) ) / ( SUM(thirteen_forecast) + SUM(actuals)) AS sku_thor_bias,
-- bias adj
( SUM(adj_thirteen_forecast) - SUM(actuals) ) / ( SUM(adj_thirteen_forecast) + SUM(actuals)) AS sku_thor_adj_bias

  FROM insert_data 
  GROUP BY PartNumber, GoogleCategoryLevel1, ItemGrade, DimProductID, created_on, time_series_category, data_length, model, holdout_mape, holdout_smape
""")

sku_thirteen_metrics.createOrReplaceTempView('sku_thirteen_metrics')

# COMMAND ----------

# MAGIC %md ##### overall metrics

# COMMAND ----------

overall_metrics = spark.sql("""
SELECT 
'overall' AS metrics_categories,
created_on, 
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY created_on
""")

# COMMAND ----------

# MAGIC %md ##### google category level 1 metrics

# COMMAND ----------

gcl1_thirteen_metrics = spark.sql("""
SELECT 
'gcl1' AS metrics_categories,
GoogleCategoryLevel1,  
created_on, 
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY GoogleCategoryLevel1, created_on
""")

gcl1_thirteen_metrics.createOrReplaceTempView('gcl1_thirteen_metrics')

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from gcl1_thirteen_metrics

# COMMAND ----------

# MAGIC %md ##### item grade metrics

# COMMAND ----------

itemgrade_thirteen_metrics = spark.sql("""
SELECT 
'itemgrade' AS metrics_categories,
ItemGrade,  
created_on, 
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY ItemGrade, created_on
""")

itemgrade_thirteen_metrics.createOrReplaceTempView('itemgrade_thirteen_metrics')

# COMMAND ----------

# MAGIC %md ##### time series length metrics

# COMMAND ----------

timeseries_thirteen_metrics = spark.sql("""
SELECT 
'time_series_length' AS metrics_categories,
time_series_length,  
created_on, 
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY time_series_length, created_on
""")

timeseries_thirteen_metrics.createOrReplaceTempView('timeseries_thirteen_metrics')

# COMMAND ----------

# MAGIC %md ##### model metrics

# COMMAND ----------

model_thirteen_metrics = spark.sql("""
SELECT 
'model' AS metrics_categories,
model,  
created_on, 
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY model, created_on
""")

model_thirteen_metrics.createOrReplaceTempView('model_thirteen_metrics')

# COMMAND ----------

### OLD CODE BELOW

# COMMAND ----------

archive_table_name = 'gold_thor.fcst_best_model_results_archive'
actual_table = 'silver_thor.fcst_oos_sales_qoh_dimshippedproductid_rollingl365'

# COMMAND ----------

actual_table, archive_table_name

# COMMAND ----------

# update tables
raw_query = """select  b.PartNumber, b.GoogleCategoryLevel1, b.ItemID, a.* from {} a left join (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM silver_thor.fcst_oos_sales_qoh group by GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b on a.ID = b.DimProductID""".format(archive_table_name)
raw_data = spark.sql(raw_query).createOrReplaceTempView('raw_data')

print('Ingesting best model results from Thor.')

# COMMAND ----------

# insert tables
raw_query = """select  b.PartNumber, b.GoogleCategoryLevel1, b.ItemID, a.* from {} a left join (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM silver_thor.fcst_oos_sales_qoh group by GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b on a.ID = b.DimProductID""".format(old_table_name)
raw_data = spark.sql(raw_query).createOrReplaceTempView('raw_data')

print('Ingesting best model results from Thor.')

# COMMAND ----------

raw_query = """
select  b.PartNumber, b.GoogleCategoryLevel1, ig.ItemGrade, b.ItemID, a.* from gold_thor.best_model_forecast_results a 
left join 
(
SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM silver_thor.fcst_oos_sales_qoh 
group by GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b 
on a.id = b.DimProductID
left join silver_thor.fcst_itemgrade ig
on a.id = ig.DimProductID
where a.label = 'Forecast'
""".format(old_table_name)

raw_data = spark.sql(raw_query).createOrReplaceTempView('raw_data')

print('Ingesting best model results from Thor.')

# COMMAND ----------

# MAGIC %sql
# MAGIC select a.* from
# MAGIC (select PartNumber, ItemGrade, GoogleCategoryLevel1, ItemID, id, created_on, ts, model, label, ts_category, data_length, y_train as actuals, thirteen_forecast, holdout_mae, holdout_mse, holdout_mape, holdout_smape, holdout_wfa, RANK() over (partition by id order by ts asc) as forecast_week from raw_data) as a
# MAGIC where a.forecast_week <= 13

# COMMAND ----------

# MAGIC %md ##### Step 3: Update dataset for actuals (current data vs last week's data)

# COMMAND ----------

# make sure the final table has the inner join of ids between forecast and actuals
spark.sql("""select id from raw_data group by id""").createOrReplaceTempView('id_groupby_table')

# grabs the actual's the week after last week's forecast
forecast_actuals = """
select 
case 
when fcst.data_length <= 24 then 'less than 24' 
when fcst.data_length > 24 and fcst.data_length <= 52 then 'between 24 and 52'
when fcst.data_length > 52 then 'over 52 weeks'
when fcst.data_length > 104 then 'over 104 weeks'
end as time_series_category,
fcst.*
from
(
select 
r.PartNumber,
r.GoogleCategoryLevel1,
ig.ItemGrade,
r.ItemID,
r.id as DimProductID,
r.created_on,
r.ts,
r.model,
r.label,
r.ts_category,
r.data_length,
r.training_length,
r.holdout_length,
r.fitted_values,
COALESCE(r.y_train, actuals.WeeklySalesOOS) as actuals,
r.error,
r.five_forecast,
r.nine_forecast,
r.thirteen_forecast,
r.fifty_two_forecast,
r.y_test,
r.holdout_forecast,
r.holdout_mae,
r.holdout_mse,
r.holdout_mape,
r.holdout_smape,
r.holdout_wfa
from {} as actuals
inner join raw_data as r
on r.id = actuals.DimProductID and r.ts = actuals.WeekStartDate
left join silver_thor.fcst_itemgrade ig
on r.id = ig.DimProductID
where actuals.WeekStartDate in (select DATE(max(ts)) + 7 latest_date from gold_thor.best_model_forecast_results where label = 'Train')
) as fcst

where fcst.DimProductID != 212506

""".format(actual_table, archive_table_name)

forecast_actuals = spark.sql(forecast_actuals).toPandas()

# COMMAND ----------

# MAGIC %md ##### Step 4: Insert dataset for actuals (current data vs last week's data)

# COMMAND ----------

# make sure the final table has the inner join of ids between forecast and actuals
spark.sql("""select id from raw_data group by id""").createOrReplaceTempView('id_groupby_table')

# grabs the actual's the week after last week's forecast
forecast_actuals = """
select 
case 
when fcst.data_length <= 24 then 'less than 24' 
when fcst.data_length > 24 and fcst.data_length <= 52 then 'between 24 and 52'
when fcst.data_length > 52 then 'over 52 weeks'
when fcst.data_length > 104 then 'over 104 weeks'
end as time_series_category,
fcst.*
from
(
select 
r.PartNumber,
r.GoogleCategoryLevel1,
ig.ItemGrade,
r.ItemID,
r.id as DimProductID,
r.created_on,
r.ts,
r.model,
r.label,
r.ts_category,
r.data_length,
r.training_length,
r.holdout_length,
r.fitted_values,
COALESCE(r.y_train, actuals.WeeklySalesOOS) as actuals,
r.error,
r.five_forecast,
r.nine_forecast,
r.thirteen_forecast,
r.fifty_two_forecast,
r.y_test,
r.holdout_forecast,
r.holdout_mae,
r.holdout_mse,
r.holdout_mape,
r.holdout_smape,
r.holdout_wfa
from {} as actuals
inner join raw_data as r
on r.id = actuals.DimProductID and r.ts = actuals.WeekStartDate
left join silver_thor.fcst_itemgrade ig
on r.id = ig.DimProductID
where actuals.WeekStartDate in (select DATE(max(ts)) + 7 latest_date from gold_thor.best_model_forecast_results where label = 'Train')
) as fcst

where fcst.DimProductID != 212506

""".format(actual_table)

forecast_actuals = spark.sql(forecast_actuals).toPandas()
