# Databricks notebook source
# MAGIC %md ##### Step 1: Table ingestion

# COMMAND ----------

old_table_name = 'gold_thor.best_model_forecast_results'
table_name = 'gold_thor.fcst_best_model_results'
archive_table_name = 'gold_thor.fcst_best_model_results_archive'
actual_table = 'silver_thor.fcst_oos_sales_qoh_dimshippedproductid_rollingl365'

# COMMAND ----------

# notes
# 1.) 52W forecast for demand planning (not versioning)
# 2.) 13W forecast for WMAPE/BIAS (versioning) - this will have actuals joined to the forecast per snapshot date

# COMMAND ----------

# MAGIC %md ##### Step 2: Join in Partnumber and GoogleCategoryLevel1 into the forecast table

# COMMAND ----------

# MAGIC %sql

# COMMAND ----------

# update tables
raw_query = """select  b.PartNumber, b.GoogleCategoryLevel1, b.ItemID, a.* from {} a left join (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM silver_thor.fcst_oos_sales_qoh group by GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b on a.ID = b.DimProductID""".format(archive_table_name)
raw_data = spark.sql(raw_query).createOrReplaceTempView('raw_data')

print('Ingesting best model results from Thor.')

# COMMAND ----------

# insert tables
raw_query = """select  b.PartNumber, b.GoogleCategoryLevel1, b.ItemID, a.* from {} a left join (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM silver_thor.fcst_oos_sales_qoh group by GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b on a.ID = b.DimProductID""".format(old_table_name)
raw_data = spark.sql(raw_query).createOrReplaceTempView('raw_data')

print('Ingesting best model results from Thor.')

# COMMAND ----------

raw_query = """
select  b.PartNumber, b.GoogleCategoryLevel1, ig.ItemGrade, b.ItemID, a.* from gold_thor.best_model_forecast_results a 
left join 
(
SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM silver_thor.fcst_oos_sales_qoh 
group by GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b 
on a.id = b.DimProductID
left join silver_thor.fcst_itemgrade ig
on a.id = ig.DimProductID
where a.label = 'Forecast'
""".format(old_table_name)

raw_data = spark.sql(raw_query).createOrReplaceTempView('raw_data')

print('Ingesting best model results from Thor.')

# COMMAND ----------

# DBTITLE 1,Insert Query
# MAGIC %sql
# MAGIC select a.* from
# MAGIC (select PartNumber, ItemGrade, GoogleCategoryLevel1, ItemID, id, created_on, ts, model, label, ts_category, data_length, y_train as actuals, thirteen_forecast, holdout_mae, holdout_mse, holdout_mape, holdout_smape, holdout_wfa, RANK() over (partition by id order by ts asc) as forecast_week from raw_data) as a
# MAGIC where a.forecast_week <= 13

# COMMAND ----------

# DBTITLE 1,Update Query
# MAGIC %sql
# MAGIC select
# MAGIC b.created_on,
# MAGIC b.ItemGrade, 
# MAGIC b.GoogleCategoryLevel1, 
# MAGIC b.ItemID, 
# MAGIC b.id,
# MAGIC b.model,
# MAGIC b.label,
# MAGIC b.ts_category,
# MAGIC b.data_length,
# MAGIC b.ts,
# MAGIC COALESCE(b.actuals, actuals.WeeklySalesOOS) as actuals,
# MAGIC b.thirteen_forecast, 
# MAGIC b.holdout_mae, 
# MAGIC b.holdout_mse, 
# MAGIC b.holdout_mape, 
# MAGIC b.holdout_smape, 
# MAGIC b.holdout_wfa
# MAGIC from
# MAGIC (
# MAGIC select a.* from
# MAGIC (select PartNumber, ItemGrade, GoogleCategoryLevel1, ItemID, id, created_on, ts, model, label, ts_category, data_length, y_train as actuals, thirteen_forecast, holdout_mae, holdout_mse, holdout_mape, holdout_smape, holdout_wfa, RANK() over (partition by id order by ts asc) as forecast_week from raw_data) as a
# MAGIC where a.forecast_week <= 13
# MAGIC ) b
# MAGIC -- only bring in the actuals the week after the previous update actuals: a week before created_on date
# MAGIC -- this file should be updated every week with create/replace
# MAGIC left join (select DimProductID, max(WeekStartDate) as MaxWeekStartDate, WeeklySalesOOS from bronze_spreedw_20220206.input_oossales_20220206) as actuals
# MAGIC b.id = actuals.DimProductID and b.ts = actuals.MaxWeekStartDate

# COMMAND ----------

# MAGIC %sql
# MAGIC select max(WeekStartDate) from silver_thor.input_oossales

# COMMAND ----------

# MAGIC %md ##### Step 3: Update dataset for actuals (current data vs last week's data)

# COMMAND ----------

# make sure the final table has the inner join of ids between forecast and actuals
spark.sql("""select id from raw_data group by id""").createOrReplaceTempView('id_groupby_table')

# grabs the actual's the week after last week's forecast
forecast_actuals = """
select 
case 
when fcst.data_length <= 24 then 'less than 24' 
when fcst.data_length > 24 and fcst.data_length <= 52 then 'between 24 and 52'
when fcst.data_length > 52 then 'over 52 weeks'
when fcst.data_length > 104 then 'over 104 weeks'
end as time_series_category,
fcst.*
from
(
select 
r.PartNumber,
r.GoogleCategoryLevel1,
ig.ItemGrade,
r.ItemID,
r.id as DimProductID,
r.created_on,
r.ts,
r.model,
r.label,
r.ts_category,
r.data_length,
r.training_length,
r.holdout_length,
r.fitted_values,
COALESCE(r.y_train, actuals.WeeklySalesOOS) as actuals,
r.error,
r.five_forecast,
r.nine_forecast,
r.thirteen_forecast,
r.fifty_two_forecast,
r.y_test,
r.holdout_forecast,
r.holdout_mae,
r.holdout_mse,
r.holdout_mape,
r.holdout_smape,
r.holdout_wfa
from {} as actuals
inner join raw_data as r
on r.id = actuals.DimProductID and r.ts = actuals.WeekStartDate
left join silver_thor.fcst_itemgrade ig
on r.id = ig.DimProductID
where actuals.WeekStartDate in (select DATE(max(ts)) + 7 latest_date from gold_thor.best_model_forecast_results where label = 'Train')
) as fcst

where fcst.DimProductID != 212506

""".format(actual_table, archive_table_name)

forecast_actuals = spark.sql(forecast_actuals).toPandas()

# COMMAND ----------

# MAGIC %md ##### Step 4: Insert dataset for actuals (current data vs last week's data)

# COMMAND ----------

# make sure the final table has the inner join of ids between forecast and actuals
spark.sql("""select id from raw_data group by id""").createOrReplaceTempView('id_groupby_table')

# grabs the actual's the week after last week's forecast
forecast_actuals = """
select 
case 
when fcst.data_length <= 24 then 'less than 24' 
when fcst.data_length > 24 and fcst.data_length <= 52 then 'between 24 and 52'
when fcst.data_length > 52 then 'over 52 weeks'
when fcst.data_length > 104 then 'over 104 weeks'
end as time_series_category,
fcst.*
from
(
select 
r.PartNumber,
r.GoogleCategoryLevel1,
ig.ItemGrade,
r.ItemID,
r.id as DimProductID,
r.created_on,
r.ts,
r.model,
r.label,
r.ts_category,
r.data_length,
r.training_length,
r.holdout_length,
r.fitted_values,
COALESCE(r.y_train, actuals.WeeklySalesOOS) as actuals,
r.error,
r.five_forecast,
r.nine_forecast,
r.thirteen_forecast,
r.fifty_two_forecast,
r.y_test,
r.holdout_forecast,
r.holdout_mae,
r.holdout_mse,
r.holdout_mape,
r.holdout_smape,
r.holdout_wfa
from {} as actuals
inner join raw_data as r
on r.id = actuals.DimProductID and r.ts = actuals.WeekStartDate
left join silver_thor.fcst_itemgrade ig
on r.id = ig.DimProductID
where actuals.WeekStartDate in (select DATE(max(ts)) + 7 latest_date from gold_thor.best_model_forecast_results where label = 'Train')
) as fcst

where fcst.DimProductID != 212506

""".format(actual_table)

forecast_actuals = spark.sql(forecast_actuals).toPandas()
