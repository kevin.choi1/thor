# Databricks notebook source
# MAGIC %md ##### Step 1: Table ingestion

# COMMAND ----------

# latest created_on date renamed as publish date
forecast_created_on = spark.sql("""select distinct(created_on) as forecast_created_on from silver_thor.input_oossales""").toPandas().min().astype(str)[0]
# year, month, day of created_on date
year_created_on = forecast_created_on[0:4]
month_created_on = forecast_created_on[5:7]
day_created_on = forecast_created_on[8:10]

print('year: ', year_created_on)
print('month: ', month_created_on)
print('day: ', day_created_on)

# COMMAND ----------

# latest created_on date renamed as publish date
publish_created_on = spark.sql("""select max(created_on) as publish_created_on from gold_thor.fcst_best_model_results_archive""").toPandas().min().astype(str)[0]
# year, month, day of created_on date
year_created_on = publish_created_on[0:4]
month_created_on = publish_created_on[5:7]
day_created_on = publish_created_on[8:10]

print('year: ', year_created_on)
print('month: ', month_created_on)
print('day: ', day_created_on)

# COMMAND ----------

# to correct 3-07-2022 data
publish_created_on = '2022-02-27'
publish_created_on = """'{}'""".format(publish_created_on)
print('publish date: ', publish_created_on)

# COMMAND ----------

# MAGIC %md ##### Step 2: Insert the newly archived forecasts

# COMMAND ----------

fcst_top_ranking_seasonality_table = spark.sql('SELECT VendorCategory, CategoryID, Correlation, LPAD(WeekNumber, 2, "00") AS WeekNumber, Seasonality FROM silver_thor.fcst_top_vendor_category_correlation')
fcst_top_ranking_seasonality_table.createOrReplaceTempView('fcst_top_ranking_seasonality_table')

# COMMAND ----------

# back fill for 2022-02-06
spark.sql(
"""
WITH
DateTable AS (
SELECT DISTINCT
MIN(DimDateID) AS DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) as WeekStartDate
,CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")) AS FiscalYearWeek
,FiscalMonth
,CONCAT(FiscalYear, LPAD(FiscalMonth, 2, "00")) AS FiscalYearMonth
,FiscalYear
,LPAD(FiscalWeek, 2, "00") AS WeekNumber
FROM bronze_spreedw.dim_date
GROUP BY DATE((LEFT(FiscalWeekRange, 10))), CONCAT(FiscalYear, LPAD(FiscalWeek, 2, "00")), LPAD(FiscalWeek, 2, "00"), FiscalMonth, FiscalYear
ORDER BY DimDateID
),

ThisWeek AS (
SELECT
{0} AS TodaysDate
,DimDateID
,DATE((LEFT(FiscalWeekRange, 10))) AS ThisWeek
FROM bronze_spreedw.dim_date
WHERE DATE(Date) = DATE({0})
),

QuantityOnHand AS (
SELECT
CAT.DimProductID
,CAT.ItemID
,CAT.PartNumber
,QOH.DimDateID
,CAL.WeekStartDate
,QOH.QuantityOnHandStartOfDayNetwork AS QuantityOnHandUnfilled
FROM bronze_spreedw.fact_productperformanceinventory AS QOH
LEFT JOIN bronze_spreedw.dim_product AS CAT ON QOH.ItemID = CAT.ItemID
LEFT JOIN DateTable AS CAL ON QOH.DimDateID = CAL.DimDateID
WHERE CAL.WeekStartDate IS NOT NULL --AND CAT.DimProductID IN (10319, 60743, 62850)
),

HistoricalSales AS (
SELECT
VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,DATE((LEFT(CAL.FiscalWeekRange, 10))) AS WeekStartDate
,SUM(VC.Quantity) AS ItemsShipped
FROM bronze_spreedw.fact_valuechain AS VC
LEFT JOIN bronze_spreedw.dim_date AS CAL ON CAL.DimDateID = VC.DimDateOfOrderID
LEFT JOIN bronze_spreedw.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
GROUP BY VC.DimShippedProductID, PROD.ItemID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
ORDER BY VC.DimShippedProductID, PROD.PartNumber, DATE((LEFT(CAL.FiscalWeekRange, 10)))
),

DimShippedIDList AS (
SELECT DISTINCT
CONCAT(PROD.Supplier, "_", PROD.GoogleCategoryLevel1) AS VendorCategory
,PROD.Supplier
,VC.DimShippedProductID
,PROD.ItemID
,PROD.PartNumber
,PROD.GoogleCategoryLevel1
FROM bronze_spreedw.fact_valuechain AS VC
LEFT JOIN bronze_spreedw.dim_product AS PROD ON PROD.DimProductID = VC.DimShippedProductID
--WHERE VC.DimShippedProductID IN (10319, 60743, 62850)
),

KeyTable AS (
SELECT DISTINCT
DimShippedIDList.*
,WeekStartDate
FROM DimShippedIDList
CROSS JOIN DateTable
),

SalesQOHUnfilled AS (
SELECT
KeyTable.VendorCategory
,KeyTable.Supplier
,KeyTable.PartNumber
,KeyTable.ItemID
,KeyTable.DimShippedProductID
,KeyTable.GoogleCategoryLevel1
,SEAS.CategoryID
,SEAS.Correlation
,DateTable.DimDateID
,KeyTable.WeekStartDate
,DateTable.FiscalYearWeek
,DateTable.WeekNumber
,DateTable.FiscalMonth
,DateTable.FiscalYearMonth
,DateTable.FiscalYear
,COALESCE(HistoricalSales.ItemsShipped, 0) AS ItemsShipped
,QuantityOnHand.QuantityOnHandUnfilled
,COALESCE(SEAS.Seasonality, 0.015) AS Seasonality
FROM KeyTable
LEFT JOIN QuantityOnHand ON QuantityOnHand.DimProductID = KeyTable.DimShippedProductID AND QuantityOnHand.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN HistoricalSales ON HistoricalSales.DimShippedProductID = KeyTable.DimShippedProductID AND HistoricalSales.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN DateTable ON DateTable.WeekStartDate = KeyTable.WeekStartDate
LEFT JOIN fcst_top_ranking_seasonality_table AS SEAS ON SEAS.VendorCategory = KeyTable.VendorCategory AND SEAS.WeekNumber = DateTable.WeekNumber
WHERE KeyTable.WeekStartDate <= CURRENT_TIMESTAMP() AND KeyTable.WeekStartDate >= '2014-12-21' --AND KeyTable.DimShippedProductID IN (10319, 60743, 62850, 72790)
ORDER BY KeyTable.DimShippedProductID, KeyTable.WeekStartDate
),

CumulativeSum AS (
SELECT
SalesQOHUnfilled.*
,SUM(ItemsShipped) OVER (PARTITION BY DimShippedProductID ORDER BY WeekStartDate) AS CumulativeSum
,SUM(SalesQOHUnfilled.ItemsShipped) OVER (PARTITION BY SalesQOHUnfilled.DimShippedProductID ORDER BY SalesQOHUnfilled.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING) AS AnnualSales
FROM SalesQOHUnfilled
ORDER BY DimShippedProductID, WeekStartDate
),

ActiveStatus AS (
SELECT
CumulativeSum.*
,CASE WHEN CumulativeSum.CumulativeSum = 0 THEN 'Inactive' ELSE 'Active' END AS ActiveStatus
FROM CumulativeSum
),

InStockStatus AS (
SELECT
ActiveStatus.*
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 0 ELSE ActiveStatus.QuantityOnHandUnfilled END AS QuantityOnHand
,CASE WHEN ActiveStatus.ActiveStatus = 'Inactive' THEN 'Inactive' WHEN ActiveStatus.QuantityOnHandUnfilled < 1 THEN 'OOS' ELSE 'InStock' END AS InStockStatus
FROM ActiveStatus
ORDER BY ActiveStatus.DimShippedProductID, ActiveStatus.WeekStartDate
),

QOHFilled AS (
SELECT
q.*
,QOHPartition
,FIRST_VALUE(q.QuantityOnHand) OVER (PARTITION BY QOHPartition ORDER BY q.DimShippedProductID, q.WeekStartDate) AS QuantityOnHandFilled
FROM (
SELECT
InStockStatus.*
,SUM(CASE WHEN QuantityOnHand IS NULL THEN 0 ELSE 1 END) OVER (ORDER BY DimShippedProductID, WeekStartDate) AS QOHPartition
FROM InStockStatus
ORDER BY DimShippedProductID
) AS q
),

RelevantVariables AS (
SELECT
QOHFilled.VendorCategory
,QOHFilled.Supplier
,QOHFilled.PartNumber
,QOHFilled.DimShippedProductID
,QOHFilled.ItemID
,QOHFilled.GoogleCategoryLevel1
,QOHFilled.CategoryID
,QOHFilled.Correlation
,QOHFilled.DimDateID
,QOHFilled.WeekStartDate
,QOHFilled.FiscalYearWeek
,QOHFilled.WeekNumber
,QOHFilled.FiscalYear
,QOHFilled.FiscalMonth
,QOHFilled.FiscalYearMonth
,QOHFilled.ItemsShipped
,QOHFilled.QuantityOnHandFilled AS QuantityOnHand
,QOHFilled.Seasonality
,QOHFilled.CumulativeSum
,QOHFilled.ActiveStatus
,QOHFilled.InStockStatus
,QOHFilled.AnnualSales
,CASE WHEN QOHFilled.InStockStatus = 'InStock' OR QOHFilled.InStockStatus = 'Inactive' THEN QOHFilled.Seasonality ELSE 0 END AS WeeklyInStockSeasonality
FROM QOHFilled
),

InStockSeasonality AS (
SELECT
RelevantVariables.*
,ROUND(SUM(RelevantVariables.WeeklyInStockSeasonality) OVER (PARTITION BY RelevantVariables.DimShippedProductID ORDER BY RelevantVariables.WeekStartDate DESC ROWS BETWEEN CURRENT ROW AND 51 FOLLOWING), 4) AS InStockSeasonality
FROM RelevantVariables
ORDER BY RelevantVariables.DimShippedProductID, RelevantVariables.WeekStartDate
),

AnnualSalesOOS AS (
SELECT
InStockSeasonality.*
,CASE
WHEN InStockSeasonality >= .9999 THEN AnnualSales
WHEN (InStockSeasonality < .1) THEN (AnnualSales / .1)
ELSE (AnnualSales / InStockSeasonality)
END AS AnnualSalesOOS
FROM InStockSeasonality
),

FinalQuery AS (
SELECT
AnnualSalesOOS.*
,CASE WHEN InStockStatus = 'OOS' THEN Seasonality * AnnualSalesOOS ELSE ItemsShipped END AS WeeklySalesOOS
,{0} AS TodaysDate
FROM AnnualSalesOOS
LEFT JOIN ThisWeek ON ThisWeek.TodaysDate = TodaysDate
WHERE DATE(AnnualSalesOOS.WeekStartDate) < DATE(ThisWeek.ThisWeek)
ORDER BY Supplier, DimShippedProductID, WeekStartDate
)

SELECT
VendorCategory
,Supplier
,PartNumber
,ItemID
,DimShippedProductID AS DimProductID
,GoogleCategoryLevel1
,CategoryID
,Correlation
,DimDateID
,FiscalYearWeek
,FiscalYear
,WeekNumber
,WeekStartDate
,FiscalYearMonth
,FiscalMonth
,ActiveStatus
,InStockStatus
,Seasonality
,COALESCE(InStockSeasonality, 0) AS InStockSeasonality
,QuantityOnHand
,COALESCE(ItemsShipped, 0) AS ItemsShipped
,COALESCE(WeeklySalesOOS, 0) AS WeeklySalesOOS
,COALESCE(AnnualSales, 0) AS AnnualSales
,COALESCE(AnnualSalesOOS, 0) AS AnnualSalesOOS
,DATE({0}) AS created_on
FROM FinalQuery
WHERE WeekStartDate >= '2015-12-27'
ORDER BY Supplier, DimProductID, WeekStartDate
""".format(forecast_created_on)).createOrReplaceTempView('input_oossales')

# COMMAND ----------

spark.sql(
"""
-- join the latest archived forecasts with PartNumber ,GCL1, ItemGrade
WITH
fcst_best_model AS (
    SELECT  
    b.PartNumber, 
    b.GoogleCategoryLevel1, 
    ig.ItemGrade,
    b.ItemID, 
    a.* 
        FROM (SELECT
              * 
              FROM gold_thor.fcst_best_model_results_archive) AS a
        LEFT JOIN (SELECT GoogleCategoryLevel1, PartNumber, ItemID, DimProductID FROM input_oossales GROUP BY GoogleCategoryLevel1, PartNumber, ItemID, DimProductID) b ON a.ID = b.DimProductID
        LEFT JOIN silver_thor.fcst_itemgrade ig ON a.id = ig.DimProductID
),

-- thirteen week future forecast
thirteen_fcst_best_model AS (
    SELECT 
    a.*
        FROM
            (SELECT
             PartNumber, 
             ItemGrade, 
             GoogleCategoryLevel1, 
             ItemID, 
             id, 
             created_on, 
             ts, 
             model, 
             label, 
             ts_category, 
             data_length, 
             y_train AS actuals, 
             thirteen_forecast,
             adj_thirteen_forecast,
             positive_margin_sales_percent,
             holdout_mae, 
             holdout_mse, 
             holdout_mape, 
             holdout_smape,
             holdout_wfa, 
             RANK() over (partition BY created_on, id ORDER BY ts ASC) AS forecast_week 
                 FROM fcst_best_model) AS a
    WHERE a.forecast_week <= 13
    ORDER BY a.id, a.forecast_week ASC
),

-- lastest week of actuals
latest_week_actuals AS (SELECT 
                        * 
                            FROM input_oossales 
                        WHERE WeekStartDate = (SELECT
                                               max(WeekStartDate) 
                                                   FROM input_oossales)  
)

--latest_week_actuals AS (SELECT
--                        *
--                            FROM input_oossales WHERE WeekStartDate = '2022-02-06')

-- thirteen week forecast table and updating the first week of actuals
SELECT 
r.PartNumber, 
r.GoogleCategoryLevel1,
ig.ItemGrade,
case
when r.data_length <= 24 then 'less than 24' 
when r.data_length > 24 AND r.data_length <= 52 then 'between 24 and 52'
when r.data_length > 52 then 'over 52 weeks'
when r.data_length > 104 then 'over 104 weeks'
end AS time_series_category,
r.ItemID, 
r.id as DimProductID , 
r.created_on, 
r.ts,
r.forecast_week,
r.model, 
r.label, 
r.ts_category, 
r.data_length, 
COALESCE(r.actuals, actuals.WeeklySalesOOS) AS actuals,
r.thirteen_forecast,
r.adj_thirteen_forecast,
r.positive_margin_sales_percent,
r.holdout_mae, 
r.holdout_mse, 
r.holdout_mape, 
r.holdout_smape,
r.holdout_wfa
    FROM thirteen_fcst_best_model as r
    LEFT JOIN latest_week_actuals AS actuals ON r.id = actuals.DimProductID AND r.ts = actuals.WeekStartDate
    LEFT JOIN silver_thor.fcst_itemgrade ig ON r.id = ig.DimProductID
WHERE r.id != 212506
ORDER BY r.id, r.forecast_week
""").createOrReplaceTempView('insert_data')

# COMMAND ----------

# DBTITLE 1,Update Archive with this week's new actuals
# MAGIC %sql
# MAGIC SELECT
# MAGIC fcst.*,
# MAGIC CASE
# MAGIC WHEN fcst.max_lag = fcst.lag THEN fcst.ts
# MAGIC END AS max_lag_date
# MAGIC FROM
# MAGIC (
# MAGIC SELECT
# MAGIC fcst.*,
# MAGIC CASE 
# MAGIC WHEN fcst.actuals IS NOT NULL THEN
# MAGIC SUM(CASE WHEN fcst.actuals IS NOT NULL THEN 1 ELSE 0 END) over (PARTITION BY fcst.forecast_created_on, fcst.DimProductID ORDER BY fcst.ts ASC)
# MAGIC WHEN fcst.actuals IS NULL THEN NULL
# MAGIC END AS lag,
# MAGIC CASE 
# MAGIC WHEN fcst.actuals IS NOT NULL THEN
# MAGIC SUM(CASE WHEN fcst.actuals IS NOT NULL THEN 1 ELSE 0 END) over (PARTITION BY fcst.forecast_created_on, fcst.DimProductID)
# MAGIC WHEN fcst.actuals IS NULL THEN NULL
# MAGIC END AS max_lag
# MAGIC FROM
# MAGIC (
# MAGIC SELECT
# MAGIC archived.PartNumber,
# MAGIC archived.GoogleCategoryLevel1,
# MAGIC archived.ItemGrade,
# MAGIC archived.time_series_category,
# MAGIC archived.ItemID,
# MAGIC archived.DimProductID,
# MAGIC archived.forecast_created_on,
# MAGIC archived.ts,
# MAGIC archived.forecast_week,
# MAGIC archived.model,
# MAGIC archived.label,
# MAGIC archived.ts_category,
# MAGIC archived.data_length,
# MAGIC COALESCE(new_data.actuals, archived.actuals) AS actuals,
# MAGIC archived.thirteen_forecast,
# MAGIC archived.adj_thirteen_forecast,
# MAGIC archived.positive_margin_sales_percent,
# MAGIC archived.holdout_mse,
# MAGIC archived.holdout_mape,
# MAGIC archived.holdout_smape,
# MAGIC archived.holdout_wfa
# MAGIC 
# MAGIC FROM gold_thor.fcst_snapshot_archive as archived
# MAGIC 
# MAGIC LEFT JOIN (SELECT created_on, DimProductID, ts, forecast_week, actuals FROM insert_data WHERE actuals IS NOT null) AS new_data ON new_data.DimProductID = archived.DimProductID AND new_data.ts = archived.ts AND new_data.created_on = archived.created_on
# MAGIC 
# MAGIC ) AS fcst
# MAGIC ) AS fcst

# COMMAND ----------

# DBTITLE 1,Temporary FCST table
spark.sql("""
SELECT
fcst.*,
CASE
WHEN fcst.max_lag = fcst.lag THEN fcst.ts
END AS max_lag_date
FROM
(
SELECT
fcst.*,
CASE 
WHEN fcst.actuals IS NOT NULL THEN
SUM(CASE WHEN fcst.actuals IS NOT NULL THEN 1 ELSE 0 END) over (PARTITION BY fcst.forecast_created_on, fcst.DimProductID ORDER BY fcst.ts ASC)
WHEN fcst.actuals IS NULL THEN NULL
END AS lag,
CASE 
WHEN fcst.actuals IS NOT NULL THEN
SUM(CASE WHEN fcst.actuals IS NOT NULL THEN 1 ELSE 0 END) over (PARTITION BY fcst.forecast_created_on, fcst.DimProductID)
WHEN fcst.actuals IS NULL THEN NULL
END AS max_lag
FROM
(
SELECT
archived.PartNumber,
archived.GoogleCategoryLevel1,
archived.ItemGrade,
archived.time_series_category,
archived.ItemID,
archived.DimProductID,
archived.forecast_created_on,
archived.ts,
archived.forecast_week,
archived.model,
archived.label,
archived.ts_category,
archived.data_length,
archived.actuals AS actuals,
archived.thirteen_forecast,
archived.adj_thirteen_forecast,
archived.positive_margin_sales_percent,
archived.holdout_mse,
archived.holdout_mape,
archived.holdout_smape,
archived.holdout_wfa

FROM gold_thor.fcst_snapshot_archive as archived

) AS fcst
) AS fcst
order by fcst.forecast_created_on, DimProductID, ts asc
""").createOrReplaceTempView('fcst')

# COMMAND ----------

# DBTITLE 1,Find the max_lag_date
# max lag date
max_lag_date = spark.sql("""select max(max_lag_date) from fcst""").toPandas().min().astype(str)[0]
# format into proper string
max_lag_date_string = """'{}'""".format(max_lag_date)
print('max lag date: ', max_lag_date_string)

# COMMAND ----------

# DBTITLE 1,Create the max_lag_date
spark.sql(
"""
SELECT
PartNumber,
GoogleCategoryLevel1,
ItemGrade,
time_series_category,
ItemID,
DimProductID,
forecast_created_on,
ts,
forecast_week,
model,
label,
ts_category,
data_length,
actuals AS actuals,
thirteen_forecast,
adj_thirteen_forecast,
positive_margin_sales_percent,
holdout_mse,
holdout_mape,
holdout_smape,
holdout_wfa,
max_lag,
lag,
CASE
WHEN lag <= max_lag THEN {0}
ELSE NULL
END AS max_lag_date
FROM fcst
""".format(max_lag_date_string)).createOrReplaceTempView('fcst_with_max_lag_date')

# COMMAND ----------

# DBTITLE 1,WMAPE Metrics: Hardcoded for 2022-02-06
# for now only start with 2022-02-06 snapshot (rolling cumulative for now)
max_lag = 4
max_lag_date = publish_created_on
spark.sql("""select * from fcst_with_max_lag_date where max_lag = {0} and max_lag_date = {1} and forecast_created_on = '2022-02-06' """.format(max_lag, max_lag_date)).createOrReplaceTempView('input_data')

# COMMAND ----------

# MAGIC %md ##### sku level metrics

# COMMAND ----------

sku_thirteen_metrics = spark.sql("""
SELECT 
PartNumber, 
GoogleCategoryLevel1, 
ItemGrade, 
DimProductID, 
forecast_created_on,
{0} AS publish_created_on,
max_lag AS lag_length,
time_series_category, 
data_length, 
model, 
holdout_mape, 
holdout_smape, 
-- forecasts
SUM(thirteen_forecast) AS cumulative_thor_forecast,
SUM(adj_thirteen_forecast) AS cumulative_thor_adj_forecast,
-- actuals
SUM(actuals) AS cumulative_actuals,
-- wmape
( ABS( SUM(actuals) - SUM(thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) AS sku_thor_wmape,
( ABS( SUM(actuals) - SUM(thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) * SUM(actuals) AS sku_thor_wmape_coef,
-- wmape adj
( ABS( SUM(actuals) - SUM(adj_thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) AS sku_thor_adj_wmape,
( ABS( SUM(actuals) - SUM(adj_thirteen_forecast) ) / ( SUM(actuals) + 0.01 ) ) * SUM(actuals) AS sku_thor_adj_wmape_coef,
-- bias
( SUM(thirteen_forecast) - SUM(actuals) ) / ( SUM(thirteen_forecast) + SUM(actuals)) AS sku_thor_bias,
-- bias adj
( SUM(adj_thirteen_forecast) - SUM(actuals) ) / ( SUM(adj_thirteen_forecast) + SUM(actuals)) AS sku_thor_adj_bias

  FROM input_data 
  GROUP BY PartNumber, GoogleCategoryLevel1, ItemGrade, DimProductID, forecast_created_on, max_lag, time_series_category, data_length, model, holdout_mape, holdout_smape
""".format(publish_created_on))

sku_thirteen_metrics.createOrReplaceTempView('sku_thirteen_metrics')

display(sku_thirteen_metrics)

# COMMAND ----------

# MAGIC %md ##### overall metrics

# COMMAND ----------

overall_metrics = spark.sql("""
SELECT 
'overall' AS metrics_categories_label,
forecast_created_on, 
publish_created_on,
lag_length,
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY forecast_created_on, publish_created_on, lag_length
""")

overall_metrics.createOrReplaceTempView('overall_metrics')

display(overall_metrics)

# COMMAND ----------

# MAGIC %md ##### google category level 1 metrics

# COMMAND ----------

gcl1_thirteen_metrics = spark.sql("""
SELECT 
'gcl1' AS metrics_categories_label,
GoogleCategoryLevel1,  
forecast_created_on,
publish_created_on,
lag_length,
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY GoogleCategoryLevel1, forecast_created_on, publish_created_on, lag_length
""")

gcl1_thirteen_metrics.createOrReplaceTempView('gcl1_thirteen_metrics')

display(gcl1_thirteen_metrics)

# COMMAND ----------

# MAGIC %md ##### item grade metrics

# COMMAND ----------

itemgrade_thirteen_metrics = spark.sql("""
SELECT 
'itemgrade' AS metrics_categories_label,
ItemGrade,  
forecast_created_on, 
publish_created_on,
lag_length, 
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY ItemGrade, forecast_created_on, publish_created_on, lag_length
""")

itemgrade_thirteen_metrics.createOrReplaceTempView('itemgrade_thirteen_metrics')

display(itemgrade_thirteen_metrics)

# COMMAND ----------

# MAGIC %md ##### model metrics

# COMMAND ----------

model_thirteen_metrics = spark.sql("""
SELECT 
'model' AS metrics_categories_label,
model,  
forecast_created_on, 
publish_created_on, 
lag_length,
-- count
COUNT(DimProductID) as id_count,
-- forecasts
SUM(cumulative_thor_forecast) AS grp_cumulative_thor_forecast,
SUM(cumulative_thor_adj_forecast) AS grp_cumulative_thor_adj_forecast,
-- actuals
SUM(cumulative_actuals) AS grp_cumulative_actuals,
-- wmape
( SUM(sku_thor_wmape_coef) / SUM(cumulative_actuals) ) AS thor_wmape,
( SUM(sku_thor_adj_wmape_coef) / SUM(cumulative_actuals) ) AS thor_adj_wmape,
-- bias
( SUM(cumulative_thor_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_forecast) + SUM(cumulative_actuals) ) AS thor_bias,
( SUM(cumulative_thor_adj_forecast) - SUM(cumulative_actuals) ) / ( SUM(cumulative_thor_adj_forecast) + SUM(cumulative_actuals) ) AS thor_adj_bias

  FROM sku_thirteen_metrics 
  GROUP BY model, forecast_created_on, publish_created_on, lag_length
""")

model_thirteen_metrics.createOrReplaceTempView('model_thirteen_metrics')

display(model_thirteen_metrics)

# COMMAND ----------

# DBTITLE 1,Union all WMAPE metrics
spark.sql(
"""
SELECT metrics_categories_label, 'overall' as metrics_categories, forecast_created_on, publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM overall_metrics

UNION ALL

SELECT metrics_categories_label, GoogleCategoryLevel1 as metrics_categories, forecast_created_on, publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM gcl1_thirteen_metrics

UNION ALL

SELECT metrics_categories_label, ItemGrade as metrics_categories, forecast_created_on, publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM itemgrade_thirteen_metrics

UNION ALL

SELECT metrics_categories_label, model as metrics_categories, forecast_created_on, publish_created_on, lag_length, id_count, thor_wmape, thor_adj_wmape, thor_bias, thor_adj_bias FROM model_thirteen_metrics
""").createOrReplaceTempView('metrics')

# COMMAND ----------

# MAGIC %sql
# MAGIC create or replace table gold_thor.fcst_metrics
# MAGIC as
# MAGIC select * from metrics

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from gold_thor.fcst_metrics
